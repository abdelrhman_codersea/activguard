<?php

@session_start();
include('../campaign_setup.php');



if (isset($_POST['upsell']) && !empty($_POST['upsell']) && $_POST['upsell'] == 'upsell_process2') {

    //NewOrderCardOnFile($upsellCampaignId, $orderId, $upsellProductId, $upsellShippingId, $upsellPrice, $click_id, $notesUpsell);
    if (!empty($_POST['AFID'])) {
        $AFID = $_POST['AFID'];
    }
    if (!empty($_POST['SID'])) {
        $SID = $_POST['SID'];
    }
    if (!empty($_POST['AFFID'])) {
        $AFFID = $_POST['AFFID'];
    }
    if (!empty($_POST['C1'])) {
        $C1 = $_POST['C1'];
    }
    if (!empty($_POST['C2'])) {
        $C2 = $_POST['C2'];
    }
    if (!empty($_POST['C3'])) {
        $C3 = $_POST['C3'];
    }
    if (!empty($_POST['AID'])) {
        $AID = $_POST['AID'];
    }
    if (!empty($_POST['OPT'])) {
        $OPT = $_POST['OPT'];
    }
    if (!empty($_POST['click_id'])) {
        $click_id = $_POST['click_id'];
    }
    if (!empty($_POST['CID'])) {
        $CID = $_POST['CID'];
    }
    if (!empty($_POST['notes'])) {
        $notes = $_POST['notes'];
    }
    $upsellProductId = $_POST['product_id'];
    $shipping_price = "0.00";
    $purchasedItemsArray = array();
    foreach ($_SESSION['purchased_items'] as $v) {
        $purchasedItemsArray[] = $v['product_id'];
    }

    $refusalArray = array(
        49 => array(49, 50),
        50 => array(49, 50),
    );
    $productId = $upsellProductId;
    $errordub = '';
    if (count(array_intersect($refusalArray[$productId], $purchasedItemsArray)) > 0) {
        $errordub = 'duplicate order';
    }





    if (empty($errordub)) {


        $content = NewOrderIdKonnective($_POST['orderId'], $upsellProductId, $shipping_price);
        $ret = json_decode($content);


        if ($ret->result == 'SUCCESS') {
            $data2 = $ret->message;

            $orderId = $data2->orderId;
            $_SESSION['ord_id2'] = $orderId;
                $_SESSION['purchased_items'][] = array('product_id' => $productId, 'time' => time(), 'return' => $content);
            $url = $campaign_path . 'thank-you.php?orderId=' . $orderId . '&upsell2=1&AFID=' . $AFID . '&SID=' . $SID . '&AFFID=' . $AFFID . '&C1=' . $C1 . '&C2=' . $C2 . '&C3=' . $C3 . '&AID=' . $AID . '&OPT=' . $OPT . '&click_id=' . $click_id . '&pid=' . $upsellProductId;
            header("Location:$url");
            die();
        } else {


            $errorMessage = "Please fix the following errors:<br>" . $ret->message;

            $url = $campaign_path . 'last-chance.php?orderId=' . $_POST['orderId'] . '&AFID=' . $AFID . '&SID=' . $SID . '&AFFID=' . $AFFID . '&C1=' . $C1 . '&C2=' . $C2 . '&C3=' . $C3 . '&AID=' . $AID . '&OPT=' . $OPT . '&click_id=' . $click_id . '&errorMessage=' . $errorMessage;

            header("Location:$url");
        }
    } else {

        $errorMessage = "Please fix the following errors:<br>" . $errordub;


        $url = $campaign_path . 'last-chance.php?orderId=' . $_POST['orderId'] . '&AFID=' . $AFID . '&SID=' . $SID . '&AFFID=' . $AFFID . '&C1=' . $C1 . '&C2=' . $C2 . '&C3=' . $C3 . '&AID=' . $AID . '&OPT=' . $OPT . '&click_id=' . $click_id . '&errorMessage=' . $errorMessage;

        header("Location:$url");
    }
    exit();
}
?>
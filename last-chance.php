<?php
@session_start();
error_reporting(0);
include('./kon/KonConfig.php');
$response = '';
$click_id = $_GET['click_id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex,nofollow"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ActivGuard - Last Chance</title>
    <meta name="robots" content="noindex">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <style>
        html{
            background-color: #f7f2eb;
        }

        body {

            background-color: #fffaef;
            background-position: center top;
            background-repeat: no-repeat;

        }
        /**/
        #teaser h2,strong {
            color: #588a17 !important;
            font-size:30px !important;
        }
        .well-shade {
            background-color: #eef7ff;
            border: 1px solid #8ec0ed;
            text-align: center;
            margin-bottom: 30px;
        }

        .action-btn {
            width: 300px;
            text-align: center;
            border: none;
            padding: 12px 0;
            display: block;
            height: 60px;
            border-radius: 30px;
            background: #ffe900;
            background: -webkit-linear-gradient(#ffe900 1%, #ffa90d 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #ffe900), to(#ffa90d));
            background: linear-gradient(#ffe900 1%, #ffa90d 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#358f0d', endColorstr='#ffb700', GradientType=0);
            color: #000;
            font-family: 'Roboto Slab', serif;
            font-size: 30px;
            font-weight: 500;
            text-shadow: 0px 1px 2px #000;
            line-height: 1;
            /*-webkit-box-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);*/
        }

        .well {
            min-height: 20px;
            padding: 20px 100px;
            margin-bottom: 20px;
            /*background-color: #eef7ff;*/
            background-color: #c1e4ff;
            border: 1px solid #8ec0ed;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
        }

        #teaser{
            background-image:none !important;
        }
        .content .well-shade h2 {
            font-size: 36px;
            font-weight: 700;
            color: #6f3b03;
        }
        .box {
            padding: 30px 25px !important;
        }
        .well-shade .intermediate {
            /*color: #588a17!important;
            display: block;
            font-size: 26px!important;
            margin: 3px 0;*/
            color: #004881 !important;
            display: block;
            font-size: 26px!important;
            margin: 3px 0;
            font-family: 'Bree Serif', serif;
        } {
            color: #6f3b03;
            display: block;
            font-size: 26px;
            margin: 3px 0;
        }
        h3.red {
            background-color: rgb(255, 47, 44);
            padding: 10px 0px;
            color: #fff;
            text-align: center;
            font-weight: 700;
            font-size: 30px;
        }
        .well-shade .intro {
            display: block;
            font-family: 'Roboto', sans-serif;
            font-size: 20px;
            font-weight: 300;
            margin-bottom: 10px;
        }
        .raleway_font {
            font-family: 'Raleway', sans-serif;
        }

        .roboto_font {
            font-family: 'Roboto', sans-serif;
        }

        .opensans_font {
            font-family: 'Open Sans', sans-serif;
        }
        .greenfont {
            color: #1f8541;
        }
        span.red2 strong {
            color: #004881 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
        }
        .bold_text{
            font-weight:bold;
        }
        p {
            font-size: 16px;
            color: #000;
            margin: 0 50px 20px;
        }
        .box-bg {
            background-color: #fffcf9;
            border: 1px solid #f7a298;
            font-weight: 700;
            padding: 15px 0;
        }
        .text-center {
            text-align: center;
        }
        .font_700{
            font-weight: 700 !important;
        }
        span.red2 {
            font-size: 26px;
            display: block;
            font-family: 'Bree Serif', serif;
        }
        .well-shade .glyphicon-check {
            font-size: 30px;
            color: #3b9001;
        }
        .glyphicon {
            position: relative;
            top: 1px;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        strong {
            /*color: #f33 !important;
            font-weight: 700;*/
            color: #004881 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
            font-size: 28px !important;
        }
        p a {
            color: #4472c4;
            text-decoration: underline;
        }
        .guarantee img {
            float: left;
            display: inline-block;
            margin-right: 10px;
            position: relative;
        }
        .guarantee p {
            font-size: 14px !important;
            color:#000 !important;
        }
        .no-thanks{
            color: #4472c4;
            text-decoration: underline;
            font-size:12px;
        }

        .hd1 {
            margin: 0 0 10px;
            font-size: 19px;
            color: #ffffff;
            background: #004881;
            border-radius: 25px;
            padding: 17px 40px;
            display: inline-block;
        }

        .hd1 {
            font-size: 30px !important;
        }

        .orange_text {
            color: #f26522 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
        }

        .green_color {
            color: #3b9001;
        }

        @media (max-width: 560px) {
            .well {
                padding: 20px 20px !important;
            }

            p {
                margin: 0 10px 20px;
            }
        }
    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<script src="/assets/js/Framework.js"></script>-->
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        function init() {
            if(isMobile.any())
                setTimeout(showButton, 2712000);//2775000
            else
                setInterval(function(){				  $('#overlay').show();			 }, 1000);
            setTimeout(showButton, 2712000);//2775000
            return;
        }
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WG3MPM3');</script>
    <!-- End Google Tag Manager -->

    <script type="text/javascript" src="js/jqeury.js"></script>
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript">
        //window.onbeforeunload = grayOut;
        function grayOut(){
            var ldiv = document.getElementById('LoadingDiv');
            ldiv.style.display='block';
        }
    </script>

</head>
<body>

<?php include_once("analyticstracking.php") ?>
<div id="LoadingDiv" style="display:none;">One Moment Please...<br /><img src="img/progressbar.gif" class="displayed" alt="" /></div>
<form method="POST" action="includes/upsell_process2.php" name="upsell" id="upsell2" >

    <?php
    if($_REQUEST['errorMessage'])
    {
        echo "<table width='100%'><tr><td style='background-color:#ff0000;color:#ffffff;font-size: 18px;font-family: arial,helvetica,sans-serif;font-weight:bold;height:50px;text-align: center;'>".urldecode($_REQUEST['errorMessage'])."</td></tr></table>";
    }
    ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <!--<h1><a href="https://activguardnow.com" onClick="">ActivGuard</a></h1>-->
                    <a href="https://activguardnow.com" onClick="window.onbeforeunload = null;"><img src="img/logo.png" class="img-responsive logo"></a>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div>
    </header>
    <section style="background-size:cover !important; background-repeat:no-repeat;" id="teaser">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box shade text-left">
                        <div class="text-center">
                            <h3 class="hd1">Wait… Your order is not complete!</h3>
                        </div>
                        <div class="well well-shade">
                            <span class="intro"><b>Do not hit the "Back" button as it can cause multiple charges to your card.</b></span>
                            <h3 class="greenfont">6 Bottles of ActivGuard Too Much?</h3>
                            <span class="intro">You Can Still Secure A HUGE Discount on ActivGuard…</span>
                            <img src="img/sp_product1-1.png" alt="" class="img-responsive center-block no-border">
                            <span style="font-size: 28px !important;" class="red2"><strong style="font-size: 28px !important;">WITH 3 ADDITIONAL BOTTLES INSTEAD OF 6</strong></span>
                            <span class="red2">Customers Only Price: <strike>$149</strike> <strong>$39</strong> <b>Per Bottle</b></span>

                        </div>
                        <div>

                            <p class="roboto_font">I understand that the special 6-bottle bonus offer of <b>ActivGuard</b> might not be for everyone...</p>
                            <p class="roboto_font">And I still want to give you the opportunity to secure a <span style="color:#f33;" class="bold_text">73% discounted price</span> on <b>ActivGuard</b> today.</p>
                            <p class="roboto_font"><span class="bold_text">But with a <span style="color:#f33;">3-bottle package</span> instead of 6 (still, at a DEEP discount)...</span></p>
                            <p class="roboto_font">Remember:</p>
                            <p class="roboto_font">Not only will your order of <b>ActivGuard</b> contain every proven ingredient you've just learned about...</p>
                            <p class="roboto_font">But all 19 of these ingredients will arrive at your doorstep, in the most optimized formula ever made. </p>

                            <!--<h3 class="box-bg text-center greenfont font_700">There’s Something Very Important That You Need To Know</h3>-->

                            <div class="well well-shade">
                                <h3 class="greenfont">You simply won't find a better blend of ingredients than</h3>
                                <h3 class="hd1">ActivGuard!</h3>
                                <span class="intro">That's why I want to give you this one-time opportunity to
add 3 more bottles of ActivGuard to your order…</span>
                                <span class="red2">For just <strike>$149</strike> <strong>$39 per bottle.</strong></span>
                                <span class="intermediate">That's an incredible savings of $110 per bottle!</span>
                            </div>

                            <p class="roboto_font">And it also ensures that you can have <b><u>steady bladder control.</u></b></p>

                            <p class="roboto_font"><b>How would you like to:</b> </p>
                            <p style="font-weight:bold;" class="roboto_font">- Feel safe in the knowledge that your prostate and bladder health will not let you down for many months to come? </p>
                            <p style="font-weight:bold;" class="roboto_font">- Reduce signs of aging, clear away incontinence, improve your productivity at work and increase your energy? </p>
                            <p style="font-weight:bold;" class="roboto_font">- Not have to bear the burden of doctor visits, needles, ‘screenings’ and dieting? </p>

                            <p class="roboto_font">If you so choose, you can have the time-tested, lab-verified health benefits…</p>
                            <p class="roboto_font">Day in, and day out… For as long as you want…</p>
                            <p class="roboto_font">While being protected from future price increases for each bottle of <strong>ActivGuard.</strong></p>
                            <p class="roboto_font">Now that's a big deal...</p>
                            <p class="roboto_font">Because these price increases are pretty much guaranteed to happen...</p>
                            <p class="roboto_font">Especially as more and more people experience <span style=" text-decoration:underline;" class="bold_text">the ‘endless bathroom break’ ending blend of all-natural ingredients</span> contained inside...</p>
                            <p class="roboto_font">In the meantime, the demand for the already scarce components found inside <strong>ActivGuard</strong> goes up…</p>
                            <p class="roboto_font">So the sooner you lock in your savings, the sooner you can have the peace of mind you need.</p>
                            <p class="roboto_font">Today, you can secure that opportunity & claim your <span style="text-decoration:underline; font-weight:bold;" class="bold_text">ridiculous discount</span>, without having to buy 6 bottles at 1 time.</p>

                            <div class="well well-shade">
                                <h3 class="greenfont">So to be clear, here's what you're getting with this One-Time-Only Offer:</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span class="glyphicon glyphicon-check"></span>
                                        <span class="intro">3 Additional Bottles of ActivGuard For Maximum Prostate Protection</span>
                                        <h4 class="roboto_font green_color"><strike>$149 per bottle</strike></h4>
                                        <strong>$39 per bottle!</strong>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="glyphicon glyphicon-check"></span>
                                        <span class="intro">Upgraded S&H: </span>
                                        <h4 class="roboto_font green_color"><strike>$9.99</strike></h4>
                                        <strong>Free</strong>
                                    </div>
                                </div>
                            </div>
                            <p class="roboto_font">Remember though: this is a one-time offer and it will not be available once you leave this page.</p>
                            <p class="roboto_font"><b>So go ahead and click the "Add to Order Now" button below...</b></p>
                            <p class="roboto_font">And secure the long-term benefits of <b>ActivGuard</b> now.</p>

                            <p><a href="./kon/complete_checkout.php?product_id=50" onClick="javascript:document.getElementById('upsell2').submit(),grayOut();">YES! Add 3 Bottles of ActivGuard to My Order at just $39 Each! That's a HUGE Savings of $110 per bottle. Plus, free shipping ($9.99 value) is included.</a></p>
                            <h3 class="text-center">Total Savings: <span class="bold_text">$330.00</span></h3>
                            <div style="" class="video_button"><center><a style="background: none !important;border: none !important;box-shadow: none !important;" href="./kon/complete_checkout.php?product_id=50" onClick="javascript:document.getElementById('upsell2').submit(),grayOut();"><img style="margin-top: 10px;" src="img/green_btn.png" /></a></center>
                                <div class="cards">
                                    <img src="img/cards.png" class="img-responsive center-block cards">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12 col-lg-8 col-lg-offset-2">
                                    <div class="guarantee">
                                        <img src="img/gurantee_img.png" alt="">
                                        <p class="roboto_font" style="padding-top: 20px;">We guarantee you’ll be completely satisfied with this product. If at any point you are not happy with the results, contact us for 100% money back within 180 days. No hassle, no questions asked.</p>
                                    </div>
                                </div>
                            </div>
                            <p><a href="thank-you.php"  class="no-thanks">No thanks. I understand that this is my only opportunity to get access to this special offer and I'm okay with missing out, even at this HUGE discount. Instead, if I'm blown away by the results I get from ActivGuard, like the 600,000+ happy customers from all across the world... I'll just re-order at $149 per bottle in the future, plus $9.99 shipping and handling costs. I will pass on this chance forever and have no regrets later.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer">
        <div class="container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a></li>

            </ul>
            <div class="disclaimer">
                <p>The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
            </div><br />
            <p>Copyright &copy; ActivGuard 2017</p>
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-73696839-3', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WG3MPM3"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <input type="hidden" name="campaign_id" value="7">
    <input type="hidden" name="product_id" value="50">
    <input type="hidden" name="product_qty" value="1">
    <input type="hidden" name="upsell" value="upsell_process2">
    <input type="hidden" name="shipping_id" value="">
    <input type="hidden" name="orderId" value="<?php echo $_REQUEST['orderId']; ?>" />
    <input type="hidden" name="sessionId" value="<?php echo $sessionId; ?>" />




    <?php
    foreach($_GET as $key => $value) {
        if($key == "click_id")
        {
            echo "<input type='hidden' name='click_id' value='".$click_id."'>";
        }
        else{
            echo "<input type='hidden' name='".safeRequestKonnective($key)."' value='".safeRequestKonnective($_GET[$key])."'>";
        }
    }
    ?>


</form>
<?php

if (count($_SESSION['purchased_items']) > 0) {
    foreach ($_SESSION['purchased_items'] as $v) {
        if (!in_array($v['product_id'], array(16, 17, 18)))
            continue;

        $pixel = $productArray[$v['product_id']]['pixel'];

        $returnArray = json_decode($v['return'], true);
        $orderId = $returnArray['message']['orderId'];
        if (is_array($pixel)) {
            foreach ($pixel as $pixel1) {
                ?>
                <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel1 ?>&t=<?php echo $orderId; ?>" height="1"
                        width="1" frameborder="0"></iframe>
                <?php
            }
        } else {
            ?>
            <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel ?>&t=<?php echo $orderId; ?>" height="1"
                    width="1" frameborder="0"></iframe>
            <?php
        }
    }
}
?>

</body>
</html>

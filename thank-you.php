<?php
@session_start();
error_reporting(0);
include('./kon/KonConfig.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="robots" content="noindex,nofollow"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ActivGuard - Thank You</title>
    <meta name="robots" content="noindex">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        html {
            background-color: #f7f2eb;
        }

        .container {
            max-width: 640px;
            background-color: transparent;
        }

        #header {
            padding-top: 15px;
            padding-bottom: 0px;
            border-bottom: 5px solid #385b1e;
            padding-right: 15px;
            padding-left: 15px;
        }

        .flag_img {
            float: right;
            padding: 15px 0px;
        }

        .header_container {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }

        body {
            background-color: #fffaef !important;
            background-image: none !important;
        }

        .top_menu_bar_shipping {
            padding: 0px 0px 0px 0px;
            clear: both;
        }

        .gray_box_with_border {
            background-color: #f7f7f7;
            border: 1px solid #e0dfdf;
            padding: 0px 10px;
            text-align: center;
        }

        .gray_box_without_border {
            background-color: #f0f0f0;
            padding: 0px;
            text-align: center;
            margin-right: 0px;
            margin-left: 0px;
            padding: 15px 0px;
        }

        .guarantee_box {
            background-color: #f0f0f0;
            border: 1px solid #d6d6d6;
            padding: 20px 20px 0px;
            border-radius: 5px;
        }

        .typage-btm {
            padding: 20px;
            text-align: center;

        }

        .roboto_c_font {
            font-family: 'Roboto Condensed', sans-serif;
        }

        .raleway_font {
            font-family: 'Raleway', sans-serif;
        }

        .roboto_font {
            font-family: 'Roboto', sans-serif;
        }

        .opensans_font {
            font-family: 'Open Sans', sans-serif;
        }

        .txt_16 {
            font-size: 16px;
        }

        .txt_17 {
            font-size: 17px;
        }

        .txt_18 {
            font-size: 18px;
        }

        .txt_20 {
            font-size: 20px;
        }

        .txt_21 {
            font-size: 21px;
        }

        .txt_22 {
            font-size: 22px;
        }

        .txt_24 {
            font-size: 24px;
        }

        .txt_28 {
            font-size: 28px;
        }

        .txt_30 {
            font-size: 30px;
        }

        .txt_32 {
            font-size: 32px;
        }

        .txt_40 {
            font-size: 40px;
        }

        .txt_44 {
            font-size: 44px;
        }

        .txt_46 {
            font-size: 46px;
        }

        .font_light {
            font-weight: 300;
        }

        .font_regular {
            font-weight: 400;
        }

        .font_medium {
            font-weight: 500;
        }

        .font_semibold {
            font-weight: 600;
        }

        .font_bold {
            font-weight: 700;
        }

        .font_italic {
            font-style: italic;
        }

        .darkgray {
            color: #3b3636;
        }

        .redfont {
            color: #ef2216;
        }

        .orangefont {
            color: #e44f00;
        }

        .orangefont_2 {
            color: #ed793c;
        }

        .darkgray_1 {
            color: #434343;
        }

        .darkgray_2 {
            color: #585858;
        }

        .darkgray_3 {
            color: #595959;
        }

        .darkgray_4 {
            color: #696969;
        }

        .darkgray_5 {
            color: #424141;
        }

        .darkgray_6 {
            color: #2f2f2f;
        }

        .darkgray_7 {
            color: #1e1c1c;
        }

        .darkgray_8 {
            color: #656565;
        }

        .light_gray {
            color: #838282;
        }

        .light_gray_2 {
            color: #f5f5f5;
        }

        .greenfont {
            color: #3b9001;
        }

        .want_bg {
            background-color: #f2f0c6;
            width: 100%
        }

        .greenfont_2 {
            color: #669a27;
        }

        .blackfont {
            color: #000;
        }

        .greenfont_2 {
            color: #43a300;
        }

        .whitefont {
            color: #fff;
        }

        .letterspace_7 {
            letter-spacing: 7px;
        }

        .padding_left_20 {
            padding-left: 20px;
        }

        .padding_top_45 {
            padding-top: 45px;
        }

        .padding_top_55 {
            padding-top: 55px;
        }

        .padding_bottom_55 {
            padding-bottom: 55px;
        }

        .top_padding_10 {
            padding-top: 10px;
        }

        .padding_left_25 {
            padding-left: 25px;
        }

        .padding_top_bottom_10 {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .no_padding {
            padding: 0px !important;
        }

        .text_right {
            text-align: right;
        }

        .text_left {
            text-align: left;
        }

        .text_center {
            text-align: center;
        }

        .section1 {
            padding: 25px 0px;
        }

        .section7 {
            padding-bottom: 20px;
        }

        .col_sm_8 {
            width: 66.6667%;
        }

        .col_sm_4 {
            width: 33.3333%;
        }

        .shipping_free {
            padding-right: 20px;
        }

        .section3 {
            padding: 20px;
            border-bottom: 4px double #ccc;
        }

        .section_heading {
            padding: 20px 0;
        }

        .panel {
            border: none;
        }

        .panel-heading {
            padding: 5px 15px !important;
        }

        .section4 .panel .panel-heading {
            background-image: url(img/green_bar_top_bg.png);
            background-size: contain;
            background-color: transparent;
            color: #fff;
            text-transform: uppercase;
            border: 1px solid #537c1d;
            border-bottom: none;
        }

        .section4 .panel-body {
            background-image: url(img/light_green_bg.png);
            border: 1px solid #537c1d;
            border-top: none;
            margin-top: -2px;
            border-radius: 5px;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            background-size: contain;
        }

        .linethrough {
            text-decoration: line-through;
        }

        .section5 .panel .panel-heading {
            background-image: url(img/gray_bar_bg_1.png);
            background-size: contain;
            background-color: transparent;
            color: #000;
            text-transform: uppercase;
            background-repeat: repeat;
            border: 1px solid #c7c7c7;
        }

        .section5 .panel-body {
            background-color: #f6f6f6;
            border: 1px solid #c7c7c7;
            border-top: none;
            margin-top: -2px;
            border-radius: 5px;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }

        .section6 .panel .panel-heading {
            background-image: url(img/gray_bar_bg_1.png);
            background-size: contain;
            background-color: transparent;
            color: #000;
            text-transform: uppercase;
            background-repeat: repeat;
            border: 1px solid #c7c7c7;
        }

        .section6 .panel-body {
            background-color: #f6f6f6;
            border: 1px solid #c7c7c7;
            border-top: none;
            margin-top: -2px;
            border-radius: 5px;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }

        .section8 {
            margin-bottom: 25px;
        }

        .right_section_greenbox {
            margin-bottom: 20px;
            -webkit-box-shadow: 0px 3px 10px -2px rgba(101, 101, 101, 0.75);
            -moz-box-shadow: 0px 3px 10px -2px rgba(101, 101, 101, 0.75);
            box-shadow: 0px 3px 10px -2px rgba(101, 101, 101, 0.75);
        }

        .footer-text {
            padding-left: 20px;
            padding-right: 20px;
        }

        .shipping_section_greenbox_body {
            background-color: #ffffff;
            padding: 0px 20px 10px;
        }

        .double_border {
            border-bottom: 4px double #6a9a30;
            padding-bottom: 20px;

        }

        .double_border_top {
            border-top: 4px double #6a9a30;
        }

        .right_section_header {
            text-align: center;
            background-image: url(img/right_sidebar_heading_bg.png);
            background-size: contain;
            border-radius: 5px;
            border-bottom-right-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .shipping_page select, .shipping_page input[type="text"] {
            padding: 10px;
            width: 100%;
            border: 2px solid #949494;
        }

        .form_label {
            padding-top: 15px;
            padding-bottom: 10px;
        }

        .what_this_space {
            padding-top: 8px;
            padding-bottom: 8px;
        }

        .shipping_page .footer_section {
            background-color: transparent;
            /*padding: 0px 45px 65px;*/
            padding: 0px 45px 25px;
        }

        .footer_text_container {
            width: 90%;
            margin: 0 auto;
        }

        .shipping_page .footer_menu_container {
            text-align: center;
            padding: 20px 0px 10px;
        }

        .shipping_page .footer_menu_container .list-inline li a, .footer_menu_separator {
            font-size: 24px;
            color: #fff;
            font-weight: 600;
            font-family: 'Open Sans', sans-serif;
        }

        .shipping_page .footer_menu_container li {
            padding-left: 0px;
            padding-right: 0px;
        }

        .copywright_text {
            text-align: center;
        }

        .copywright_text span {
            color: #cecdcd;
            font-family: 'Raleway', sans-serif;
            font-weight: 300;
            font-size: 14px;
        }

        .no_border {
            border: none !important;
        }

        .shipping_section_greenbox_body .field_inner {
            padding: 15px 0px;
        }

        .shipping_section_greenbox_body .form_fields {
            padding-top: 10px;
        }

        .logo_container img {
            max-width: 350px;
            height: auto;
        }

        .form_submit {
            max-width: 100%;
            padding-left: 20px;
            padding-right: 20px;
        }

        .orangefont {
            color: #ff9c00;
        }

        .product_row {
            padding-top: 20px;
        }

        .green_relief_product {
            margin-top: 80%;
        }

        .order_summary {
            margin-top: 50px;
        }

        .order_summary_table {
            margin-top: 32px;
        }

        .fullwidth {
            width: 100%;
        }

        .border_top {
            border-top: 1px solid #000;
        }

        .footer_menu_separator {
            color: #005696;
        }

        @media (min-width: 1200px) {
            .container {
                width: 1180px;
            }
        }

        @media (max-width: 768px) {
            .footer-text {
                padding-left: 20px !important;
            }

            .green_relief_product {
                margin-top: 50px;
                text-align: center;
            }
        }

        .logo_container {
            background-color: #fff !important;
            padding: 10px;
        }

        .logo_container img {
            max-width: 100% !important;
            height: auto;
        }

        #header {
            padding-top: 0px !important;
            margin-bottom: 0px !important;
        }

        /* codersea */
        .shipping_section_greenbox_body>.row>.order-receipt .oreder_summary_price_table{
            padding: 0;
        }
    </style>

</head>

<body class="shipping_page">
<?php include_once("analyticstracking.php") ?>
<div class="container header_container">
    <header id="header" class="no_border">
        <div class="row">
            <div class="col-sm-12 text_center logo_container">
                <a href="https://activguardnow.com"><img src="img/thanks_logo.png"/></a>
            </div>
            <div class="top_menu_bar_shipping">
                <img src="img/ordersummaryheader.png"/>
            </div>
        </div>
    </header>
</div>
<div class="container">
    <div class="row">

        <div>
            <div class="row">

                <div class="col-sm-12">
                    <div class="right_section_greenbox">

                        <div class="shipping_section_greenbox_body">
                            <div class="double_border">
                                <div class="text_center">
                                    <span class="opensans_font blackfont txt_40 font_bold font_italic">Thank You For Your Order</span>
                                </div>
                                <div class="text_center">

                                    <div><span style="font-size: 16px; color: red;"><strong>You will receive your confirmation e-mail within 30 minutes.</strong></span>
                                    </div>
                                </div>
                            </div>
                            <div product_row class="row padding_bottom_55">
                                <?php
                                $totalAmlount = 0.00;
                                if (count($_SESSION['purchased_items']) > 0):
                                    $i = 0;
                                    foreach ($_SESSION['purchased_items'] as $v):
                                        // var_dump($v['product_id']);
                                        // var_dump($productArray[$v['product_id']]['name']);
                                        // continue;
                                        $i++;;
                                        $v['time'];
                                        $v[''];

                                        $name = $productArray[$v['product_id']]['name'];
                                        $image = $productArray[$v['product_id']]['img'];
                                        $pr_price = $productArray[$v['product_id']]['price'];
                                        $total = $pr_price + 0.00;
                                        $totalAmlount += $total;
                                        $returnArray = json_decode($v['return'], true)
                                        ?>
                                        <div class="col-sm-4">
                                            <div class="green_relief_product product-<?php echo $i; ?>">
                                                <img src="./kon/<?php echo $image; ?>" alt="<?php echo $name ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-8 order-receipt">
                                            <div class="order_summary">
                                                <span class="opensans_font font_semibold txt_24"
                                                      style="color: #005599;">ORDER RECEIPT</span>
                                                <div class="order_summary_table fullwidth">
                                                    <table class="fullwidth">
                                                        <tr>
                                                            <td>
                                                                <span class="opensans_font font_semibold blackfont txt_16">Order placed:</span>
                                                            </td>
                                                            <td class="text_right"><span
                                                                        class="opensans_font font_regular blackfont txt_16"><?php echo date('l jS \of F', $v['time']); ?></span>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="top_padding_10"><span
                                                                        class="opensans_font font_semibold blackfont txt_16">Transaction ID:</span>
                                                            </td>
                                                            <td class="top_padding_10 text_right"><span
                                                                        class="opensans_font font_regular blackfont txt_16">&nbsp;<?php echo $returnArray['message']['orderId']; ?></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="top_padding_10"><span
                                                                        class="opensans_font font_semibold blackfont txt_16">Items ordered:</span>
                                                            </td>
                                                            <td class="top_padding_10 text_right"><span
                                                                        class="opensans_font font_regular greenfont_2 txt_16">&nbsp;<?php echo $name; ?></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="oreder_summary_price_table padding_top_45 fullwidth padding_bottom_55">
                                                    <table class="fullwidth border_top">
                                                        <!-- <tr>
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_semibold blackfont txt_16">ActivGuard <?php echo $_SESSION['product_name']; ?></span></td>
                                                            <td class="text_right"><span class="opensans_font font_semibold greenfont_2 txt_16">$<?php echo number_format($_SESSION['pr_price'], 2); ?></span></td>
                                                        </tr> -->


                                                        <tr class="border_top">
                                                            <td class="padding_top_bottom_10"><span
                                                                        class="opensans_font font_regular blackfont txt_16">Pricing & Handling:</span>
                                                            </td>
                                                            <td class="top_padding_10 text_right"><span
                                                                        class="opensans_font font_regular blackfont txt_16"><?php echo '$' . $pr_price ?></span>
                                                            </td>
                                                        </tr>

                                                        <tr class="border_top">
                                                            <td class="padding_top_bottom_10"><span
                                                                        class="opensans_font font_regular blackfont txt_16">Shipping & Handling:</span>
                                                            </td>
                                                            <td class="top_padding_10 text_right"><span
                                                                        class="opensans_font font_regular blackfont txt_16">$0.00</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="double_border_top">
                                                            <td class="top_padding_10"><span
                                                                        class="opensans_font font_semibold blackfont txt_16">Total:</span>
                                                            </td>
                                                            <td class="top_padding_10 text_right"><span
                                                                        class="opensans_font font_semibold blackfont txt_16"><?php echo '$' . $total ?></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>


                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="want_bg">
                            <div class="typage-btm">
                                <p><span style="font-size: 24px; color: red;"><strong>Want to Spread the Word About ActivGuard?</strong></span><br>
                                    <span style="font-size: 20px;">Do you know someone who could benefit from ActivGuard?</span><br>
                                    <a class="link"
                                       href="mailto:?subject=Checkout This Video I Just Watched&amp;body=Hey!%0A%0A&#10;I just watched a fascinating video and wanted to share it with you. It’s an all natural supplement, whose ingredients have been shown to promote bladder health, shrink the male and female prostate, and eradicate both sudden and nagging feelings of urgency, and incontinence.%0A%0A&#10;Here’s a link to the video so you can watch it now! - https://activguardnow.com/ "
                                       title="Share by Email">
                                        <span style="font-size: 18px;">Click here to email our presentation to your friends and loved ones.</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="field_inner text_left footer-text" style="padding-top: 0px;">
                            <span class="opensans_font font_italic font_regular txt_14">The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</span>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="footer_section row">
        <!--<div class="footer_text_container text_center">
            <p class="raleway_font font_light txt_17 light_gray_2">The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
        </div>-->
        <div class="footer_menu_container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank"
                       style="color: #005696;">Terms &amp; Conditions</a></li>
                <li><span class="footer_menu_separator">&nbsp;&nbsp;|&nbsp;&nbsp;</span><a style="color: #005696;"
                                                                                           onClick="window.onbeforeunload = null;"
                                                                                           href="privacy.html"
                                                                                           target="_blank">Privacy</a><span
                            class="footer_menu_separator">&nbsp;&nbsp;|&nbsp;&nbsp;</span></li>
                <li><a style="color: #005696;" onClick="window.onbeforeunload = null;" href="contact-us.html"
                       target="_blank">Contact Us</a></li>
            </ul>
        </div>
        <div class="copywright_text">
            <span style="color: #005696;">Copyright &copy; ActivGuard 2017</span>
        </div>
    </div>

</div>
<?php

?>


<?php

if (count($_SESSION['purchased_items']) > 0) {
    foreach ($_SESSION['purchased_items'] as $v) {


        $pixel = $productArray[$v['product_id']]['pixel'];

        $returnArray = json_decode($v['return'], true);
        $orderId = $returnArray['message']['orderId'];
        if (is_array($pixel)) {

            foreach ($pixel as $pixel1) {
                ?>
                <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel1 ?>&t=<?php echo $orderId; ?>" height="1"
                        width="1" frameborder="0"></iframe>
                <?php
            }
        } else {
            ?>
            <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel ?>&t=<?php echo $orderId; ?>" height="1"
                    width="1" frameborder="0"></iframe>
            <?php
        }
    }
}
?>

</body>
</html>

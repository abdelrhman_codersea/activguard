//window.onbeforeunload = grayOut;
function grayOut() {
    var ldiv = document.getElementById('LoadingDiv');
    ldiv.style.display = 'block';
}
function scrollTopPage(){
    $(window).resize(function() {
        if($(window).width() >= 768) {
            $("html, body").stop().animate({scrollTop:200}, 1000, 'swing');
        } else {
            $("html, body").stop().animate({scrollTop:820}, 1000, 'swing');
        }
    }).resize();
}

$(document).ready(function () {

    var min1 = 9;
    var second1 = 59;
    var zeroPlaceholder1 = 0;
    function countDown() {
        second1--;
        if (min1 > 0) {
            if (second1 == -1) {
                second1 = 59;
                min1 = min1 - 1;
            }
            if (second1 >= 10) {
                zeroPlaceholder1 = '';
            } else if (second1 < 10) {
                zeroPlaceholder1 = 0;
            } else {
                if (second1 == 00) {
                    zeroPlaceholder1 = 0;
                }
            }
            $('.count-up').html(min1 + ':' + zeroPlaceholder1 + second1);
        } else {
            $('.count-up').html('0:00');
        }
    }

    var counterId = setInterval(function () {
        countDown();
    }, 1000);

    /******************************************************************************/
    /* faq */
    $('#step1').css('min-height',$(window).height()-325);
    $faqPanels = $('.panel-group#faq-container .panel');
    $faqPanels.find('.panel-heading .panel-title').click(function () {
        $faqPanels.removeClass('active');
        $(this).parents('.panel').addClass('active');
    });
});
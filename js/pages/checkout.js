function onlyNumbers(e,type)
{
   var keynum;
   var keychar;
   var numcheck;
   if(window.event) // IE
   {
      keynum = e.keyCode;
   }
   else if(e.which) // Netscape/Firefox/Opera
   {
      keynum = e.which;
   }
   keychar = String.fromCharCode(keynum);
   numcheck = /\d/;

   switch (keynum)
   {
      case 8:    //backspace
      case 9:    //tab
      case 35:   //end
      case 36:   //home
      case 37:   //left arrow
      case 38:   //right arrow
      case 39:   //insert
      case 45:   //delete
      case 46:   //0
      case 48:   //1
      case 49:   //2
      case 50:   //3
      case 51:   //4
      case 52:   //5
      case 54:   //6
      case 55:   //7
      case 56:   //8
      case 57:   //9
      case 96:   //0
      case 97:   //1
      case 98:   //2
      case 99:   //3
      case 100:  //4
      case 101:  //5
      case 102:  //6
      case 103:  //7
      case 104:  //8
      case 105:  //9
         result2 = true;
         break;
      case 109: // dash -
         if (type == 'phone')
         {
            result2 = true;
         }
         else
         {
         result2 = false;
         }
      break;
      default:
         result2 = numcheck.test(keychar);
         break;
   }

   return result2;
}

$(document).ready(function () {
    
    $('#checkout').css('min-height',$(window).height()-325);

    /******************************************************************************/
    /* select product */
    $buttonsPackage = $('#buttons-package [class*="btn-package-"]');
    $buttonsPackage.click(function () {
        $buttonsPackage.removeClass('active');
        $(this).addClass('active');
        packagePreview = $(this).attr('data-target');
        $('.product-preview').hide();
        $(packagePreview).show();
    });
    productId = $('[name="prc_id"]').val();
    $('[data-product="'+productId+'"]').click();

    /******************************************************************************/
    /* Billing */
    $('#Billing input[type="checkbox"]').click(function () {
        $('#billingsame').toggle();
    });

    /******************************************************************************/
    /* counter */
    setInterval(function () {
        countDown();
    }, 1000);

});

var second1 = 59;
var zeroPlaceholder1 = 0;
function countDown() {
    second1--;
    if (min1 > 0) {
        if (second1 == -1) {
            second1 = 59;
            min1 = min1 - 1;
        }
        if (second1 >= 10) {
            zeroPlaceholder1 = '';
        } else if (second1 < 10) {
            zeroPlaceholder1 = 0;
        } else {
            if (second1 == 00) {
                zeroPlaceholder1 = 0;
            }
        }
        jQuery('#counter').html(min1 + ':' + zeroPlaceholder1 + second1);
    } else {
        jQuery('#counter').html('0:00');
    }
}
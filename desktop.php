<?php @session_start();
error_reporting(0);
include('./kon/KonConfig.php'); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="Expires" content="30"/>
    <meta name="robots" content="noindex,nofollow"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk"/>
    <meta name="viewport"
          ontent="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>ActivGuard</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/sass/main.min.css">
</head>
<body class="beige-color">
<header>
    <div class="container">
        <div class="col-lg-4 col-xs-12">
            <a href="./">
                <img src="./img/logo.png" id="logo">
            </a>
        </div>
        <div class="col-lg-4 col-xs-12 pull-right" id="sound-on">
            <i class="fa fa-volume-up"></i><span> Remember to turn your sound on!</span>
        </div>
    </div>
</header>
<div id="desktop">
    <div id="vid-container">
        <div class="container">
            <h5 class="vid-subTitile">(Please wait up to 3 seconds for the video to load)</h5>
            <h5 class="vid-subTitile">Warning: The following information is extremely controversial, and based on
                the
                opinion of the narrator.</h5>
            <div id="vid-title">
                <h1>America Might Be On The Verge of a 2nd Civil War…</h1>
                <h1>Will You Be Ready When Chaos Erupts?</h1>
            </div>
            <div id="main-vid">
                <div class="col-md-10 col-md-offset-1">
                    <div class="overlay"></div>
                    <div class="vid-effect">
                        <div class="embed-res">
                            <div class="videoContainer">
                                <iframe src="https://www.youtube.com/embed/kJhZFaSoZ-0?rel=0&modestbranding=1&controls=0&showinfo=0&autoplay=1&vq=medium"
                                        allowfullscreen="" width="100%" height="100%" frameborder="0">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="get-activguard" class=" col-lg-8 col-lg-offset-2 col-xs-12">
                <a href="./step1.php">
                    <img src="./img/ActivGuard/green_btn.png" alt="Get ActivGUARD Now">
                </a>
                <div class="cards col-lg-6 col-lg-offset-3 col-xs-8 col-xs-offset-2">
                    <img src="./img/cards.png" alt="Cards">
                </div>
            </div>
        </div>
    </div>
    <section id="TSL">
        <div class="container">
            <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                <div id="TSL-container">
                    <h2 class="text-dark text-bold text-center ">My Wife’s "New Weird Body Part" & <br>
                        How Discovering It Eradicated Our Over Active Bladders</h2>

                    <p>I was at my nephew’s graduation when the final straw BROKE…</p>

                    <p>My brother just recently passed away…</p>

                    <p>And I know my brother’s son felt pretty alone in the world…</p>

                    <p>It made me proud to step in as a father figure at his hard-fought college graduation…</p>

                    <p>Represent myself and my brother on a happy occasion… </p>

                    <p>But then… just as my nephew’s name was about to be called…</p>

                    <p><strong class="text-dark">I HAD to run to the bathroom.</strong></p>

                    <p><strong class="text-dark">Even though I had went less than an hour before.</strong></p>

                    <p>And even though my oldest nephew, son of my closest brother, was about to receive the diploma
                        that
                        meant so
                        much to him, his dad, and me…</p>

                    <p>I couldn’t sit in the audience a moment longer.</p>

                    <p>I had to get up and walk away.</p>

                    <p>Miss his moment.</p>

                    <p>Picture his face looking out for me in the audience.</p>

                    <p>His own father couldn’t be there… and now neither could I?</p>

                    <h3 class="box-bg text-center greenfont font_700">Because of a stupid need to pee… again???</h3>

                    <p>While my wife understood my irritation and embarrassment (she’s had similar issues for
                        years…)</p>

                    <p>I knew that I let both my nephew and my brother down… </p>

                    <p>I was at a peak of frustration with myself, and terrified about this getting even worse and
                        developing into a
                        serious health problem…</p>

                    <p><strong class="text-dark">That was the day I decided to do whatever it took to eliminate my
                            stupid
                            “bladder problem” forever.</strong></p>

                    <h2 class="text-dark text-bold text-center ">And what I found was the little-known way to eliminate
                        your
                        nagging need to go, for good…</h2>

                    <ul type="none" class="pl-0">
                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                A bizarre but incredibly effective, all-natural remedy that gives you complete control of your bladderagain.
                            </div>
                        </li>
                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Leaving you feeling comfortable, confident, and NORMAL, with no nagging urges anymore.
                            </div>
                        </li>
                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                It’s worked for thousands of people across the U.S and counting.
                            </div>
                        </li>
                    </ul>

                    <p>And when you see just how simple it is, you might kick yourself for not knowing about it
                        earlier.</p>

                    <p>But be glad you’re discovering it now.</p>

                    <p>Because getting this powerful bladder elixir today can mark a true turning point for you.</p>

                    <p>Where you can live each day comfortably from here on out…</p>

                    <div class="well well-shade">
                        <h3 class="greenfont font_700"><strong>In total control of your bladder, without having to even
                                think about it.</strong></h3>
                        <center><img src="./img/review.png"></center>
                    </div>

                    <p>Just like what happened to Dave from Illinois, who writes…</p>

                    <p>“Wow. This marks 7 days since I’ve started. 5 days with no feelings of urgency at all. I forgot
                        what
                        it was
                        like to go to the bathroom like a normal person. My life is really changed for the better, and I
                        am
                        so, so
                        grateful.”</p>

                    <p>Or Janice from San Diego, who wrote in…</p>

                    <p>“I was ready to accept the fact that I’d have this problem forever, but now it is totally gone. I
                        stopped
                        buying protective pads, I go to movies without even thinking about it… having to get up multiple
                        times
                        during a movie used to embarrass me so much, but now I really feel like that part of my life is
                        over. I also
                        sleep all through the night now without having to get up to go to the bathroom. THANK YOU!”</p>

                    <p>I could share letters from hundreds of people just like these.</p>

                    <p>And what’s most important is that YOU find the same relief they did. The same…</p>

                    <h2 class="text-dark text-bold text-center ">Life-changing renewed confidence, freedom, and
                        happiness
                        that you can have starting today.</h2>

                    <p>Because listen… if you’ve been:</p>

                    <ul type="none" class="pl-0">
                        <li class="row mb-20">
                            <i class="fa fa-times text-red col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Waking up in the night… Sometimes multiple times… Destroying your sleep…
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-times text-red col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Missing out on moments with your loved ones...
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-times text-red col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Dealing with... or even contemplating... the embarrassment of wearing a pad or a diaper...
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-times text-red col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Or just not being able to focus on life's good times...
                            </div>
                        </li>
                    </ul>

                    <p>All because of a constant need to go…</p>

                    <h3 class="box-bg text-center greenfont">…then it’s time for your <br>
                        <span><strong><u>completely UNNECESSARY suffering</u></strong></span> to stop…
                    </h3>

                    <p>Now, to be honest, it took time for me to feel comfortable sharing this remedy with the
                        world.</p>

                    <p>It’s embarrassing to admit, “I have bladder troubles”… It’s seen as a sign of aging, and just not
                        cool at
                        all…</p>

                    <p>You begin to ask yourself some scary questions, like…</p>

                    <p>What if the problem never stopped…</p>

                    <p>What if the urgency became constant… with a “need to go feeling” always stealing my focus and
                        attention?</p>

                    <p><strong class="text-dark">Will I ever get a full night’s sleep, without waking up multiple times
                            to
                            pee?</strong></p>

                    <p>What if I lost control? Or was ever “too late” to a restroom?</p>

                    <p>What if this problem makes it harder for my wife to love me?</p>

                    <p>Or harder to find someone special in the first place?</p>

                    <p>What if this escalates into a real medical issue that can take years off my life?</p>

                    <p>Not only do you have these bladder troubles, but you become stressed as heck about all of this
                        other
                        stuff.</p>

                    <p>So I know just how hard it is to face the reality that you have an issue.</p>

                    <h3 class="box-bg text-center greenfont">And when I realized that the so-called cures most “doctors”
                        give you don’t work…</h3>

                    <p>That they actually hurt your health and overall make you feel worse… </p>

                    <p><strong class="text-dark">Giving you bad side effects like grogginess and lack of
                            libido…</strong>
                    </p>

                    <p>It became all the more important for me to step forward and reveal the facts about your constant
                        need
                        to
                        go.</p>

                    <p>The good news is that this issue has been PROVEN to be easier to treat than you might think. </p>

                    <p>That’s why it’s so important for both your health and your quality of life that you pay very
                        close
                        attention
                        here.</p>

                    <p><strong class="text-dark">Because I do believe that “big pharma” and the billion dollar health
                            industry WANTS to keep the path to
                            urgency-relief complicated and confusing.</strong></p>

                    <p>Letting us all believe that overactive bladder, and even incontinence, are just signs of aging
                        that
                        are too
                        “complex” to cure…</p>

                    <p>… or just plain unavoidable.</p>

                    <p>… telling us to do silly sex exercises like Keagles… or to even have complicated, painful
                        surgery.</p>

                    <p><strong class="text-dark">I’m sure the makers of all those God-awful “undergarments” would like
                            us to
                            believe there’s nothing we can do
                            to regain bladder health and control.</strong></p>

                    <p>This all means that I could be sanctioned to take this website down at any time.</p>

                    <p>Because it shares the TRUE, natural, simple source of relief from your urinary issues.</p>

                    <p>Nutrients proven by numerous clinical studies the world over.</p>

                    <h3 class="box-bg text-center greenfont">And heck… if a handful of natural herbs and vitamins that
                        you
                        can
                        <span><strong>pick up from your closest store right now</strong></span>
                        can <span><strong><u>eliminate your nagging need to go…</u></strong></span><br>
                        Then shoot…<span><strong><u>you NEED to know</u></strong></span> about it, right?</h3>

                    <p>Of course.</p>

                    <p>So let me get started…</p>

                    <p>Hi, I’m Tom Mapler, and I’ll admit, I discovered this remedy in a pretty weird way…</p>

                    <p>It began with finding out that my wife, and all women…</p>

                    <p>Actually have a prostate just the same as men do.</p>

                    <p>And because both of our prostate glands were enlarged, we’d been suffering with urgency for
                        YEARS.</p>

                    <p><strong class="text-dark">Now that we both take the nutrients I’m about to share with you, we
                            don’t
                            have to deal with ANY abnormal urgency.</strong></p>

                    <p>You heard that right… NO more urgency.</p>

                    <p>Since the nutrients we take come right from mother Earth, there are no scary side-effects…</p>

                    <h3 class="box-bg text-center greenfont">And when you take these nutrients in the specific way I’m
                        about
                        to share, you could become in total control of your bladder… Like you were in your 20s all over
                        again…</h3>

                    <p>PLUS, instead of side-effects, there are dozens of side BENEFITS, like better digestive health,
                        mental
                        clarity, even stronger bones and muscles, which surprised me.</p>

                    <p>After the true shame that I felt over my nephew’s graduation, I got to work, researching
                        everything I
                        possibly could about the topic of urinary and bladder health.</p>

                    <p><strong class="text-dark">It’s actually pretty interesting, and here’s how things work:</strong>
                    </p>

                    <p>First… Urine enters your bladder from your kidneys, and as your bladder fills up, nerve signals
                        get
                        sent to
                        your pelvic muscle…</p>

                    <p>This causes your pelvic muscle to contract.</p>

                    <p>That’s the “feeling” of having to go to the bathroom… a series of contractions in your pelvic
                        muscle….</p>

                    <p>When it works normally, you feel the feeling, and have ample time to comfortably make it to the
                        bathroom.</p>

                    <p>You enter your whole bladder and continue about your day.</p>

                    <p>And normally this happens 6 – 10 times a day.</p>

                    <p>All standard biological function.</p>

                    <p>But what goes wrong somewhere along the way, often as we get older…</p>

                    <p>…Is that those nerve signals that make our pelvis contract get activated more and more…</p>

                    <p>Even when your bladder is only a little full.</p>

                    <p>The nerve signals can also come out of nowhere and feel very intense very fast.</p>

                    <p>What causes the nerve signals to fire more?</p>

                    <p><strong class="text-dark">One of the main reasons our nerve signals fire is more is due to
                            pressure
                            from a swollen prostate.</strong></p>

                    <p>But “swollen prostate” I thought, only applied to me.</p>

                    <p>It didn’t explain my wife’s consistent need to go, and why she sometimes goes a little when she
                        laughs really
                        hard…</p>

                    <p>See… the whole time I was trying to look for two separate things at once: a way for men to get
                        relief, and a
                        way for women to get relief.</p>

                    <p><strong class="text-dark">I thought that nutrients that reduce prostate swelling could help men,
                            and
                            that there was some “other” remedy
                            for women.</strong></p>

                    <p>Until my own wife told me, “women have a prostate, silly.”</p>

                    <p>Turns out that the female prostate is commonly called “Skene’s gland”.</p>

                    <p><strong class="text-dark"><u>And as we age, the female prostate can swell JUST like the men’s
                                does. </u></strong></p>

                    <p>Putting pressure on the bladder and causing the sense of urgency we want to be rid of.</p>

                    <p>This discovery was like a light bulb turning on.</p>

                    <p>There aren’t two separate solutions.</p>

                    <h3 class="box-bg text-center greenfont">There is ONLY one thing we need to do: reverse and prevent
                        prostate swelling.</h3>

                    <p><strong class="text-dark">And all of the tortures of an over-active bladder and incontinence can
                            go
                            away for good.</strong></p>

                    <p>As soon as I searched for all-in one relief for prostate swelling, I discovered the brand B
                        Naturals.</p>

                    <p>Now the good folks, doctors, and scientists at B Naturals already knew about the female
                        prostate…</p>

                    <p>AND how to shrink an enlarged prostate all naturally.</p>

                    <h3 class="box-bg text-center greenfont">See… there is a handful of very common nutrients and
                        vitamins
                        that have been scientifically PROVEN to help reduce prostate swelling in men and women
                        alike.</h3>

                    <p>And…</p>

                    <p>When these nutrients are taken in the proper proportions…</p>

                    <p>They can begin to shrink the prostate IMMEDIATELY…</p>

                    <p><strong class="text-dark">Meaning you can notice your sense of urgency decreasing right away –
                            which
                            is truly incredible. </strong></p>

                    <p>So what are these prostate shrinking nutrients? </p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #1: Zinc</h3>

                    <center><img src="./img/1.Zinc.jpg" style="width: 60%;"></center>

                    <p>Zinc has been shown in numerous studies to promote prostate health and to shrink the prostate
                        gland. </p>

                    <p>And Rush University Medical Center in Chicago reports that Zinc, “not only prevents prostate
                        enlargement” and
                        can “help shrink a prostate gland that’s already swollen.</p>

                    <p>And you can find Zinc in foods like pumpkin seeds, oysters, nuts and beans.</p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #2: Lycopene</h3>

                    <center><img src="./img/2.Lycopene.jpg" style="width: 60%;"></center>

                    <p>Numerous studies, including ones published by the American Journal of Health, indicate that 10 –
                        15
                        mg of
                        lycopene daily can promote prostate health and shrink an enlarged prostate. </p>

                    <p>And in one clinical trial, individuals with enlarged prostates who were given lycopene saw up to
                        an
                        18%
                        reduction in the problematic protein called Prostate-Specific Antigen, which plays a key role in
                        swelling.</p>

                    <p>In the same study, those who were given a placebo, or sugar pill, saw no effects.</p>

                    <p>Lycopene is found in tomatoes and tomato based products. </p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #3: Vitamin E</h3>

                    <center><img src="./img/3.VitaminE.png" style="width: 60%;"></center>

                    <p>In just one of numerous studies, the United States Department of Agriculture reports that Vitamin
                        E
                        has been
                        proven to inhibit prostate growth.</p>

                    <p>Plus, their findings also indicate that individuals taking Vitamin E supplements can have a
                        decreased
                        risk of
                        prostate growth.</p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #4: Stinging Nettle</h3>

                    <center><img src="./img/4.StingingNettle1.jpg" style="width: 60%;"></center>

                    <p>Stinging Nettle is widely used throughout Europe to treat an enlarged prostate…</p>

                    <p>And its effects have been proven to be comparable to prescription drugs commonly prescribed for
                        an
                        enlarged
                        prostate.</p>

                    <p>As the University of Maryland Medical Center reports, Stinging Nettle has been shown to “slow the
                        growth of
                        prostate cells,” reducing symptoms such as “reduced urinary flow, incomplete emptying of the
                        bladder, post
                        urination dripping, and the constant urge to urinate.” </p>

                    <p>This makes Stinging Nettle one of the most powerful all-natural incontinence remedies. Others
                        include… </p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #5: Maitake Mushroom:</h3>

                    <center><img src="./img/5.MaitakeMushroom2.jpg" style="width: 60%;"></center>

                    <p>The US National Library of Medicine has reported that concentrations of Maitake Mushroom have
                        eliminated up
                        to 95% of prostate growth cells! </p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #6: Reishi Mushroom:</h3>

                    <center><img src="./img/6.ReishiMushroom.jpg" style="width: 60%;"></center>

                    <p>Chinese tradition refers to the Reishi mushroom (Ganoderma lucidum) as ‘the lucky fungus’, for
                        its
                        powers in
                        alleviating health issues for over 2,000 years! And researchers from the University of Haifa
                        announced their
                        success in preliminary efforts to slow the growth of prostate cells using reishi extracts.</p>

                    <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #7: Saw Palmetto:</h3>

                    <center><img src="./img/7.SawPalmetto.jpg" style="width: 60%;"></center>

                    <p>With hundreds of studies proving the effectiveness of Saw Palmetto in treating urinary issues,
                        one of
                        the
                        most incredible findings is that Saw Palmetto can be as effective at eliminating urgency and
                        incontinence as
                        prescription drugs, WITHOUT the common side effects of a lost libido.</p>

                    <p>Some of the other most potent all-natural nutrients that have been shown to help prostate health
                        are: </p>

                    <p><strong class="text-dark">Vitamin B-6, Shiitake Mushroom, Beta-Sitosterol, Red Raspberry Juice
                            Extract, Graviola, and Broccoli Extract. </strong></p>

                    <p>These have been proven effective in HUNDREDS of scientific studies just like those we’ve already
                        discussed.</p>

                    <h3 class="box-bg text-center greenfont">Now, before you run off to the store to load up on Zinc,
                        Vitamin B-6, Vitamin E, Selenium, Copper, Saw Palmetto, Red Raspberry Juice Extract, and so
                        on…<br><br>(Which would cost you a <br>few hundred bucks in total)<br><br>I’m about to make your
                        life a heck of a lot easier.</h3>

                    <p>See… instead of buying a bunch of different vitamins and supplements…</p>

                    <p>And trying to figure out the proper dose of each to ensure you reap the benefits of a healthy and
                        normal
                        bladder…</p>

                    <p><strong class="text-dark">The team of experts at B Naturals have combined the purest forms of
                            these
                            nutrients, in the perfect dosages,
                            all into one tiny, easy-to-swallow capsule, called ActivGuard.</strong></p>

                    <center><img src="./img/multibottle.png"></center>

                    <p>As soon as I found ActivGuard and learned that ALL of its ingredients are carefully sourced and
                        designed to
                        shrink the prostate and promote prostate health, I knew I had to try it for me and my wife…</p>

                    <p>And just like thousands of other people who’ve tried ActivGuard, once we started, we never looked
                        back.</p>

                    <h3 class="box-bg text-center greenfont">I could feel a difference the very same day I started
                        taking
                        ActivGuard. It was subtle at first… but I noticed that I didn’t wake up in the night to use the
                        bathroom.</h3>

                    <p>I thought it might be a fluke, but when my wife and I went to the movies that Friday night,
                        neither
                        of us had
                        to take a bathroom break.</p>

                    <p><strong class="text-dark"><u> It had been years since we both sat in the theater for a full
                                movie,
                                without missing a second.</u></strong></p>

                    <p>Needless to say, we were thrilled. </p>

                    <p>And we’ve been thrilled ever since.</p>

                    <p>We both take ActivGuard every day, and in addition to having control of our bladders’ again, we
                        also
                        both
                        feel healthier…</p>

                    <p><strong class="text-dark">With more energy, mental clarity, and stamina…</strong></p>

                    <p><strong class="text-dark">Which makes sense, because the nutrients inside ActivGuard are pure,
                            powerful, and great for you.</strong></p>

                    <h3 class="box-bg text-center greenfont">ActivGuard is made in a state of the art facility right
                        here in
                        the United States.</h3>

                    <p>And every single bottle of ActivGuard is quality screened by a third party, FDA compliant
                        facility,
                        also
                        right here in the United States.</p>

                    <p>This ensures that each bottle of ActivGuard is packed with the prostate-shrinking ingredients
                        that
                        are
                        written on the label.</p>

                    <p>The exact ingredients that have been proven to improve your sense of well-being by optimizing
                        your
                        prostate
                        health.</p>

                    <h3 class="box-bg text-center greenfont">What this means is, right here on this page, you have the
                        opportunity to <span><strong>effortlessly eliminate your urinary problems, the urges to go, and your incontinence.</strong></span>
                    </h3>

                    <p>This can make you feel years younger, like a weight has been lifted off your shoulders.</p>

                    <p><strong class="text-dark">You can feel free to take long road trips, sit for a long movie, or a
                            long
                            dinner date.</strong></p>

                    <p><strong class="text-dark">Without any interruption or worry.</strong></p>

                    <p>And as mentioned earlier, ActivGuard is PACKED with all-natural ingredients, meaning there are no
                        side
                        effects… and even cooler… there are awesome side-benefits… </p>

                    <p>Just to name a few:</p>

                    <center><img src="./img/natural_ingredients.jpg"></center>

                    <p>The Quercetin found inside ActivGuard’s proprietary blend has been proven to reduce symptoms
                        associated with
                        chronic prostatitis/chronic pelvic pain syndrome.</p>

                    <p>The Buchu Leaf - also found inside - has been held in great esteem by the natives of the Cape of
                        Good
                        Hope,
                        as a remedy for a number of diseases, particularly irritative or chronic inflammatory affections
                        of
                        the
                        urethra, bladder, prostate gland, and rectum.</p>

                    <p>And the Gravel Root – an herbal medicine – is used to treat kidney stones, prostate gland
                        problems,
                        rheumatism (stiff and sore muscles and joints) and gout (swelling due to thyroid problems). </p>

                    <h3 class="box-bg text-center greenfont">So by now you might be wondering how to get your hands on
                        ActivGuard yourself.</h3>

                    <p>The team at B Naturals spares no expense in sourcing the purest and highest quality forms of raw
                        materials on
                        the planet. </p>

                    <p><strong class="text-dark"><u> Because of this, ActivGuard is ALWAYS in short
                                supply. </u></strong>
                    </p>

                    <p>ActivGuard is not some mass-market item that gets cranked out by the thousands. </p>

                    <p><strong class="text-dark">ActivGuard is an artisan, premium blend of vitamins and nutrients
                            that’s
                            been proven to deliver real results
                            you can feel.</strong></p>

                    <p>And for the opportunity to be rid of bladder problems, to feel younger, freer, and happier, and
                        healthier…</p>

                    <p>All without constant doctors visits, being poked and prodded, and all the side effects of
                        expensive
                        prescription drugs.</p>

                    <p>The fact that ActivGuard can truly give you a happier and healthier life starting as soon as you
                        get
                        your
                        first bottle…</p>

                    <p>Is worth way more than what a few hundred bucks could buy you.</p>

                    <p>That’s why, at $149 for a month’s supply of ActivGuard, you get an absolute bargain.</p>

                    <p>But since B Naturals specializes in the sourcing of these pure and high quality ingredients, the
                        savings can
                        be passed on to you.</p>

                    <p>That’s why you won’t be asked to invest $149…</p>

                    <p>Or even $99</p>

                    <p>Or even $79 for your supply of ActivGuard.</p>

                    <p>But instead just $69…</p>

                    <p><strong class="text-dark">$69 for your best chance at never having to struggle with urgency
                            again.</strong></p>

                    <ul type="none" class="pl-0">
                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                To never have to get up multiple times during a dinner, a meeting, a movie, or a flight…
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                To be in complete control of when and where you go, without question, thanks to a healthierprostate.
                            </div>
                        </li>
                    </ul>

                    <p>And that’s why most health experts recommend you choose at least 3 bottles of ActivGuard…</p>

                    <p>And 6 bottles if you want to be completely safe…</p>

                    <center><img src="./img/multibottle.png"></center>

                    <p>Then take it for at least 90 days…</p>

                    <p>That way you can get the absolute best results possible…</p>

                    <h3 class="box-bg text-center greenfont">And when you purchase ActivGuard on this website, your
                        premium
                        shipping and handling is included for free, which is a $12.99 value.</h3>

                    <p>Plus that’s just the start…</p>

                    <p>I understand that being able to sleep through the night, being present with family and friends at
                        events, and
                        living comfortably can improve your life big time…</p>

                    <p>And it can improve the life of any loved ones who suffer, or will suffer, from a nagging
                        bladder…</p>

                    <p>That’s why, today, you can access a special discounted package plan…</p>

                    <p>Where you can get 6 bottles of ActivGuard…</p>

                    <p><strong class="text-dark"><u> For only $49 a bottle…</u></strong></p>

                    <p>A total of $294.</p>

                    <p>This special discount package is only being offered to you today through this website as part of
                        me
                        and B
                        Natural’s Feel Free Again program…</p>

                    <p>And only while supplies last. </p>

                    <p>And you’ll get the free premium shipping today, which is another $12.99 value…</p>

                    <p>But both the special multi-bottle discount and the free shipping are limited.</p>

                    <p>When ActivGuard runs out again, it could take up to three months before another batch is
                        available…</p>

                    <p>So click the 6 bottle package (or any other package you choose) below this video right now to
                        secure
                        your
                        order.</p>

                    <p>This is truly one of the smallest but most important investments that you’ll ever make in your
                        health
                        and
                        wellbeing…</p>

                    <p>A single bottle of ActivGuard will have you noticing less sudden, and less severe urges…</p>

                    <p>Three bottles will do the same thing, but will amplify the effects dramatically…</p>

                    <p>And 6 bottles of ActivGuard will give your bladder an entire six months of removed stress and
                        enhancement…</p>

                    <p>Transforming your entire urinary system from its current state into a state of seeming
                        perfection…</p>

                    <p>Where you’re in complete and effortless control…</p>

                    <p>And waking up multiple times a night, fearing an accident, and being distracted by urgency are
                        very
                        distant
                        memories.</p>

                    <p>Okay so at this point you’re probably thinking all of this sounds really exciting…</p>

                    <p>And you’re asking: “how do I get started?”</p>

                    <p>Just click the yellow button below right now and choose the package of ActivGuard that’s right
                        for
                        you. </p>

                    <p>You’ll then go to the secure checkout page where you’ll enter your order details…</p>

                    <p>And within 5 business days from now…</p>

                    <p>You’ll receive ActivGuard right to your front door.</p>

                    <p>So go ahead and click the button below now to secure ActivGuard while there are still bottles
                        available.</p>

                    <p>This should be one of the easiest decisions of your life as well…</p>

                    <p>Because your investment is also covered by a 180 Day (which is 6 months, which is HALF A YEAR),
                        100%
                        Money
                        back Guarantee.</p>

                    <center><img src="./img/gurantee_img.png" alt=""></center>

                    <p>It works like this…</p>

                    <p>Start using ActivGuard now…</p>

                    <p>Start noticing the sudden urges subside and become way less severe…</p>

                    <p>How you’re able to enjoy a whole dinner, a whole flight or car ride, without having to use the
                        bathroom…</p>

                    <p>And if for any reason you’re not happy with your results using ActivGuard…</p>

                    <p>Simply call or email B Naturals’ top rated, U.S. based customer support team 24/7…</p>

                    <p>And you’ll receive a prompt and immediate refund with no questions asked….</p>

                    <p>Plus you don’t even need to return your bottles – they’re yours to keep to say “thanks” for
                        putting
                        your
                        faith in B Naturals, and their mission.</p>

                    <h3 class="box-bg text-center greenfont"><u>That means all you have to do is decide to TRY
                            ActivGuard
                            today.</u></h3>

                    <p>You have nothing to lose here except the bladder problems that have been holding you back.</p>

                    <p>It’s a 100% risk free investment…</p>

                    <div class="well well-shade">
                        <h3 class="greenfont">So click the button below to be rushed your no-risk supply of ActivGuard
                            Right
                            now.</h3><br>

                        <center><img src="./img/multibottle.png" alt=""
                                     class="img-responsive center-block no-border text-center"></center>

                        <a style="background: none !important;border: none !important;box-shadow: none !important;"
                           href="./step1.php" onclick="grayOut();"><img class="mt-0" src="./img/green_btn.png"></a>

                        <span class="intermediate"><br><b>When you click that button, you’ll see the opportunity to save maximum on ActivGuard by purchasing up to 6 bottles at once.</b></span>

                        <h3 class="greenfont"><u>This also protects you from supply shortages and price increases.</u>
                        </h3>

                    </div>

                    <p>Remember: We’re working towards living in a world where NO ONE has to suffer with bladder
                        urgency… </p>

                    <p>And thanks to ActivGuard, more and more people get that relief each day.</p>

                    <p>So as more and more people discover true relief from bladder troubles, the demand for ActivGuard
                        and
                        its
                        ingredients will only rise. </p>

                    <p>This will cause the price of these ingredients to increase, which may force the price of
                        ActivGuard
                        to
                        increase…</p>

                    <p><strong class="text-dark">Plus, ActivGuard has been backordered before, and it will be
                            backordered
                            again.</strong></p>

                    <p>And given its proven effectiveness, you do NOT want to risk being without ActivGuard.</p>

                    <p><strong class="text-dark">In fact, the only risk you take today is ignoring this potentially
                            life-changing information. </strong></p>

                    <p>This presentation is now ending and the choice is ultimately yours…</p>

                    <p>One of these options is to stop watching this video, and continue on with your day.</p>

                    <p>That’s absolutely your right, but what will change if you make that decision?</p>

                    <p>How are you going to feel in a month, six months, or a year from now…</p>

                    <p>If you don’t take action today and your bladder keeps getting worse and worse?</p>

                    <p>The sad truth is that usually people wait too long to act…</p>

                    <p>Yet if you do nothing, you’ll just suffer more.</p>

                    <p>That’s why…</p>

                    <h3 class="box-bg text-center greenfont">Saying “No” to ActivGuard is saying NO to the chance at
                        real
                        relief from urgency and incontinence.</h3>

                    <p>It’s agreeing to let your bladder run your life.</p>

                    <p>It’s agreeing to being told by doctors there’s nothing you can do but deal with it, take
                        prescription
                        drugs
                        that lower your libido, or have complex surgery.</p>

                    <h3 class="box-bg text-center greenfont">WHY would you risk all of that, condemning yourself to
                        suffer
                        completely unnecessarily, when ActivGuard is both proven and 100% risk-free?</h3>

                    <p>The only thing that makes sense is to give yourself a great chance at life-changing relief with
                        ActivGuard.</p>

                    <p><strong class="text-dark">So click the button below to see if there is supply available.</strong>
                    </p>

                    <div class="well well-shade">
                        <center><img src="./img/multibottle.png" alt=""
                                     class="img-responsive center-block no-border text-center"></center>

                        <center>
                            <a style="background: none !important;border: none !important;box-shadow: none !important;"
                               href="./step1.php" onclick="grayOut();"><img class="mt-0" src="./img/green_btn.png"></a>
                        </center>
                    </div>

                    <p>If so, go ahead and get your bottles through the secure, triple-encrypted checkout page. </p>

                    <p><strong class="text-dark"><u>Your order will be on its way to you within 48 hours.</u></strong>
                    </p>

                    <p>Get excited because your best chance at real relief is on its way to you. Relief that can mean a
                        truly new
                        lease on life.</p>

                    <p>So click the button above with excitement and confidence, because you’re on the way to something
                        incredible,
                        with truly life changing potential.</p>

                    <h3 class="box-bg text-center greenfont">Now is the time for you reclaim control of your bladder
                        with
                        ActivGuard.</h3>

                    <ul type="none" class="pl-0">
                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Remember, ActivGuard has been proven to promote bladder health, shrink the prostate, and
                            eliminate urgency and incontinence.
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                And now you can get your very own supply affordably and with zero risk.
                            </div>
                        </li>

                        <li class="row mb-20">
                            <i class="fa fa-check col-xs-1"></i>
                            <div class="col-xs-11 pa-0">
                                Click the button below and complete checkout now.
                            </div>
                        </li>
                    </ul>

                    <div class="well well-shade">
                        <center>
                            <img src="./img/multibottle.png" alt=""class="img-responsive center-block no-border text-center">
                        </center>
                        <center>
                            <a style="background: none !important;border: none !important;box-shadow: none !important;"
                               href="./step1.php" onclick="grayOut();"><img class="mt-0" src="./img/green_btn.png"></a>
                        </center>
                    </div>

                    <div class="faq text-left">
                        <h2 id="faq-title" class="text-center bordera-0">Frequently Asked Questions</h2>
                        <div class="panel-group" id="faq-container">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-1">
                                        <div class="panel-title">
                                            What is ActivGuard?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            ActivGuard is a unique, 100% natural supplement, whose ingredients have been
                                            shown to promote bladder health, shrink the male and female prostate, and
                                            eradicate both sudden and nagging feelings of urgency, and incontinence.
                                        </p>
                                        <p>
                                            It is not easy to source these ingredients in their purest and most potent
                                            forms, which is why they are so rare and difficult to find. But B Naturals
                                            spares no expense, doing everything to ensure that ActivGuard improves as many
                                            lives as possible.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-2">
                                        <div class="panel-title">
                                            Is there anything else in ActivGuard?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Yes. EVERYTHING inside ActivGuard is designed to help you feel better. There’s
                                            Saw Palmetto, Beta-Sitosterol, Pygeum Africanum, Red Raspberry Juice Extract,
                                            Graviola, and much more.
                                        </p>
                                        <p>
                                            These ingredients have been shown to support a healthy prostate, less frequent
                                            need to go to the restroom, and a greater peace of mind for people who have
                                            trouble with untimely urination.
                                        </p>
                                        <p>
                                            And we’re excited for you to be the next person who feels better thanks to
                                            ActivGuard.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-3">
                                        <div class="panel-title">
                                            How do I take ActivGuard?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Each bottle of ActivGuard contains 60 small, easy to swallow capsules. It’s
                                            recommended you take 2 capsules daily. Because ActivGuard is all natural with no
                                            side effects, you can take up to 4 capsules daily if you suffer from severe
                                            urgency or incontinence.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-4">
                                        <div class="panel-title">
                                            How fast does ActivGuard work?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            ActivGuard can begin to shrink the prostate and promote bladder health starting
                                            with the very first time you take it. This is because the pure and potent Zinc,
                                            Copper, Selenium, Saw Palmetto, Reishi Mushroom, and other ingredients inside
                                            ActivGuard will immediately do their work at reducing prostate size, cleaning
                                            the bladder, and more. That means you can feel relief from urgency and
                                            incontinence with you very first bottle of ActivGuard.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-5">
                                        <div class="panel-title">
                                            How much does ActivGuard cost?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            The relief from urgency to use the bathroom and incontinence, the improved
                                            health, and the renewed lease on life you’ll have once your bladder problems are
                                            eradicated is truly priceless.
                                        </p>
                                        <p>
                                            That’s why the original asking price of $149 is an incredible bargain –
                                            especially because of how expensive all traditional methods are… and
                                            ActivGuard’s ability to help you save so much money on all of that hassle…
                                        </p>
                                        <p>
                                            But what’s even more important than the money you can save is the relief you can
                                            feel. That’s why, even though it is expensive and time-consuming to harvest the
                                            pure ingredients inside ActivGuard, B Naturals uses its manufacturing experience
                                            to be able to keep the costs as low as possible for you, making 1 bottle just
                                            $69, and when you buy multiple bottles, it can be as low as $49 per bottle.
                                        </p>
                                        <p>
                                            This includes FREE shipping premium of your order, which is a $12.99 value.
                                        </p>
                                        <p>
                                            And when you get ActiveGuard today, you do so 100% risk-free.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#faq-container" href="#faq-6">
                                        <div class="panel-title">
                                            Okay, how do I get my first shipment of ActivGuard?
                                        </div>
                                    </a>
                                </div>
                                <div id="faq-6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            First, click the button below. Then, complete the secure checkout page. Feel
                                            good about this smart decision and the new possibilities of real, lasting relief
                                            that you can feel. Your order will be processed and shipped within 48 hours and
                                            sent directly to your door.
                                        </p>
                                        <p>
                                            Relief starts with clicking the button below right now. We’re so excited for you
                                            to try ActivGuard.
                                        </p>
                                        <p>
                                            Click the button below right now.
                                        </p>
                                        <div class="text-center">
                                            <a href="./kon/checkout.php?pr_id=18">
                                                <img src="./img/multibottle.png" alt="Multi Bottles">
                                                <img src="./img/ActivGuard/green_btn.png" alt="Get ActivGuard Now">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
<div id="works-cited-section" class="bg-white">
    <div class="container text-center">
        <div class="row">
            <div class="col-xs-12">
                <div id="works-cited">
                    <h1>Works Cited</h1>
                    <img src="./img/symbols.gif" alt="Symbols">
                </div>
            </div>
        </div>
        <div class="row hidden-sm hidden-xs cites">
            <div class="col-xs-4">
                <div class="cite">
                    <div class="site">
                        <div class="num">1</div>
                        <p>
                            Protesters with Black Lives Matter shut down 405 Freeway in Inglewood
                            http://www.latimes.com/local/lanow/la-me-ln-protest-inglewood-20160710-snap-story.html
                            Matt
                            Hamilton
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">4</div>
                        <p>
                            Five Dallas Officers Were Killed as Payback, Police Chief Says
                            http://www.nytimes.com/2016/07/09/us/dallas-police-shooting.html?_r=0 MANNY FERNANDEZ,
                            RICHARD .....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">7</div>
                        <p>
                            Jade Helm 15: Controversial military exercise starts Wednesday
                            https://www.armytimes.com/story/military/2015/07/14/jade-helm-starts-army-special
                            -operations-exercise/30144561....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">10</div>
                        <p>
                            As Jade Helm 15 Military Exercise Begins, Texans Keep Watch ‘Just in Case’
                            http://www.nytimes.com/2015/07/16/us/in-texas-a-military-exercise-is-met-by-some-with-suspicion
                            .html......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">13</div>
                        <p>
                            Choosing between NSAID and arnica for topical treatment of hand osteoarthritis in a
                            randomised, double-blind study. http://www.ncbi.nlm.nih.gov/pubmed/17318618 Rheumatology
                            Clinic, 9004,...
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">16</div>
                        <p>
                            Beta-carotene and other carotenoids as antioxidants.
                            http://www.ncbi.nlm.nih.gov/pubmed/10511324 Faculdade de Medicina de Botucatu,
                            Universidade..
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="cite">
                    <div class="site">
                        <div class="num">2</div>
                        <p>
                            Black Lives Matter protesters shut down Virginia freeway, 13 arrested
                            http://nbc4i.com/2016/07/19/black-lives-matter-protesters-shut-down-virginia-freeway-13-arrested
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">5</div>
                        <p>
                            Dallas police chief invites Black Lives Matter protesters to join department
                            http://www.washingtontimes.com/news/2016/jul/11/black-lives-matter-slammed-by-backlash-after-dalla.....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">8</div>
                        <p>
                            Jade Helm 15 media blackout: Press not given access to controversial military exercises
                            http://www.washingtontimes.com/news/2015/jul/9/jade-helm-15......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">11</div>
                        <p>
                            620,000 soldiers died during the Civil War. Two thirds died of disease, not wounds.
                            http://www.civilwar.org/education/pdfs/civil-was-curriculum-medicine.pdf
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">14</div>
                        <p>
                            Arnica montana gel in osteoarthritis of the knee: an open, multicenter clinical trial.
                            http://www.ncbi.nlm.nih.gov/pubmed/12539881 Department of....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">17</div>
                        <p>
                            Beta-carotene http://umm.edu/health/medical/altmed/supplement/betacarotene
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="cite">
                    <div class="site">
                        <div class="num">3</div>
                        <p>
                            ‘Black Lives Matter’ protesters block I-64 in downtown St. Louis
                            http://fox2now.com/2016/07/16/black-lives-matter-protesters-block-i-64-in-downtown-st-louis
                            JOE MILLITZER
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">6</div>
                        <p>
                            Hillary Clinton Top Secret Emails LEAKED! Assassination Teams for US Citizens!
                            http://www.anonews.co/leaked-hillary-clinton-emails
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">9</div>
                        <p>
                            What is Jade Helm 15 and why is it attracting controversy?
                            https://www.bostonglobe.com/news/nation/2015/07/13/why-jade-helm-fueling-fears-government-takeover/......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">12</div>
                        <p>
                            Railroads In The Civil War http://www.american-rails.com/railroads-in-the-civil-war.html
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">15</div>
                        <p>
                            American War Deaths Through History
                            http://www.militaryfactory.com/american_war_deaths.asp
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">18</div>
                        <p>
                            Protesters with Black Lives Matter shut down 405 Freeway in Inglewood
                            http://www.latimes.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row visible-sm visible-xs cites">
            <div class="col-xs-12">
                <div class="cite">
                    <div class="site">
                        <div class="num">1</div>
                        <p>
                            Protesters with Black Lives Matter shut down 405 Freeway in Inglewood
                            http://www.latimes.com/local/lanow/la-me-ln-protest-inglewood-20160710-snap-story.html
                            Matt
                            Hamilton
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">2</div>
                        <p>
                            Five Dallas Officers Were Killed as Payback, Police Chief Says
                            http://www.nytimes.com/2016/07/09/us/dallas-police-shooting.html?_r=0 MANNY FERNANDEZ,
                            RICHARD .....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">3</div>
                        <p>
                            Jade Helm 15 media blackout: Press not given access to controversial military exercises
                            http://www.washingtontimes.com/news/2015/jul/9/jade-helm-15......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">4</div>
                        <p>
                            As Jade Helm 15 Military Exercise Begins, Texans Keep Watch ‘Just in Case’
                            http://www.nytimes.com/2015/07/16/us/in-texas-a-military-exercise-is-met-by-some-with-suspicion
                            .html......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">5</div>
                        <p>
                            American War Deaths Through History
                            http://www.militaryfactory.com/american_war_deaths.asp
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">6</div>
                        <p>
                            Beta-carotene and other carotenoids as antioxidants.
                            http://www.ncbi.nlm.nih.gov/pubmed/10511324 Faculdade de Medicina de Botucatu,
                            Universidade..
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">7</div>
                        <p>
                            Black Lives Matter protesters shut down Virginia freeway, 13 arrested
                            http://nbc4i.com/2016/07/19/black-lives-matter-protesters-shut-down-virginia-freeway-13-arrested
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">8</div>
                        <p>
                            Dallas police chief invites Black Lives Matter protesters to join department
                            http://www.washingtontimes.com/news/2016/jul/11/black-lives-matter-slammed-by-backlash-after-dalla.....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">9</div>
                        <p>
                            Jade Helm 15: Controversial military exercise starts Wednesday
                            https://www.armytimes.com/story/military/2015/07/14/jade-helm-starts-army-special
                            -operations-exercise/30144561....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">10</div>
                        <p>
                            620,000 soldiers died during the Civil War. Two thirds died of disease, not wounds.
                            http://www.civilwar.org/education/pdfs/civil-was-curriculum-medicine.pdf
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">11</div>
                        <p>
                            Arnica montana gel in osteoarthritis of the knee: an open, multicenter clinical trial.
                            http://www.ncbi.nlm.nih.gov/pubmed/12539881 Department of....
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">12</div>
                        <p>
                            Beta-carotene http://umm.edu/health/medical/altmed/supplement/betacarotene
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">13</div>
                        <p>
                            ‘Black Lives Matter’ protesters block I-64 in downtown St. Louis
                            http://fox2now.com/2016/07/16/black-lives-matter-protesters-block-i-64-in-downtown-st-louis
                            JOE MILLITZER
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">14</div>
                        <p>
                            Hillary Clinton Top Secret Emails LEAKED! Assassination Teams for US Citizens!
                            http://www.anonews.co/leaked-hillary-clinton-emails
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">15</div>
                        <p>
                            What is Jade Helm 15 and why is it attracting controversy?
                            https://www.bostonglobe.com/news/nation/2015/07/13/why-jade-helm-fueling-fears-government-takeover/......
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">16</div>
                        <p>
                            Railroads In The Civil War http://www.american-rails.com/railroads-in-the-civil-war.html
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">17</div>
                        <p>
                            Choosing between NSAID and arnica for topical treatment of hand osteoarthritis in a
                            randomised, double-blind study. http://www.ncbi.nlm.nih.gov/pubmed/17318618 Rheumatology
                            Clinic, 9004,...
                        </p>
                    </div>
                    <div class="site">
                        <div class="num">18</div>
                        <p>
                            Protesters with Black Lives Matter shut down 405 Freeway in Inglewood
                            http://www.latimes.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="inline-list">
                    <li><a href="./faq.html" target="_blank">FAQ</a></li>
                    <li><a href="./disclaimer.html" target="_blank">Disclaimer</a></li>
                    <li><a href="./anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                    <li><a href="./privacy.html" target="_blank">Privacy</a></li>
                    <li><a href="./terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                    <li><a href="./refunds.html" target="_blank">Refunds</a></li>
                    <li><a href="./contact-us.html" target="_blank">Contact Us</a></li>
                    <li><a href="./step1.php" target="_blank">Get ActivGuard Now</a></li>
                </ul>
            </div>
            <div class="col-xs-10 col-xs-offset-1">
                <p class="text-light">
                    The statements on this website and on these product labels have not been evaluated by the food and
                    drug
                    administration. These products are intended as a dietary supplement only. Products are not intended
                    to
                    diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body
                    type, compliance, and other factors. All products are intended for use by adults over the age of 18.
                    Consult a physician before taking any of our products, especially if you are pregnant, nursing,
                    taking
                    medication, diabetic, or have any medical condition.
                </p>
            </div>
            <div class="col-xs-12">
                <p>Copyright © ActivGuard 2017</p>
            </div>
        </div>
    </div>
</footer>
<script src="./js/jquery.min.js"></script>
<script async src="./js/bootstrap.min.js"></script>
<script async src="./js/pages/desktop.min.js"></script>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ActivguardNow</title>
	<meta name="robots" content="noindex">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
   	<style>
		html {
			background-color: #f7f2eb;
		}
		
    	.container {
			max-width: 1180px;
			background-color: transparent;
		}
		
		#header {
			padding-top: 25px;
			padding-bottom: 25px;
			border-bottom: none;
			padding-right: 15px;
			padding-left: 15px;
		}
		
		.flag_img {
			float: right;
			padding: 15px 0px;
		}
		
		.header_container {
			padding-right: 0px !important;
			padding-left: 0px !important;
		}
		
		body {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/patern.png) !important;
			background-color: transparent;
			background-position: center top;
			background-repeat: no-repeat;
		}
		
		.top_menu_bar_sp {
			padding: 40px 10px 70px;
		}
		
		.top_menu_bar_sp_packages {
			padding: 40px 10px 30px;
		}
		
		.gray_box_with_border {
			background-color: #f7f7f7;
			border: 1px solid #e0dfdf;
			padding: 0px 10px;
			text-align: center;
		}
		
		.gray_box_without_border {
			background-color: #f0f0f0;
			padding: 0px;
			text-align: center;
			margin-right: 0px;
    		margin-left: 0px;
    		padding: 15px 0px;
		}
		
		.guarantee_box_sp {
			background-color: #fffdfb;
			padding: 30px 20px 30px;
			border-radius: 10px;
			border-bottom: 3px solid #f15d4e;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;		
		}
		
		.guarantee_box_sp .col-sm-9 {
			padding: 0px 30px 0px 0px;
    		margin-top: 15px;
		}
		
		.whats_behind_gr_section {
			background-color: #fffdfb;
			border: none;
			padding: 0px 20px;
			border-radius: 10px;
			border-bottom: 3px solid #f15d4e;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}
		
		.whats_behind_gr_section .col-sm-8 {
			padding: 45px 35px;
    		margin-top: 0px;
		}
		
		.faq_gr_section {
			background-color: #fffdfb;
			padding: 30px 0px;
			border-radius: 10px;
			border-bottom: 3px solid #f15d4e;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}
		
		.roboto_c_font {
			font-family: 'Roboto Condensed', sans-serif; 
		}
		
		.raleway_font {
			font-family: 'Raleway', sans-serif; 
		}
		
		.roboto_font {
			font-family: 'Roboto', sans-serif;
		}
		
		.opensans_font {
			font-family: 'Open Sans', sans-serif;
		}
		
		.doridsans_font {
			font-family: 'Droid Sans', sans-serif;
		}
		
		.txt_16 {
			font-size: 16px;
		}
		
		.txt_17 {
			font-size: 17px;
		}
					
		.txt_18 {
			font-size: 18px;
		}
		
		.txt_21 {
			font-size: 21px;
		}
		
		.txt_22 {
			font-size: 22px;
		}
		
		.txt_24 {
			font-size: 24px;
		}
		
		.txt_28 {
			font-size: 28px;
		}
				
		.txt_30 {
			font-size: 30px;
		}
		
		.txt_32 {
			font-size: 32px;
		}
		
		.txt_36 {
			font-size: 36px;
		}
					
		.txt_44 {
			font-size: 44px;
		}
		
		.txt_46 {
			font-size: 46px;
		}
		
		.txt_48 {
			font-size: 48px;
		}
		
		.font_light {
			font-weight: 300;
		}
		
		.font_regular {
			font-weight: 400;
		}
		
		.font_medium {
			font-weight: 500;
		}
				
		.font_semibold {
			font-weight: 600;
		}
		
		.font_bold {
			font-weight: 700;
		}
				
		.darkgray {
			color: #3b3636;
		}
		
		.redfont {
			color: #ef2216;
		}
		
		.redfont_2 {
			color: #ff0000;
		}
		
		.orangefont {
			color: #e44f00;
		}
		
		.orangefont_2 {
			color: #ed793c;
		}
		
		.darkgray_1 {
			color: #434343;
		}
		
		.darkgray_2 {
			color: #585858;
		}
		
		.darkgray_3 {
			color: #595959;
		}
		
		.darkgray_4 {
			color: #696969;
		}
		
		.darkgray_5 {
			color: #424141;
		}
		
		.darkgray_6 {
			color: #2f2f2f;
		}
		
		.darkgray_7 {
			color: #1e1c1c;
		}
		
		.darkgray_8 {
			color: #272727;
		}
		
		.light_gray {
			color: #838282;
		}
		
		.light_gray_2 {
			color: #f5f5f5;
		}
		
		.greenfont {
			color: #3b9001;
		}
		
		.blackfont {
			color: #000;
		}
		
		.greenfont_2 {
			color: #43a300;
		}
					
		.letterspace_7 {
			letter-spacing: 7px;
		}
		
		.padding_left_20 {
			padding-left: 20px;
		}
		
		.padding_top_55 {
			padding-top: 55px;
		}
		
		.padding_left_25 {
			padding-left: 25px;
		}
		
		.padding_top_bottom_10 {
			padding-top: 10px;
			padding-bottom: 10px;
		}
		
		.no_padding {
			padding: 0px !important;
		}
		
		.text_right {
			text-align: right;
		}
			
		.text_left {
			text-align: left;
		}
		
		.text_center {
			text-align: center;
		}
			
		.section1 {
			padding: 25px 0px;
		}
		
		.section7 {
			padding-bottom: 20px;
		}
		
		.col_sm_8 {
			width: 66.6667%;
		}
		
		.col_sm_4 {
			width: 33.3333%;
		}
		
		.shipping_free {
			padding-right: 20px;
		}
		
		.section3 {
			padding: 20px;
			border-bottom: 4px double #ccc;
		}
		
		.section_heading {
			padding: 20px 0;
		}
		
		/*.section4 .panel .panel-heading {
			background-image: url('<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_img1.png');
			background-size: cover;
		}*/
		
		.panel {
			border: none;
		}
		
		.panel-heading {
			padding: 5px 15px !important;
		}
		
		.panel .panel-heading {
			/*background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/gray_bar_bg_1.png);*/
			background-size: contain;
			background-color: #fff8f0;
			color: #000;
			/*text-transform: uppercase;*/
			background-repeat: repeat;
			/*border: 1px solid #c7c7c7;*/
		}
		
		.panel-body {
		    background-color: #f6f6f6;
			border: 1px solid #c7c7c7;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
		}
					
		.selected .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_bg.png);
			background-size: contain;
			background-color: transparent;
			color: #fff;
			text-transform: uppercase;
			border: 1px solid #537c1d;
			border-bottom: none;
		}
		
		.selected .panel-body {
		    background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/light_green_bg.png);
			border: 1px solid #537c1d;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			background-size: contain;
		}
		
		.linethrough {
			text-decoration:line-through;
		}
		
		.section8 {
			margin-bottom: 25px;
		}
		
		.right_section_greenbox {
			margin-bottom: 40px;
		}
		
		.right_section_greenbox_body {
			background-color: #fafafa;
			padding: 20px;
			border: 1px solid #2d6728;
			border-radius: 3px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			border-top: none;
		}
		
		.right_section_header {
			text-align: center;
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/right_sidebar_heading_bg.png);
			background-size: contain;
			border-radius: 5px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}
		
		select, input[type="text"] {
			padding: 7px;
    		width: 100%;
			border: 1px solid #31802b;
		}
		
		.form_label {
		    padding-top: 15px;
    		padding-bottom: 10px;
		}
		
		.what_this_space {
			padding-top: 8px;
    		padding-bottom: 8px;
		}
		
		.footer_section {
			background-color: #3a1612;
			padding: 45px 45px;
		}
		
		.footer_text_container {
		    width: 90%;
    		margin: 0 auto;
		}
		
		.footer_menu_container {
			text-align: center;
			padding: 0px 0px 10px;
		}
		
		.footer_menu_container .list-inline li a {
			font-size: 17px;
			color: #d4875e;
			font-weight: 400;
			font-family: 'Raleway', sans-serif; 
		}
		
		.copywright_text {
			text-align: center;
		}
		
		.copywright_text span {
			color: #cecdcd;
			font-family: 'Raleway', sans-serif;
			font-weight: 300;
			font-size: 14px;
		}
		
		.form_submit_btn {
			max-width: 100%;
		}
		
		.hidethis {
			display: none;
		}
		
		input[name="product_selected"] {
			visibility: hidden;
		}
		
		.checked {
			margin-top: -12px;
		}
		
		.notchecked {
			margin-top: -7px;
		}
		
		/* Custom colored checkbox start */
		.regular-checkbox {
			display: none;
		}
		
		.regular-checkbox + label {
			-webkit-appearance: none;
			background-color: #ffffff;
			border: 1px solid #255521;
			/*padding: 9px;*/
			padding: 6px;
			border-radius: 1px;
			display: inline-block;
			position: relative;
			margin-right: 5px;
			margin-bottom: -2px;
		}
		
		.regular-checkbox:checked + label:after {
			content: ' ';
			width: 24px;
			height: 17px;
			border-radius: 1px;
			position: absolute;
			top: -5px;
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/black_checkbox.png);
			background-size: contain;
			text-shadow: 0px;
			left: 0px;
			font-size: 32px;
			background-repeat: no-repeat;
		}
		
		.regular-checkbox:checked + label {
			color: #99a1a7;
			border: 1px solid #255521;
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1), inset 0px 0px 10px rgba(0,0,0,0.1);
		}
		
		.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
		}
		
		/* Custom colored checkbox end */
		
		.container.header_container {
			max-width: 100%;
			width: 100%;
		}
		
		.header_right {
			list-style: none;
		}
		
		.header_right li {
			float: left;
		}
		
		.header_right_div {
			float: right;
    		padding-top: 10px;
			padding-right: 20px;
		}
		
		.packagecol {
			/*background-image: url('img/packagebg.png');*/
			/*background-image: url('img/pinkbg2.png');
			background-repeat: repeat;
			background-size: contain;*/
			/*padding: 20px;*/
		}
		
		.packagecol_middle .packagecol {
			/*background-image: url('img/packagebg.png');*/
			background-image: url('img/pinkbg.png') !important;
			background-repeat: repeat;
			background-size: contain;
			border-radius: 40px;
    		border-bottom-right-radius: 0px;
    		border-bottom-left-radius: 0px;
			/*padding: 20px;*/
		}
		
		/*.first_package {
			border-radius: 40px;
    		border-bottom-right-radius: 0px;
    		border-bottom-left-radius: 0px;
			border-top-right-radius: 0px;
			border: 2px solid #f15d4e;
    		border-right: none;
		}
		
		.second_package {
			border-radius: 40px;
    		border-bottom-right-radius: 0px;
    		border-bottom-left-radius: 0px;
			border-top-left-radius: 0px;
			border: 2px solid #f15d4e;
    		border-left: none;
		}*/
		
		.packagecol_middle .second_package .packagecol_header {
			line-height: 50px;
    		padding-top: 45px !important;
		}
		
		.bg_row {
			border: 2px solid #f15d4e !important;
			border-bottom: 6px solid #f15d4e !important;
			border-radius: 40px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
			background-image: url(img/pinkbg2.png);
			background-repeat: repeat;
			background-size: contain;
		}
		
		.packagecol_middle .second_package {
			border: 2px solid #f15d4e !important;
			border-radius: 40px;
    		border-bottom-right-radius: 0px;
    		border-bottom-left-radius: 0px;
			/*border-top-left-radius: 0px;*/
			border-bottom: none !important;
		}
		
		.divider_container_middle {
			padding-top: 12px;
		}
		
		.packagecol_header {
			padding: 20px 0px;
		    /*border-bottom: 1px solid #5e782c;*/
		}
		
		.packagecol_body {
			/*padding: 20px;*/
		}
		
		.packagecol_middle {
			padding-left: 0px;
			padding-right: 0px;
			/*margin-left: -15px;
			margin-right: -15px;*/
			margin-top: -35px;
		}
		
		.addtocart_btn {
			padding: 20px;
		}
		
		.remove_max_width {
			max-width: none !important;
		}
		
		.width_30 {
			width: 30% !important;
			padding-left: 0px;
    		padding-right: 0px;
		}
		
		.width_40 {
			width: 40% !important;
		}
		
		/* Shadow */
		.packagecol_middle {
			position:relative;
			/*-webkit-box-shadow:0 0px 5px rgba(0, 0, 0, 0.3), 0 0 5px rgba(0, 0, 0, 0.1) inset;
			-moz-box-shadow:0 0px 5px rgba(0, 0, 0, 0.3), 0 0 5px rgba(0, 0, 0, 0.1) inset;
			box-shadow: 0 0px 5px rgba(0, 0, 0, 0.3), 0 0px 5px rgba(0, 0, 0, 0.1) inset;*/
			z-index: 1;
		}
		
		.packagecol_middle:before, .packagecol_middle:after { 
			content:"";
			position:absolute;
			z-index:-1;
			/*-webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
			-moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
			box-shadow:0 0 20px rgba(0,0,0,0.8);*/
			top:10px;
			bottom:10px;
			left:0;
			right:0;
			-moz-border-radius:100px / 10px;
			border-radius:100px / 10px;
		}
		
		.packagecol_middle:after {
			right:10px;
			left:auto;
			-webkit-transform:skew(8deg) rotate(3deg);
			-moz-transform:skew(8deg) rotate(3deg);
			-ms-transform:skew(8deg) rotate(3deg);
			-o-transform:skew(8deg) rotate(3deg);
			transform:skew(8deg) rotate(3deg);
		}
		
		.hr_line {
			border-bottom: 2px solid #f15d4e;
			padding: 5px;
		}
		
		.qa_question {
			/*color: #454545;*/
			font-size: 22px;
			padding: 20px 0px;
			/*border-bottom: 2px solid #f15d4e;*/
			text-align: left;
			color: #000000 !important;
			font-weight: 600;
			font-family: 'Open Sans', sans-serif;
		}
		
		.qa_answer {
			color: #4e4e4d;
			font-size: 16px;
			font-family: 'Open Sans', sans-serif;
			font-weight: 400;
			padding: 0px 30px 0px;
			line-height: 35px;
		}
		
		.faq_container {
		    padding: 0px 50px;
		}
		
		.qa_answer p {
			font-size: 16px;
			font-family: 'Open Sans', sans-serif;
			font-weight: 400;
			color: #4e4e4d;
		}
		
		ul.tick {
			padding-left: 60px;
			text-indent: -17px;
			list-style: none;
			list-style-position: outside;
		}
		
		ul.tick li:before {
			content: '✔';
			margin-left: -1em;
			margin-right: 1.100em;
		}
		
		.row_max_width {
			max-width: 1440px;
			margin: 0 auto;
		}
		
		.top_menu_bar_sp_packages .row {
			margin-left: -10px !important;
			margin-right: -10px !important;
		}
		
		.faq_main_container {
			padding: 0 55px;
		}
		
		.faq_container .panel-title {
			text-align: left;
		}
		
		.according_icon {
			padding-right: 15px;
		}
		
		.faq_container .panel-heading {
			padding: 0px 0px !important;
		}
		
		.faq_container .panel-title a {
			text-decoration: none;
		}
		
		.faq_container .panel-body {
			background-color: #fff !important;
			border: none !important;
		}
		
		.faq_container .panel .panel-title {
			color: #000000 !important;
			font-weight: 600;
			font-family: 'Open Sans', sans-serif;
			font-size: 22px;
		}
		
		.icon_minus .minus_img {
			display: inline-block;
		}
		
		.icon_plus .minus_img {
			display: none;
		}
		
		.icon_plus .plus_img {
			display: inline-block;
		}
		
		.icon_minus .plus_img {
			display: none;
		}
		
		.plus_minus_container {
			width: 40px;
			height: 100%;
			position: absolute;
			background: #f15d4e;
			left: 0px;
			top: 0px;
		}
		
		.faq_container .panel-title {
			padding: 5px 20px 11px 50px !important;
			position: relative;
		}
		
		@media (min-width: 1200px) {
			.container {
				width: 1180px;
			}
		}
		
		@media (max-width: 768px) {
			.section6 .panel-body .col-sm-5 {
			    text-align: center;
			}
			
			.section6 .panel-body .col-sm-5 img {
			    padding-left: 0px !important;
			}
			
			.section4 .panel-body .col-sm-5 {
			    text-align: center;
				margin-top: 75px;
			}
			
			.section4 .panel-body .col-sm-3 {
			    text-align: center;
				position: absolute;
				top: -10px;
				right: -15px;
			}
			
			.section4 .row {
				position: relative;
			}
			
			.width_40, .width_30 {
				width: 100% !important;
			}
			
			.packagecol_middle {
				margin: 0 auto;
			}
			
			.faq_main_title_container {
				padding-left: 15px !important;
			}
			
			.faq_container {
				padding: 0px 15px !important;
			}
		}
    </style>
	
<script type="text/javascript">
//window.onbeforeunload = grayOut;
function grayOut(){
var ldiv = document.getElementById('LoadingDiv');
ldiv.style.display='block';
}
</script>
<script type="text/javascript" src="js/jqeury.js"></script>
<link rel="stylesheet" type="text/css" href="css/custom.css">		
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89657482-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
	
	
	<div id="LoadingDiv" style="display:none;">One Moment Please...<br /><img src="img/progressbar.gif" class="displayed" alt="" /></div>
	<div class="container header_container">
        <header id="header">    	
            <div class="row row_max_width">
                <div class="col-sm-6">
                    <a href="http://activguardnow.com"><img src="img/logos.png" /></a>
                </div>
                <div class="col-sm-6 flag_img">
                	<div class="header_right_div">
                    	<ul class="header_right">
                        	<!--<li>
                    			<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            </li>-->
                            <li>    
                            	<span class="txt_22 opensans_font font_regular" style="color: #000;"><span class="glyphicon glyphicon-time" aria-hidden="true" style="top: 4px;"><img src="img/headicon.png" /></span> Bottles of ActivGUARD left in stock: </span><span class="txt_22 font_regular" style="color: #de0000;">28</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>        
        </header>
    </div>
    <div class="container">
    	<div class="top_menu_bar_sp text_center">
            <div class="txt_44 blackfont font_semibold opensans_font">CHOOSE YOUR DISCOUNTED PACKAGE</div>
            <div><img src="img/divider_selectpackage_main.png" /></div>
        </div>
        <div class="top_menu_bar_sp_packages text_center">
            <div class="row bg_row">
            	<div class="col-sm-4 width_30">
                	<div class="first_package packagecol">
                        <div class="packagecol_header">
                            <div style="color: #343434;" class="txt_36 font_semibold doridsans_font">3 Month Supply</div>
                            <div style="color: #6b6959;" class="txt_32 font_semibold doridsans_font">3 Bottles</div>
                            <div><img src="img/divider_small.png" /></div>
                        </div>
                        <div class="packagecol_body">
                            <div><img src="img/sp_product1.png" class="remove_max_width" /></div>
                            <div>
                            	<span class="blackfont txt_46 roboto_c_font font_bold">$59 / bottle</span>
                            </div>
			     
			    
                            <div class="addtocart_btn">
                            	<a href="#" onclick="grayOut();"><img src="img/addtocartbtn.png" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 packagecol_middle width_40">
                	<div style="position:absolute;margin-top: -3px;margin-left: -2px;"><img src="img/bestseller_tag.png" /></div>
                	<div class="second_package packagecol">
                        <div class="packagecol_header">
                            <div style="color: #343434;" class="txt_48 font_semibold doridsans_font">6 Month Supply</div>
                            <div style="color: #6b6959;" class="txt_44 font_semibold doridsans_font">6 Bottles</div>
                            <div class="divider_container_middle"><img src="img/divider_big.png" /></div>
                        </div>
                        <div class="packagecol_body">
                            <div><img src="img/sp_product2.png" /></div>
                            <div>
                            	<span class="blackfont txt_46 roboto_c_font font_bold">$49 / bottle</span>
                            </div>
			    
                            <div class="addtocart_btn">
                            	<a href="#"  onclick="grayOut();"><img src="img/addtocartbtn.png" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 width_30">
                	<div class="second_package packagecol">
                        <div class="packagecol_header">
                            <div style="color: #343434;" class="txt_36 font_semibold doridsans_font">1 Month Supply</div>
                            <div style="color: #6b6959;" class="txt_32 font_semibold doridsans_font">1 Bottle</div>
                            <div><img src="img/divider_small.png" /></div>
                        </div>
                        <div class="packagecol_body">
                            <div><img src="img/sp_product3.png" /></div>
                            <div>
                            	<span class="blackfont txt_46 roboto_c_font font_bold">$69 / bottle</span>
                            </div>
			   
                            <div class="addtocart_btn">
                            	<a href="#" onclick="grayOut();"><img src="img/addtocartbtn.png" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col-sm-12">
                     <div class="section8">
                        <div class="guarantee_box_sp">
                            <div class="row">
                                <div>
                                    <div class="col-sm-3 no_padding text_center">
                                        <img src="img/moneyback.png" />
                                    </div>
                                    <div class="col-sm-9">
                                    	<div style="padding-bottom: 15px;">
                                        	<span class="txt_30 font_semibold opensans_font" style="color: #ef2216;"><span class="redfont">INCLUDES A 60 DAYS</span> 100% MONEY BACK GUARANTEE</span>
                                        </div>
                                        <div>
                                        	<p class="txt_16 font_regular opensans_font" style="color: #4e4e4d;">NutraHER comes with a 60 Day, 100% Money Back Guarantee. That means if you change your mind about this decision at any point in the next two months – all you need to do is email us, and we’ll refund your purchase. Plus, you don’t need to return the bottle.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                     
                </div>                
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                     <div class="section8">
                        <div class="whats_behind_gr_section">
                            <div class="row">
                                <div>                                    
                                    <div class="col-sm-8">
                                    	<div style="padding-bottom: 15px;">
                                        	<span class="txt_30 font_semibold opensans_font" style="color: #313131;"><span class="redfont">What’s the Story Behind</span> NutraHER?</span>
                                        </div>
                                        <div>
                                        	<p class="txt_16 font_regular opensans_font" style="color: #4e4e4d;">NutraHer Lean is made in the U.S.A by a supplement company named NutraSMASH. It’s unique blend of fat-burning ingredients stimulate the body’s powerful fat burning hormones. NutraHer Lean is one of the most potent fat burning supplements in the world that you can get without a prescription. It is made of 100% all-natural and pure ingredients that’ve been proven to help the body shed fat in thousands of studies.</p>
                                            <p class="txt_16 font_regular opensans_font" style="color: #4e4e4d;">NutraHer Lean is built off of the experience and results seen in our Weight Management Centers, where clients have lost over 250,000 lbs of body fat, and counting! NutraHer is the best way to get these proven results in your own home, without changing your diet or activity level at all.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 no_padding text_center">
                                        <img src="img/lady.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                     
                </div>                
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                     <div class="section8">
                        <div class="faq_gr_section">
                            <div class="row">
                                <div>                                    
                                    <div class="col-sm-12">
                                        <div style="padding-bottom: 25px; padding-left: 50px;" class="faq_main_title_container">
                                            <span class="txt_30 font_semibold opensans_font" style="color: #313131;">Frequently Asked Questions</span>
                                            <div class="hr_line"></div>
                                        </div>
                                                                                                                        
                                        <div class="panel-group faq_container" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="qa_question font_regular opensans_font">Okay so now does NutraHer Lean work again?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class="qa_answer">
                                                            <p>NutraHer Lean really works in two phases…

                                                            <p>Phase 1 is to trigger the fat burning hormone norepinephrine…</p>
                                                            
                                                            <p>Which is a hormone that’s already inside of your body.</p>
                                                            
                                                            <p>This hormone has been shown to break down fat tissues and cells…</p>
                                                            
                                                            <p>Leading to what’s called fat oxidation.</p>
                                                            
                                                            <p>And the ingredients in NutraHer Lean that help trigger norepinephrine are Green Tea Extract, L-Carnitine, and Oolong Tea Extract.</p>
                                                            
                                                            <p>Phase 2 is to stop carbs, starches, sugars, and other foods you eat in the future from being stored as fat. </p>
                                                            
                                                            <p>This is crucial because normally these foods would be sent to cells…</p>
                                                            
                                                            <p>Where they pile up in your gut…your butt…your thighs…your waist…and all the other places where you don’t want fat accumulating.</p>
                                                            
                                                            <p>There are two ingredients inside of NutraHer Lean that do just that. </p>
                                                            
                                                            <p>These ingredients are: White Kidney Bean Extract and CLA (Conjugated Linoleic Acid)…</p>
                                                            
                                                            <p>And they’ve been shown to stop the formation of new fat cells…</p>
                                                            
                                                            <p>By sending extra calories to muscle tissues, where they are burned as lean energy. </p>
                                                            
                                                            <p>Then on top of that…</p>
                                                            
                                                            <p>We’ve included 6 additional fat-burning and fat-stopping ingredients inside of NutraHer Lean…</p>
                                                            
                                                            <p>And they are: </p>
                                                            
                                                            <p>Guarana Seed Extract, Green Coffee Bean Extract, Raspberry Ketone, Alpha Lipoic Acid, Garcinia Fruit Extract, and Cayenne Pepper Fruit Powder. </p>
                                                            
                                                            <p>I didn’t go into all of the details of these additional ingredients inside the presentation…</p>
                                                            
                                                            <p>But they have been shown in numerous studies to assist with weight loss…</p>
                                                            
                                                            <p>While also promoting lean muscle growth, improving your metabolism, speeding up your energy, and providing healthy antioxidants that fight aging.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                                                        
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed qa_question font_regular opensans_font">Will NutraHer Lean work for me?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <div class="qa_answer">
                                                            <p>YES!</p> 

                                                            <p>The truth is that we all gain and lose weight in the same way…</p>
                                                            
                                                            <p>Because all of our bodies are fundamentally similar.</p>
                                                            
                                                            <p>NutraHer Lean uses proven nutritional science that works with your internal biology…</p>
                                                            
                                                            <p>Which in turn makes achieving your fat-loss goals easy.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="collapsed qa_question font_regular opensans_font">How long will NutraHer Lean be available for?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <div class="qa_answer">
                                                            <p>It’s hard to say…</p>

                                                            <p>Because NutraHer Lean contains twice the fat-burning ingredients of most leading supplements…</p>
                                                            
                                                            <p>It takes us twice as long to make each batch. </p>
                                                            
                                                            <p>We frequently run out of stock…</p>
                                                            
                                                            <p>Which means if we do have supplies today…</p>
                                                            
                                                            <p>I’d highly recommend you take advantage and secure your own deeply discounted bottle.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive" class="collapsed qa_question font_regular opensans_font">And what are the terms of that guarantee again?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <div class="qa_answer">
                                                            <p>NutraHer Lean is covered by a 60 Day, 100% Money Back Guarantee…</p>

                                                            <p>Which means you have a full two months to try out NutraHer Lean for youself….</p>
                                                            
                                                            <p>And if you change your mind about this investment for any reason at all..</p>
                                                            
                                                            <p>Simply call or email our customer service team and we’ll immediately refund your investment with no questions asked.</p>
                                                            
                                                            <p>Plus you don’t have to return your bottle…</p>
                                                            
                                                            <p>It’s yours to keep as our way of saying “thanks” for putting your faith in our team.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix" class="collapsed qa_question font_regular opensans_font">Okay, how do I get started?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <div class="qa_answer">
                                                            <p>That part’s easy!</p>

                                                            <p>Get your own supply of NutraHer Lean right now by clicking the button below right now.</p>
                                                            
                                                            <p>You’re order will then arrive in the next 3-5 business days…</p>
                                                            
                                                            <p>And we’re also including your shipping and handling for free!</p>
                                                            
                                                            <p>It all starts with you clicking the button below and completing checkout, and you’ll feel so good about what’s to come.</p>
                                                            
                                                            <p>Thank you for watching. Click the button below now.</p>
                                                         
                                                                                        
                                                            <p>&nbsp;</p>
                                                            <div style="text-align: center;">
                                                                <div><a href="#"  onclick="grayOut();"><img src="img/threebottles.png" /></a></div>
                                                                <div><a href="#"  onclick="grayOut();"><img src="img/buynow1.png" /></a></div>
                                                            </div>         
                                                            <p>&nbsp;</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!--                                             
                                        <div class="faq_container">
                                        </div>-->
                                    </div>
                                    
                                </div>
                                 
                            </div>
                           
                        </div>
                         <div style="text-align:center; margin:0 auto;margin-top: 30px;"><a href="#"  onclick="grayOut();"><img src="img/buynow1.png" /></a></div>
                    </div>
                                     
                </div>                
            </div>
            
            <!--<div class="row" style="padding: 45px 45px 50px;">
                <div class="col-sm-12">
                    <div class="text_center"><img src="img/get_gf_button.png" /></div>
                </div>
            </div> -->
            
        </div>        
    </div>
    <div class="footer_section">
    
    	<div class="footer_menu_container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a></li>       
                <!--<li><a onClick="window.onbeforeunload = null;" href="#" target="_blank">Get NutraHer Now</a></li>-->          
            </ul>
        </div>
    
        <div class="footer_text_container text_center">
            <p class="roboto_font font_light txt_17 light_gray_2">The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
        </div>
        
        <div class="copywright_text">
            <span>Copyright &copy; NutraHER 2016</span>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script>
    jQuery(function() {
		// var counter = 599;
		var one        = 0;
		var ten        = 0;
		var hundered   = 6;
		var intervalId = setInterval(function(){
			time();
		}, .7);
    
    	function time(){
			one--;
		
			if(one == -1){
				ten = ten - 1;
				one = 0 + 9;
			} 
			if(ten == -1 ){
				hundered = hundered - 1;
				ten = 0 + 9;
			}
			var wholeNum = hundered+''+ten+''+one;
			if(wholeNum == 250){
				clearInterval(intervalId);
			}
		
			$('.timer').html('<span>'+hundered+'</span><span>'+ten+'</span><span>'+one+'</span>');
		
		}
		
		var min    = 0;
		var second = 00;
		var zeroPlaceholder = 0;
		
		var counterId = setInterval(function(){
			countDown();
		}, 1000);
		
		function countUp () {
			second++;
			if(second == 59){
				second = 00;
				min = min + 1;
			}
			if(second == 10){
				zeroPlaceholder = '';
			}else {
				if(second == 00){
					zeroPlaceholder = 0;
				}
		
				jQuery('.count-up').html(min+':'+zeroPlaceholder+second);
			} 
		}
		
		var min1    = 9;
		var second1 = 59;
		var zeroPlaceholder1 = 0;
		
		function countDown () {
			second1--;
			if(min1 > 0) {
				if(second1 == -1){
					second1 = 59;
					min1 = min1 - 1;
				}
				if(second1 >= 10){
					zeroPlaceholder1 = '';
				}else if(second1 < 10) {
					zeroPlaceholder1 = 0;
				}else {
					if(second1 == 00){
						zeroPlaceholder1 = 0;
					}			
				} 
				jQuery('.count-up').html(min1+':'+zeroPlaceholder1+second1);
			}else {
				jQuery('.count-up').html('0:00');
			}
		}   
    });
		
	jQuery('.product_checkbox input:radio').change(function(){
		jQuery('.product_checkbox').removeClass("selected");
		jQuery('.checked').addClass("hidethis");
		jQuery('.notchecked').removeClass("hidethis");
		var getclass = jQuery(this).attr("class");
		if(jQuery(this).is(":checked")) {
			jQuery('.'+getclass+' .product_checkbox').addClass("selected");
			jQuery('.'+getclass+' .checked').removeClass("hidethis");
			jQuery('.'+getclass+' .notchecked').addClass("hidethis");
		} else {
			jQuery('.'+getclass+' .product_checkbox').removeClass("selected");
			jQuery('.checked').addClass("hidethis");
			jQuery('.notchecked').removeClass("hidethis");
		}
	});
	
	jQuery('.product_checkbox').click(function(){
		jQuery('.product_checkbox').removeClass("selected");
		jQuery(this).addClass("selected");
		jQuery('.checked').addClass("hidethis");
		jQuery('.notchecked').removeClass("hidethis");
		jQuery('.product_checkbox input:radio').removeAttr("checked");
		
		jQuery(this).find("input:radio").attr('checked', true);
		
		var getclass = jQuery(this).find("input:radio").attr("class");
			
		if(jQuery(this).find("input:radio").is(":checked")) {
			jQuery('.'+getclass+' .product_checkbox').addClass("selected");
			jQuery('.'+getclass+' .checked').removeClass("hidethis");
			jQuery('.'+getclass+' .notchecked').addClass("hidethis");
		} else {
			jQuery('.'+getclass+' .product_checkbox').removeClass("selected");
			jQuery('.checked').addClass("hidethis");
			jQuery('.notchecked').removeClass("hidethis");
		}
		
	});
	
	jQuery('#billing_address_same').change(function(){
		if(!jQuery(this).is(":checked")) {
			jQuery(".differentshipping").removeClass("hidethis");
		}
		
		if(jQuery(this).is(":checked")) {
			jQuery(".differentshipping").addClass("hidethis");
		}
	});
	
	/*jQuery(function ($) {
    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="glyphicon glyphicon-minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="glyphicon glyphicon-plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
        $('#accordion .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })
});*/
	jQuery(function ($) {
		var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
		$active.find('a').prepend('<div class="plus_minus_container"><span class="according_icon icon_minus"><img class="minus_img" src="img/minus.png" /><img src="img/plus.png" class="plus_img" /></span></div>');
		$('#accordion .panel-heading').not($active).find('a').prepend('<div class="plus_minus_container"><span class="according_icon icon_plus"><img class="minus_img" src="img/minus.png" /><img src="img/plus.png" class="plus_img" /></span></div>');
		$('#accordion').on('show.bs.collapse', function (e)
		{
			$('#accordion .panel-heading.active').removeClass('active').find('.according_icon').toggleClass('icon_plus icon_minus');
			$(e.target).prev().addClass('active').find('.according_icon').toggleClass('icon_plus icon_minus');
		});
		$('#accordion').on('hide.bs.collapse', function (e)
		{
			$(e.target).prev().removeClass('active').find('.according_icon').removeClass('icon_minus').addClass('icon_plus');
		});
	});
    </script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Expires" content="30" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ActivGuard</title>
	<meta name="robots" content="noindex">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="style.css?v=1.0.1" rel="stylesheet">
    <style>
	.boldtext{
		font-weight:bold;
		color:#000;
	}
	.centerheading{
		color:#000 !important;
	}
	
	ul.tick li:before {
		content: '✔';   
		margin-left: -1em; 
		margin-right: 1.100em;
	}
	
	ul.tick {
		padding-left: 60px;
		text-indent: -17px;
		list-style: none;
		list-style-position: outside;
	}
	ul.cross li:before {
		content: 'x';   
		margin-left: -1em; 
		margin-right: 1.100em;
		color:red;
	}
	
	ul.cross {
		padding-left: 60px;
		text-indent: -17px;
		list-style: none;
		list-style-position: outside;
	}
.control div.btnPlay{
	display:none!important;
}

/* FAQ accordion start */
.hr_line {
	border-bottom: 1px solid #5e782c;
	padding: 5px;
}

.qa_question {
	color: #454545 !important;
	font-size: 22px;
	padding: 20px 0px;
	border-bottom: 1px solid #cfdcb5;
	text-transform: none;
	text-decoration: none !important;
}

.qa_answer {
	color: #454545;
	font-size: 16px;
	font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	padding: 20px 0px 0px;
}

.faq_gr_section .panel .panel-title {
	text-align: left !important;
}

.faq_container {
	padding: 0px 50px;
}

.qa_answer p {
	font-size: 16px;
	font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	color: #454545;
}

.faq_gr_section .panel {
    margin-bottom: 20px;
    background-color: #fff;
    border-bottom: 1px solid rgb(88, 88, 88) !important;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}

.faq_gr_section .panel-default>.panel-heading {
	background-color: #eef7ff !important;
}
.panel-default>.panel-heading{
	color:#000 !important;
}
#accordion .panel-heading {
    padding: 0px 0px!important;
}
.icon_minus .minus_img {
    display: inline-block;
}
.faq_gr_section .panel-default {
    border-color: transparent !important;
}

.qa_question {
	font-size: 20px !important;
}

.panel-heading {
	padding-left: 0px !important;
}

.effect2:before, .effect2:after {
    z-index: -1;
    position: absolute;
    content: "";
    bottom: 10px;
    left: 10px;
    width: 50%;
    top: 80%;
    max-width: 300px;
    background: #b9b4b4;
    -webkit-box-shadow: 0 15px 10px #b9b4b4;
    -moz-box-shadow: 0 15px 10px #b9b4b4;
    box-shadow: 0 15px 10px #b9b4b4;
    -webkit-transform: rotate(-3deg);
    -moz-transform: rotate(-3deg);
    -o-transform: rotate(-3deg);
    -ms-transform: rotate(-3deg);
    transform: rotate(-3deg);
}
.greenfont {
    color: #0c66b2;
}
.box-bg {
    background-color: #f5f5f5;
    border: 1px solid #8ec0ed;
    font-weight: 700;
    padding: 15px 0;
}

.effect2:after {
    -webkit-transform: rotate(3deg);
    -moz-transform: rotate(3deg);
    -o-transform: rotate(3deg);
    -ms-transform: rotate(3deg);
    transform: rotate(3deg);
    right: 10px;
    left: auto;
}

.effect2 {
    position: relative;
}
.box_1 {
    width: 734px;
    margin: 0px auto;
    z-index: 3;
    max-width: 100%;
}
.well {
			min-height: 20px !important;;
			padding: 20px 100px !important;;
			margin-bottom: 20px !important;;
			background-color: #eef7ff !important;;
			border: 1px solid #8ec0ed !important;;
			border-radius: 4px !important;;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05) !important;;
			box-shadow: inset 0 1px 1px rgba(0,0,0,.05 !important;);
		}
		.content .well-shade h2 {
			font-size: 36px;
			font-weight: 700;
			color: #6f3b03;
		}
		.box {
			padding: 30px 100px !important;
		}
		.well-shade .intermediate {
			color: #588a17!important;
			display: block;
			font-size: 26px!important;
			margin: 3px 0;
		} 
		h3.red {
			background-color: rgb(255, 47, 44);
			padding: 10px 0px;
			color: #fff;
			text-align: center;
			font-weight: 700;
			font-size: 30px;
		}
		.well-shade .intro {
			display: block;
			font-family: 'Roboto', sans-serif;
			font-size: 20px;
			font-weight: 300;
			margin-bottom: 10px;
		}
		.well-shade {
			background-color: #eef7ff !important;;
			border: 1px solid #8ec0ed !important;;
			text-align: center !important;;
			margin-bottom: 30px !important;;
		}
		
		.content {
			font-family: 'Open Sans', sans-serif;
		}
		
		.cite-item .content {
			font-size: 16px;
		}
		.panel .panel-heading {
    background-size: contain;
    background-color: #eef7ff;
    color: #000;
     background-repeat: repeat;
   
}
.qa_question {
    font-size: 22px;
    padding: 20px 0px;
    text-align: left;
    color: #000000 !important;
    font-weight: 600;
    font-family: 'Open Sans', sans-serif;
}

@media (max-width: 789px){
.effect2:before, .effect2:after {
    z-index: -1;
    position: absolute;
    content: "";
    bottom: 10px;
    left: 50px;
    width: 50%;
    top: 80%;
    max-width: 300px;
    background: #b9b4b4;
    -webkit-box-shadow: 0 15px 10px #b9b4b4;
    -moz-box-shadow: 0 15px 10px #b9b4b4;
    box-shadow: 0 15px 10px #b9b4b4;
    -webkit-transform: rotate(-3deg);
    -moz-transform: rotate(-3deg);
    -o-transform: rotate(-3deg);
    -ms-transform: rotate(-3deg);
    transform: rotate(-3deg);
}

.effect2:after {
    -webkit-transform: rotate(3deg);
    -moz-transform: rotate(3deg);
    -o-transform: rotate(3deg);
    -ms-transform: rotate(3deg);
    transform: rotate(3deg);
    right: 50px;
    left: auto;
}

		.box {
			padding: 30px 25px !important;
		}
	}
	
	.icon_minus .minus_img {
		display: inline-block;
	}
	
	.icon_plus .minus_img {
		display: none;
	}
	
	.icon_plus .plus_img {
		display: inline-block;
	}
	
	.icon_minus .plus_img {
		display: none;
	}
	
	.plus_minus_container {
		width: 40px;
		height: 100%;
		position: absolute;
		background: #f15d4e;
		left: 0px;
		top: 0px;
	}
	
	.according_icon {
		padding-right: 15px;
	}
	
	.qa_answer {
		color: #4e4e4d;
		font-size: 16px;
		font-family: 'Open Sans', sans-serif;
		font-weight: 400;
		padding: 0px 40px 0px;
		line-height: 35px;
	}
	
	.icon_minus .minus_img {
		display: inline-block;
	}
	
	.plus_minus_container {
		width: 40px;
		height: 100%;
		position: absolute;
		background: #00457c;
		left: 0px;
		top: 0px;
	}
/* FAQ accordion end */
	
	@media (max-width: 767px) {
		#citation h3:before {
			width: 85%;
			left: 50%;
			top: 50%;
			transform: translate(-50%,-50%);			
		}
		
		.container {
			overflow: hidden;
		}
	}
	
	@media (max-width: 665px) {
		.mainheading {
		    font-size: 38px !important;
		}
		
		.breakdiv {
			display: none;
		}
	}
	@media (max-width: 767px) {

		.img-responsive.logo {
		    padding: 15px 0px 15px;
		}
	}	

	@media (max-width: 767px) {

		#header .text {
		    padding-top: 0px;
		    padding-bottom: 15px;

		}
		
		.faq_main_title_container {
			padding-left: 15px !important;
		}
		
		.faq_container {
			padding: 0px 15px !important;
		}
	}
	
	
	</style>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!--<script src="/assets/js/Framework.js"></script>-->
<!--[if lt IE 9]>
<script>
$(document).ready(function() {
	$('.control').hide();
	$('.loading').fadeOut(500);
	$('.caption').fadeOut(500);
});
</script>
<![endif]-->

<script type="text/javascript">
    var mysrc = "https://tracking.softwareprojects.com/track/?a=4861&firstcookie=0&referrer="+encodeURIComponent(document.referrer)+"&product=GRX1&sessid2="+ReadCookie('sessid2');

    var newScript = document.createElement('script');
    newScript.type = 'text/javascript';
    newScript.async = true;
    newScript.src = mysrc;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(newScript, s);
    function ReadCookie(name){name += '='; var parts = document.cookie.split(/;\s*/);for (var i = 0; i < parts.length; i++) {var part = parts[i]; if (part.indexOf(name) == 0) return part.substring(name.length)} return '';}
</script>

<script type="text/javascript">
//window.onbeforeunload = grayOut;
function grayOut(){
var ldiv = document.getElementById('LoadingDiv');
ldiv.style.display='block';
}
</script>
<script type="text/javascript" src="js/jqeury.js"></script>
<link rel="stylesheet" type="text/css" href="css/custom.css">
   
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WG3MPM3');</script>
<!-- End Google Tag Manager --><script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-89675103-1', 'auto');  ga('send', 'pageview');</script>
<meta name="google-site-verification" content="88FZm9_l20oieVLskbZSQdEb3Unoqv8HlSc8qI94ym0" />
</head>
<body onLoad="init();">
<!--<div style="height:0;"><img src="//pixel.adblade.com/imps.php?sgms=17396" border="0" /></div>-->

<div id="LoadingDiv" style="display:none;">One Moment Please...<br /><img src="img/progressbar.gif" class="displayed" alt="" /></div>

	<header id="header" >
		<div class="container">
			<div class="row">
				<div class="col-sm-6">

					

                    <a href="https://greenreliefnow.com/" onClick="window.onbeforeunload = null;"><img src="img/logo.png" class="img-responsive logo"></a>

				</div>
				<div class="col-sm-6 vid-content" >
					<div class="text"><span class="glyphicon glyphicon-volume-up"></span> Remember to turn your sound on!</div>
				</div>
			</div>
		</div>
        <div style=" background-image:url(img/headerbg.png); background-repeat:no-repeat; height:10px; width:100%"></div>
	</header>
    
    
    <div class="div-content-teaser">
	<section  class="vid-content" id="teaser">
		<div class="container">
			<h3 style="color:#00457c; font-size:18px;font-family: 'Roboto Condensed', sans-serif;font-weight: 700; margin-top: 55px;">(Please wait up to 3 seconds for the video to load)</h3>
			<h2 style="color: #00457c; font-family: 'Roboto', sans-serif; font-size:18px;"></h2>
			<h2 style="color:#0c7e29;font-size: 40px; font-family: 'Oswald', sans-serif; font-weight: 700;" class="mainheading">My Wife's "New Weird Body Part" & How
            <div class="breakdiv"></div>Discovering It Eradicated Our Over Active Bladders</h2>
			<div id="vid" style="padding-bottom: 40px;">
            	<div class="row">                	
                	<div class="col-md-10 col-md-offset-1" >
                    	<div class="overlay" style="position:absolute; width: 100%; z-index: 1000000001; height: 100%;"></div>
                        	<div class="effect2 box_1">
                            	<div class="embed-responsive2">
                                	<div id="vimeoWrap" class="videoContainer">
                                    
                                        <video autoplay controls id="playerid2">
                                            <source src="https://greenreliefnow.com/video/CBD_Green_Relief_faster_.mp4" type="video/mp4" >
                                            Your browser does not support the video tag or the file format of this video. 
                                        </video>
                                   
                                    
                                        <div class="control">
                                            <div class="btmControl">
                                                <div class="btnPlay btn-cnt" title="Play/Pause video"></div>
                                            </div>                                    
                                        </div>
                                    	<div class="loading"></div>                               
                                    
                                	</div>
                            	</div> 
                        	</div>                                      	
                    	</div>
                    <div style="display:none;" class="video_button"><center><a style="background: none !important;border: none !important;box-shadow: none !important;" href="step1.php" onClick="grayOut();"><img style="margin-top: 45px;" src="img/green_btn.png" /></a></center>
                    <div class="cards">
		<img src="img/cards.png" class="img-responsive center-block cards">
	</div>
                    </div>
                    
            	</div>
			</div>
			<div class="rightBackground"></div>
        </div>
        <div class="getGuard" >
		
	</div>

	
	</section>
	</div>
	
	
	<section class="vid-content" id="citation">
		<div class="container" style="padding-right: 0px;">
			<h3 style="font-family: 'Roboto', sans-serif; font-weight:bold;"><span>Works Cited</span></h3>
			<div class="getGuard">
				<img src="img/symbols.gif" style="padding: 30px 30px;" class="img-responsive center-block symbols">
			</div>
<div id="grid" class="row" data-columns>
			  	<div>
					<div class="col-lg-12 cite-item">
						<div class="num">1</div>
				  		<div class="content">
							  https://www.rush.edu/health-wellness/discover-health/protecting-prostate
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">2</div>
				  		<div class="content">
							http://www.reuters.com/article/us-vitamin-b-idUSTRE58L3MD20090922
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">3</div>
				  		<div class="content">
							https://www.ncbi.nlm.nih.gov/pubmed/27175597
						</div>
					</div>	
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">4</div>
				  		<div class="content">
							https://academic.oup.com/jnci/article/doi/10.1093/jnci/djw153/2576953/Selenium-and-Prostate-Cancer-Analysis-of
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">5</div>
				  		<div class="content">
							https://www.ars.usda.gov/research/publications/publication/?seqNo115=212739
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">6</div>
				  		<div class="content">
                  				https://www.lahey.org/Departments_and_Locations/Departments/Cancer_Center/Ebsco_Content/Prostate_Cancer.aspx?chunkiid=22198
						</div>
					</div>
				</div>
				
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">7</div>
				  		<div class="content">
							Piscoya J, Rodriguez Z, Bustamante SA, et al. Efficacy and safety of freeze-dried cat's claw in osteoarthritis of the knee: mechanisms of action of the species Uncaria guianensis. Inflamm Res. 2001;50:442-448.
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">8</div>
				  		<div class="content">
							Mur E, Hartig F, Eibl G, et al. Randomized double blind trial of an extract from the pentacyclic alkaloid-chemotype of uncaria tomentosa for the treatment of rheumatoid arthritis. J Rheumatol. 2002;29:678-681.
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">9</div>
				  		<div class="content">
							https://www.ncbi.nlm.nih.gov/pubmed/20155615
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">10</div>
				  		<div class="content">
							http://www.medscape.com/viewarticle/487510 
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">11</div>
				  		<div class="content">
                  https://www.ncbi.nlm.nih.gov/pubmed/23883692 
						</div>
					</div>
				</div>
                
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">12</div>
				  		<div class="content">
							Ernst E, Chrubasik S. Phyto–anti-inflammatories. A systematic review of randomized, placebo-controlled, double-blind trials. Rheum Dis Clin North Am. 2000;26:13-27.
						</div>
					</div>
				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">13</div>				  		<div class="content">							Pittler MH. Complementary therapies for treating benign prostatic hyperplasia. FACT. 2000;5:255-257.						</div>					</div>				</div>																<div>					<div class="col-lg-12 cite-item">						<div class="num">14</div>				  		<div class="content">							http://umm.edu/health/medical/altmed/herb/stinging-nettle 						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">15</div>				  		<div class="content">							https://www.ncbi.nlm.nih.gov/pubmed/10851301 						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">16</div>				  		<div class="content">							http://www.nhs.uk/news/2009/11November/Pages/anti-cancer-mushroom-research.aspx						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">17</div>				  		<div class="content">							Mayell M . Maitake extracts and their therapeutic potential . Altern Med Rev . 2001;6:48-60.						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">18</div>				  		<div class="content">							Adachi K , Ohno N , Ohsawa M , Yadomae T . Change of biological activities of (1----3)-beta-D-glucan from Grifola frondosa upon molecular weight reduction by heat treatment . Chem Pharm Bull . 1990;38:477-481.						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">19</div>				  		<div class="content">							Ohno N , Asada N , Adachi Y , Yadomae T . Enhancement of LPS triggered TNF-alpha (tumor necrosis factor-alpha) production by (1-->3)-beta-D-glucans in mice . Biol Pharm Bull . 1995;18:126-133.						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">20</div>				  		<div class="content">							Nanba H . Townsend Lett Doct Patient . 1996;84-85.						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">21</div>				  		<div class="content">							http://www.israel21c.org/israeli-scientists-reveal-the-prostate-healing-power-of-mushrooms/ 						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">22</div>				  		<div class="content">							https://www.lahey.org/Health_and__Wellness_Information/Ebsco_Content/Fighting_Prostate_Cancer__Eat_Your_Way_to_Victory.aspx?chunkiid=220562 						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">23</div>				  		<div class="content">							https://www.ncbi.nlm.nih.gov/pubmed/21798389 						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">24</div>				  		<div class="content">							https://books.google.com/books?id=SVEUG8h_tmgC&lpg=PA86&ots=larzqphdHC&dq=buchu%20leaf%20prostate&pg=PA86#v=onepage&q=buchu%20leaf%20prostate&f=false						</div>					</div>				</div>												<div>					<div class="col-lg-12 cite-item">						<div class="num">25</div>				  		<div class="content">							http://www.allinahealth.org/ccs/doc/alternative_medicine/48/10090.htm 						</div>					</div>				</div>
				<!--<div>
					<div class="col-lg-12 cite-item">
						<div class="num">13</div>
				  		<div class="content">
							American War Deaths Through History http://www.militaryfactory.com/american_war_deaths.asp
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">14</div>
				  		<div class="content">
							Arnica montana gel in osteoarthritis of the knee: an open, multicenter clinical trial. http://www.ncbi.nlm.nih.gov/pubmed/12539881
                            <b>Department of Rheumatology, Valens Clinic for Rheumatism, Valens, Switzerland. Abstract</b>
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">15</div>
				  		<div class="content">
							Choosing between NSAID and arnica for topical treatment of hand osteoarthritis in a randomised, double-blind study. http://www.ncbi.nlm.nih.gov/pubmed/17318618
                            <b>Rheumatology Clinic, 9004, St Gallen, Switzerland.</b>
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">16</div>
				  		<div class="content">
							Beta-carotene and other carotenoids as antioxidants. http://www.ncbi.nlm.nih.gov/pubmed/10511324
                            <b>Faculdade de Medicina de Botucatu, Universidade Estadual Paulista, São Paulo, Brazil.</b>
						</div>
					</div>
				</div>
				<div>
					<div class="col-lg-12 cite-item">
						<div class="num">17</div>
				  		<div class="content">
							Beta-carotene http://umm.edu/health/medical/altmed/supplement/betacarotene
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</section>
    
	<!----=========== Second page content here ========================================--> 
	<section class="sec-content" style="background-size:cover !important; background-repeat:no-repeat;" id="teaser2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="box shade text-left">
            			<h2 style="color:white;font-size: 37px;font-family: 'Roboto Condensed', sans-serif;font-weight: 700;">My Wife’s “New Weird Body Part” <br />& How Discovering It Eradicated Our Over Active Bladders</h2>
                        <p>
                        I was at my nephew’s graduation when the final straw BROKE…<br/><br/>
            My brother just recently passed away…<br/><br/>
            And I know my brother’s son felt pretty alone in the world…<br/><br/>
            It made me proud to step in as a father figure at his hard-fought college graduation…<br/><br/>
            Represent myself and my brother on a happy occasion…<br/><br/>
            <span class="boldtext">I HAD to run to the bathroom.</span><br/><br/>
            <span class="boldtext">Even though I had went less than an hour before.</span></p>
               
            <p>And even though my oldest nephew, son of my closest brother, was about to receive the diploma that meant so much to him, his dad, and me…<br/><br/>
            I couldn’t’t sit in the audience a moment longer.<br/><br/>
            I had to get up and walk away.<br/><br/>

			Miss his moment.<br/><br/>

			Picture his face looking out for me in the audience.<br/><br/>

			His own father couldn’t be there… and now neither could I?<br/><br/></p>
            
            <h3 class="box-bg text-center greenfont font_700">Because of a stupid need to pee… again???</h3><br/>
            
            <p class="">While my wife understood my irritation and embarrassment (she’s had similar issues for years…)</p>
            <p class="">I knew that I let both my nephew and my brother down…</p>
             <p class="">I was at a peak of frustration with myself, and terrified about this getting even worse and developing into a serious health problem…</p>
             <p class="boldtext">That was the day I decided to do whatever it took to eliminate my stupid “bladder problem” forever.</p>
             <h3 class="box-bg text-center greenfont font_700">And what I found was the little-known way to eliminate your nagging need to go, for good…</h3><br/>
                    
            <ul class="tick">
                <li>A bizarre but incredibly effective, all-natural remedy that gives you complete control of your bladder again.</li>
                <li>Leaving you feeling comfortable, confident, and NORMAL, with no nagging urges anymore.</li>
                <li>It’s worked for thousands of people across the U.S and counting.</li>
                </ul>                                                                  
                                                                          
            
            <p> And when you see just how simple it is, you might kick yourself for not knowing about it earlier.</p>
           
            <p>But be glad you’re discovering it now.</p>
            <p>Because getting this powerful bladder elixir today can mark a true turning point for you.</p>
            <p>Where you can live each day comfortably from here on out…</p>
            <div class="well well-shade">
            	<h3 class="greenfont font_700" style="font-weight:bold;">In total control of your bladder, without having to even think about it.</h3>
            	<center><img src="img/Review.jpg"/></center>
            </div>
            <p>Just like what happened to Dave from Illinois, who writes...</p>
            <p><i>"Wow. This marks 7 days since I’ve started. 5 days with no feelings of urgency at all. I forgot what it was like to go to the bathroom like a normal person. My life is really changed for the better, and I am so, so grateful."</i></p>
            <p>Or Janice from San Diego, who wrote in...</p>

            <p><i>"I was ready to accept the fact that I'd have this problem forever, but now it is totally gone. I stopped buying protective pads, I go to movies without even thinking about it… having to get up multiple times during a movie used to embarrass me so much, but now I really feel like that part of my life is over. I also sleep all through the night now without having to get up to go to the bathroom. THANK YOU!"</i></p>
            
            <p>I could share letters from hundreds of people just like these.<p>
            
            <p>And what’s most important is that YOU find the same relief they did. The same...<p>
            
            
            <h3 class="box-bg text-center greenfont font_700">Life-changing renewed confidence, freedom, and happiness that you can have starting today.</h3><br/>
            <p>Because listen... if you’ve been:</p>
            
            <ul class="cross">
            	<li>Waking up in the night</li>
                <li>Missing out on moments with your loved ones...</li>
                <li>Dealing with... or even contemplating... the embarrassment of wearing a pad or a diaper...</li>
                <li>Or just not being able to focus on life's good times...</li>
            </ul>
            
            <p><span style="" class="">All because of a constant need to go…</span></p>
            <h3 class="box-bg text-center greenfont">…then it’s time for your <br/><span style="font-weight:bold; text-decoration: underline">completely UNNECESSARY suffering</span> to stop…</h3><br/>
            <p>Now, to be honest, it took time for me to feel comfortable sharing this remedy with the world.</p>
            <p>It’s embarrassing to admit, “I have bladder troubles”… It’s seen as a sign of aging, and just not cool at all…</p>
            <p>You begin to ask yourself some scary questions, like…</p>
            <p><i>What if the problem never stopped...</i></p>
            <p><i>What if the urgency became constant… with a “need to go feeling” always stealing my focus and attention?</i></p>

            <p><i>What if I lost control? Or was ever “too late” to a restroom?</i></p>
            
            <p><i>What if this problem makes it harder for my wife to love me?</i></p>
            
            <p><i>Or harder to find someone special in the first place?</i></p>
            
            <p><i>What if this escalates into a real medical issue that can take years off my life?</i></p>
            
            <p>Not only do you have these bladder troubles, but you become stressed as heck about all of this other stuff.</p>

			<p>So I know just how hard it is to face the reality that you have an issue.</p>
            
            <h3 class="box-bg text-center greenfont">And when I realized that the so-called cures most “doctors” give you don’t work…</h3>
            <br/>
            <p>That they actually hurt your health and overall make you feel worse…</p>

            <p><strong>Giving you bad side effects like grogginess and lack of libido…</strong></p>
            
            <p>It became all the more important for me to step forward and reveal the facts about your constant need to go.</p>
            
            <p>The good news is that this issue has been PROVEN to be easier to treat than you might think.</p> 
            
            <p>That’s why it’s so important for both your health and your quality of life that you pay very close attention here.</p>
            
            <p><strong>Because I do believe that “big pharma” and the billion dollar health industry WANTS to keep the path to urgency-relief complicated and confusing.</strong></p>
            
            <p>Letting us all believe that overactive bladder, and even incontinence, are just signs of aging that are too “complex” to cure…</p>
            
            <p>… or just plain unavoidable.</p>
            
            <p>… telling us to do silly sex exercises like Keagles… or to even have complicated, painful surgery.</p>
            
            <p><strong>I’m sure the makers of all those God-awful “undergarments” would like us to believe there’s nothing we can do to regain bladder health and control.</strong></p>
            
            <p>This all means that I could be sanctioned to take this website down at any time.</p>
            
            <p>Because it shares the TRUE, natural, simple source of relief from your urinary issues.</p>
            
            <p>Nutrients proven by numerous clinical studies the world over.</p>

			<h3 class="box-bg text-center greenfont">And heck… if a handful of natural herbs and vitamins that you can <span style="font-weight: bold;">pick up from your closest store right now</span> can <span style="font-weight: bold;"><u>eliminate your nagging need to go…</u></span><br /><br />Then shoot… <span style="font-weight: bold;"><u>you NEED to know</u></span> about it, right?</h3><br/>
            
            <p>Of course.</p>

            <p>So let me get started…</p>
            
            <p>Hi, I’m Tom Mapler, and I’ll admit, I discovered this remedy in a pretty weird way…</p>
            
            <p>It began with finding out that my wife, and all women…</p>
            
            <p>Actually have a prostate just the same as men do.</p>
            
            <p>And because both of our prostate glands were enlarged, we’d been suffering with urgency for YEARS.</p>
            
            <p><strong>Now that we both take the nutrients I’m about to share with you, we have don’t deal with ANY abnormal urgency.</strong></p>
            
            <p>You heard that right… NO more urgency.</p>

			<p>Since the nutrients we take come right from mother Earth, there are no scary side-effects…</p>
            
            <h3 class="box-bg text-center greenfont">And when you take these nutrients in the specific way I’m about to share, you could become in total control of your bladder… Like you were in your 20s all over again…</h3><br/>
            
            <p>PLUS, instead of side-effects, there are dozens of side BENEFITS, like better digestive health, mental clarity, even stronger bones and muscles, which surprised me.</p>

            <p>After the true shame that I felt over my nephew’s graduation, I got to work, researching everything I possibly could about the topic of urinary and bladder health.</p>
            
            <p><strong>It’s actually pretty interesting, and here’s how things work:</strong></p>
            
            <p>First… Urine enters your bladder from your kidneys, and as your bladder fills up, nerve signals get sent to your pelvic muscle…</p>
            
            <p>This causes your pelvic muscle to contract.</p>
            
            <p>That’s the “feeling” of having to go to the bathroom…  a series of contractions in your pelvic muscle….</p>
            
            <p>When it works normally, you feel the feeling, and have ample time to comfortably make it to the bathroom.</p>
            
            <p>You enter your whole bladder and continue about your day.</p>
            
            <p>And normally this happens 6 – 10 times a day.</p>
            
            <p>All standard biological function.</p>
            
            <p>But what goes wrong somewhere along the way, often as we get older…</p>
            
            <p>…Is that those nerve signals that make our pelvis contract get activated more and more…</p>
            
            <p>Even when your bladder is only a little full.</p>
            
            <p>The nerve signals can also come out of nowhere and feel very intense very fast.</p>
            
            <p>What causes the nerve signals to fire more?</p>
            
            <p><strong>One of the main reasons our nerve signals fire is more is due to pressure from a swollen prostate.</strong></p>
            
            <p>But “swollen prostate” I thought, only applied to me.</p>
            
            <p>It didn’t explain my wife’s consistent need to go, and why she sometimes goes a little when she laughs really hard…</p>
            
            <p>See… the whole time I was trying to look for two separate things at once: a way for men to get relief, and a way for women to get relief.</p>
            
            <p><strong>I thought that nutrients that reduce prostate swelling could help men, and that there was some “other” remedy for women.</strong></p>
            
            <p>Until my own wife told me, “women have a prostate, silly.”</p>
            
            <p>Turns out that the female prostate is commonly called “Skene’s glad”.</p>
            
            <p><strong><u>And as we age, the female prostate can swell JUST like the men’s does.</u></strong></p> 

            <p>Putting pressure on the bladder and causing the sense of urgency we want to be rid of.</p>
            
            <p>This discovery was like a light bulb turning on.</p>
            
            <p>There aren’t two separate solutions.</p>
            
            <h3 class="box-bg text-center greenfont">There is ONLY one thing we need to do: reverse and prevent prostate swelling.</h3><br/>
            
            <p><strong>And all of the tortures of an over-active bladder and incontinence can go away for good.</strong></p><br/>

            <p>As soon as I searched for all-in one relief for prostate swelling, I discovered the brand B Naturals.</p>
            
            <p>Now the good folks, doctors, and scientists at B Naturals already knew about the female prostate…</p>
            
            <p>AND how to shrink any enlarged prostate all naturally.</p>
            
            <h3 class="box-bg text-center greenfont">See… there is a handful of very common nutrients and vitamins that have been scientifically PROVEN to help reduce prostate swelling in men and women alike.</h3><br/>
            
            <p>And…</p>

            <p>When these nutrients are taken in the proper proportions…</p>
            
            <p>They can begin to shrink the prostate IMMEDIATELY…</p>
            
            <p><strong>Meaning you can notice your <u>sense of urgency decreasing right away</u> – which is truly incredible.</strong></p> 
            
            <p>So what are these prostate shrinking nutrients?</p> 
			            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #1: Zinc</h3><br/>
            
            <center><img src="img/1.Zinc.jpg" style="width: 60%;" /></center>
                        
            <p>Zinc has been shown in numerous studies to promote prostate health and to shrink the prostate gland.</p> 

            <p>And Rush University Medical Center in Chicago reports that Zinc, “not only prevents prostate enlargement” and can “help shrink a prostate gland that’s already swollen.</p>
            
            <p>And you can find Zinc in foods like pumpkin seeds, oysters, nuts and beans.</p>
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #2: Lycopene</h3><br/>
            
            <center><img src="img/2.Lycopene.jpg" style="width: 60%;" /></center>
            
            <p>Numerous studies, including ones published by the American Journal of Health, indicate that 10 – 15 mg of lycopene daily can promote prostate health and shrink an enlarged prostate.</p>

            <p>And in one clinical trial, individuals with enlarged prostates who were given lycopene saw up to an 18% reduction in the problematic protein called Prostate-Specific Antigen, which plays a key role in swelling.</p>
            
            <p>In the same study, those who were given a placebo, or sugar pill, saw no effects.</p>
            
            <p>Lycopene is found in tomatoes and tomato based products.</p>
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #3: Vitamin E</h3><br/>
            
            <center><img src="img/3.VitaminE.png" style="width: 60%;" /></center>
            
            <p>In just one of numerous studies, the United States Department of Agriculture reports that Vitamin E has been proven to inhibit prostate growth.</p>
            
            <p>Plus, their findings also indicate that individuals taking Vitamin E supplements can have a decreased risk of prostate growth.</p>
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #4: Stinging Nettle</h3>
            <br/>
			<center><img src="img/4.StingingNettle1.jpg" style="width: 60%;" /></center>
                        
            <p>Stinging Nettle is widely used throughout Europe to treat an enlarged prostate…</p>
            
            <p>And its effects have been proven to be comparable to prescription drugs commonly prescribed for an enlarged prostate.</p>
            
            <p>As the University of Maryland Medical Center reports, Stinging Nettle has been shown to “slow the growth of prostate cells,” reducing symptoms such as “reduced urinary flow, incomplete emptying of the bladder, post urination dripping, and the constant urge to urinate.”</p>
            
            <p>This makes Stinging Nettle one of the most powerful all-natural incontinence remedies. Others include…</p>
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #5: Maitake Mushroom:</h3><br/>
            
            <center><img src="img/5.MaitakeMushroom2.jpg" style="width: 60%;" /></center>
            
            <p>The US National Library of Medicine has reported that concentrations of Maitake Mushroom have eliminated up to 95% of prostate growth cells!</p> 
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #6: Reishi Mushroom:</h3><br/>
            
            <center><img src="img/6.ReishiMushroom.jpg" style="width: 60%;" /></center>
                        
            <p>Chinese tradition refers to the Reishi mushroom (Ganoderma lucidum) as ‘the lucky fungus’, for its powers in alleviating health issues for over 2,000 years! And researchers from the University of Haifa announced their success in preliminary efforts to slow the growth of prostate cells using reishi extracts.</p>
            
            <h3 class="box-bg text-center greenfont">Prostate Shrinking Nutrient #7: Saw Palmetto:</h3><br/>
            
            <center><img src="img/7.SawPalmetto.jpg" style="width: 60%;" /></center>

            <p>With hundreds of studies proving the effectiveness of Saw Palmetto in treating urinary issues, one of the most incredible findings is that Saw Palmetto can be as effective at eliminating urgency and incontinence as prescription drugs, WITHOUT the common side effects of a lost libido.</p>
            
            <p>Some of the other most potent all-natural nutrients that have been shown to help prostate health are:</p> 
            
            <p>Vitamin B-6, Shiitake Mushroom, Beta-Sitosterol, Red Raspberry Juice Extract, Graviola, and Broccoli Extract.</p> 
            
            <p>These have been proven effective in HUNDREDS of scientific studies just like those we’ve already discussed.</p>
            
            <h3 class="box-bg text-center greenfont">Now, before you run off to the store to load up on Zinc, Vitamin B-6, Vitamin E, Selenium, Copper, Saw Palmetto, Red Raspberry Juice Extract, and so on…<br /><br />(which would cost you a few hundred bucks in total)<br /><br />I’m about to make your life a heck of a lot easier.</h3><br/>
            
            <p>See… instead of buying a bunch of different vitamins and supplements…</p>

            <p>And trying to figure out the proper dose of each to ensure you reap the benefits of a healthy and normal bladder…</p>
            
            <p>The team of experts at B Naturals have <strong>combined the purest forms of these nutrients, in the perfect dosages, all into one tiny, easy-to-swallow capsule, called ActivGuard.</strong></p>
            
            <center><img src="https://activguardnow.com/img/multibottle.png"/></center><br/>
            
            <p>As soon as I found ActivGuard and learned that ALL of its ingredients are carefully sourced and designed to shrink the prostate and promote prostate health, I knew I had to try it for me and my wife…</p>
            
            <p>And just like thousands of other people who’ve tried ActivGuard, once we started, we never looked back.</p>

			<h3 class="box-bg text-center greenfont">I could feel a difference the very same day I started taking ActivGuard. It was subtle at first… but I noticed that I didn’t wake up in the night to use the bathroom.</h3><br/>

            <p>I thought it might be a fluke, but when my wife and I went to the movies that Friday night, neither of us had to take a bathroom break.</p>
            
            <p><strong><u>It had been years since we both sat in the theater for a full movie, without missing a second.</u></strong></p>
            
            <p>Needless to say, we were thrilled.</p> 
            
            <p>And we’ve been thrilled ever since.</p>
            
            <p>We both take ActivGuard every day, and in addition to having control of our bladders’ again, we also both feel healthier…</p>
            
            <p><strong>With more energy, mental clarity, and stamina…</strong></p>
            
            <p><strong>Which makes sense, because the nutrients inside ActivGuard are pure, powerful, and great for you.</strong></p>

			<h3 class="box-bg text-center greenfont">ActivGuard is made in a state of the art facility right here in the United States.</h3><br/>
            
            <p>And every single bottle of ActivGuard is quality screened by a third party, FDA compliant facility, also right here in the United States.</p>

            <p>This ensures that each bottle of ActivGuard is packed with the prostate-shrinking ingredients that are written on the label.</p>
            
            <p>The exact ingredients that have been proven to improve your sense of well-being by optimizing your prostate health.</p>
            
            <h3 class="box-bg text-center greenfont">What this means is, right here on this page, you have the opportunity to <span style="font-weight: bold;">effortlessly eliminate your urinary problems, the urges to go, and your incontinence.</span></h3><br/>
            
            <p>This can make you feel years younger, like a weight has been lifted off your shoulders.</p>

            <p><strong>You can feel free to take long road trips, sit for a long movie, or a long dinner date.</strong></p>
            
            <p><strong>Without any interruption or worry.</strong></p>
            
            <p>And as mentioned earlier, ActivGuard is PACKED with all-natural ingredients, meaning there are no side effects… and even cooler… there are awesome side-benefits…</p> 
            
            <p>Just to name a few:</p>
            
            <center><img src="https://activguardnow.com/img/multibottle.png"/></center><br />
            
            <p>the Quercitin found inside ActivGuard’s proprietary blend has been proven to reduce symptoms associated with chronic prostatitis/chronic pelvic pain syndrome.</p>
            
            <p>The Buchu Leaf - also found inside - has been held in great esteem by the natives of the Cape of Good Hope, as a remedy for a number of diseases, particularly irritative or chronic inflammatory affections of the urethra, bladder, prostate gland, and rectum.</p>
            
            <p>And the Gravel Root – an herbal medicine – is used to treat kidney stones, prostate gland problems, rheumatism (stiff and sore muscles and joints) and gout (swelling due to thyroid problems).</p> 

			<h3 class="box-bg text-center greenfont">So by now you might be wondering how to get your hands on ActivGuard yourself.</h3><br/>
            
            <p>The team at B Naturals spares no expense in sourcing the purest and highest quality forms of raw materials on the planet.</p> 

            <p><strong><u>Because of this, ActivGuard is ALWAYS in short supply.</u></strong></p>
            
            <p>ActivGuard is not some mass-market item that gets cranked out by the thousands.</p> 
            
            <p><strong>ActivGuard is an artisan, premium blend of vitamins and nutrients that’s been proven to deliver real results you can feel.</strong></p>
            
            <p>And for the opportunity to be rid of bladder problems, to feel younger, freer, and happier, and healthier…</p>
            
            <p>All without constant doctors visits, being poked and prodded, and all the side effects of expensive prescription drugs.</p>
            
            <p>The fact that ActivGuard can truly give you a happier and healthier life starting as soon as you get your first bottle…</p>
            
            <p>Is worth way more than what a few hundred bucks could buy you.</p>
            
            <p>That’s why, at $149 for a month’s supply of ActivGuard, you get an absolute bargain.</p>
            
            <p>But since B Naturals specializes in the sourcing of these pure and high quality ingredients, the savings can be passed on to you.</p>
            
            <p>That’s why you won’t be asked to invest $149…</p>
            
            <p>Or even $99</p>
            
            <p>Or even $79 for your supply of ActivGuard.</p>
            
            <p>But instead just $69…</p>
            
            <p><strong>$69 for your best chance at never having to struggle with urgency again.</strong></p>

 			<ul class="tick">
            	<li>To never have to get up multiple times during a dinner, a meeting, a movie, or a flight…</li>

				<li>To be in complete control of when and where you go, without question, thanks to a healthier prostate.</li>
            </ul>

			<h3 class="box-bg text-center greenfont">And when you purchase ActivGuard on this website, your premium shipping and handling is included for free, which is a $12.99 value.</h3><br/>
            
            <center><img src="https://activguardnow.com/img/gurantee_img.png" alt=""></center><br/>

            <p>Plus, your investment in ActivGuard today is backed by our no-questions-asked money back guarantee.</p>
            
            <p>You have a full 180 days, which is 6 months, which is half a year, to try out ActivGuard.</p>
            
            <h3 class="box-bg text-center greenfont" style="text-decoration: underline;">That means all you have to do is decide to TRY ActivGuard today.</h3><br/>
            
            <p>If for any reason you don’t love it (or heck, even if you DO) then all you have to do is call or e-mail our top-rated customer support team and we will give you an immediate refund of your investment.</p>
            
            <p><strong>You don’t even need to ship back the bottle. It’s yours to say THANK YOU for trying out ActivGuard.</strong></p>
            
            <p>And if you DO love ActivGuard, it’s likely because the end of your urgency has given you a new lease on life.</p>
            
            <p>That’s the whole point.</p>
            
            <div class="well well-shade">
            	<h3 class="greenfont">Click the button below to be rushed your no-risk supply of ActivGuard Right now.</h3>
                
                <center><img src="https://activguardnow.com/img/multibottle.png" alt="" class="img-responsive center-block no-border text-center"></center>
                
                <a style="background: none !important;border: none !important;box-shadow: none !important;" href="step1.php" onClick="grayOut();"><img style="margin-top: 45px;" src="img/green_btn.png"></a>
                
                <span class="intermediate"><b>When you click that button, you’ll see the opportunity to save EVEN MORE on ActivGuard by purchasing multiple bottles at once.</b></span>
                
                <h3 class="greenfont" style="text-decoration: underline;">This protects you from supply shortages and price increases.</h3>

			</div>
            <p>Remember: We’re working towards living in a world where NO ONE has to suffer with bladder urgency. And thanks to ActivGuard, more and more people get that relief each day.</p>
            
            <p>So as more and more people discover true relief from bladder troubles, the demand for ActivGuard and its ingredients will only rise.</p>

            <p>This will cause the price to increase.</p>
            
            <p><strong>ActivGuard has been backordered before, and it will be backordered again.</strong></p>
            
            <p>And given its proven effectiveness, you do NOT want to risk being without ActivGuard.</p>
            
            <p><strong>In fact, the only risk you take today is ignoring this potentially life-changing information.</strong></p>
			
            <h3 class="box-bg text-center greenfont">Saying “No” to ActivGuard is saying NO to the chance at real relief from urgency and incontinence.</h3><br/>
            
            <p>It’s agreeing to let your bladder run your life.</p>

			<p>It’s agreeing to being told by doctors there’s nothing you can do but deal with it, take prescription drugs that lower your libido, or have complex surgery.</p>
            
            <h3 class="box-bg text-center greenfont">WHY would you risk all of that, condemning yourself to suffer completely unnecessarily, when ActivGuard is both proven and 100% risk-free?</h3><br/>
            
            <p>The only thing that makes sense is to give yourself a great chance at life-changing relief with ActivGuard.</p>

			<p><strong>So click the button below to see if there is supply available.</strong></p>
            <div class="well well-shade">
            <center><img src="https://activguardnow.com/img/multibottle.png" alt="" class="img-responsive center-block no-border text-center"></center>
            
            <center><a style="background: none !important;border: none !important;box-shadow: none !important;" href="step1.php" onClick="grayOut();"><img style="margin-top: 45px;" src="img/green_btn.png"></a></center>
</div>
			<p>If so, go ahead and get your bottles through the secure, triple-encrypted checkout page.</p> 

            <p><strong><u>Your order will be on its way to you within 48 hours.</u></strong></p>
            
            <p>Get excited because your best chance at real relief is on its way to you. Relief that can mean a truly new lease on life.</p>
            
            <p>So click the button above with excitement and confidence, because you’re on the way to something incredible, with truly life changing potential.</p>
            
            <h3 class="box-bg text-center greenfont">Now is the time for you reclaim control of your bladder with ActivGuard.</h3><br/>
            
            <ul class="tick">
            	<li>Remember, ActivGuard has been proven to promote bladder health, shrink the prostate, and eliminate urgency and incontinence.</li>
                <li>And now you can get your very own supply affordably and with zero risk.</li>
                <li>Click the button below and complete checkout now.</li>
            </ul>
<div class="well well-shade">
			<center><img src="https://activguardnow.com/img/multibottle.png" alt="" class="img-responsive center-block no-border text-center"></center>
            
            <center><a style="background: none !important;border: none !important;box-shadow: none !important;" href="step1.php" onClick="grayOut();"><img style="margin-top: 45px;" src="img/green_btn.png"></a></center>

</div>
            
            <div class="row">
                <div class="col-sm-12">
					<div class="section8">
                        <div class="faq_gr_section">
                            <div class="row">
                                <div>                                    
                                    <div class="col-sm-12">
                                    	<div style="padding-bottom: 15px; text-align:center">
                                        	<h2 class="centerheading">Frequently Asked Questions</h2>
                                        </div>
                                                                                                                        
                                        <div class="panel-group faq_container" id="accordion" role="tablist" aria-multiselectable="true">
                                        	
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="qa_question boldtext">What is ActivGuard?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>ActivGuard is a unique, 100% natural supplement, whose ingredients have been shown to promote bladder health, shrink the male and female prostate, and eradicate both sudden and nagging feelings of urgency, and incontinence.</p>
 
															<p>It is not easy to source these ingredients in their purest and most potent forms, which is why they are so rare and difficult to find. But B Naturals spares no expense, doing everything to ensure that ActivGuard improves as many lives as possible.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="qa_question boldtext">Is there anything else in ActivGuard?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>Yes. EVERYTHING inside ActivGuard is designed to help you feel better. There’s Saw Palmetto, Beta-Sitosterol, Pygeum Africanum, Red Raspberry Juice Extract, Graviola, and much more.</p>
 
                                                            <p>These ingredients have been shown to support a healthy prostate, less frequent need to go to the restroom, and a greater peace of mind for people who have trouble with untimely urination.</p>
                                                             
                                                            <p>And we’re excited for you to be the next person who feels better thanks to ActivGuard.</p>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="qa_question boldtext">How do I take ActivGuard?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>Each bottle of ActivGuard contains 60 small, easy to swallow capsules. It’s recommended you take 2 capsules daily. Because ActivGuard is all natural with no side effects, you can take up to 4 capsules daily if you suffer from severe urgency or incontinence.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="qa_question boldtext">How fast does ActivGuard work?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>ActivGuard can begin to shrink the prostate and promote bladder health starting with the very first time you take it. This is because the pure and potent Zinc, Copper, Selenium, Saw Palmetto, Reishi Mushroom, and other ingredients inside ActivGuard will immediately do their work at reducing prostate size, cleaning the bladder, and more. That means you can feel relief from urgency and incontinence with you very first bottle of ActivGuard.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive" class="qa_question boldtext">How much does ActivGuard cost?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>The relief from urgency to use the bathroom and incontinence, the improved health, and the renewed lease on life you’ll have once your bladder problems are eradicated is truly priceless.</p>
 
                                                            <p>That’s why the original asking price of $149 is an incredible bargain – especially because of how expensive all traditional methods are… and ActivGuard’s ability to help you save so much money on all of that hassle…</p>
                                                             
                                                            <p>But what’s even more important than the money you can save is the relief you can feel. That’s why, even though it is expensive and time-consuming to harvest the pure ingredients inside ActivGuard, B Naturals uses its manufacturing experience to be able to keep the costs as low as possible for you, making 1 bottle just $69, and when you buy multiple bottles, it can be as low as $49 per bottle.</p>
                                                             
                                                            <p>This includes FREE shipping premium of your order, which is a $12.99 value.</p>
                                                            
                                                            <p>And when you get ActiveGuard today, you do so 100% risk-free.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix" class="qa_question boldtext">What are the terms of the guarantee again?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>You have a full 180 days, which is 6 months, which is half a year, to try out ActivGuard.</p>
 
                                                            <p>If you don’t love ActivGuard (or heck, even if you DO) all you have to do is call or e-mail our top-of-the-line customer support team and we will give you a complete refund no questions asked.</p>
                                                             
                                                            <p>And you do NOT have to ship back any product – you can keep it as our way of saying thanks for trying out ActivGuard.</p>
                                                             
                                                            <p>That means the only risk you face today is ignoring this information, and continuing to suffer needlessly.</p>
                                                             
                                                            <p>Do not condemn yourself to that, when it is totally unnecessary to do so. You owe it to yourself to get your best chance at relief that comes all naturally, that helps the root causes of bladder problems, and that can improve your life for years to come. That relief starts by clicking the button below, and getting ActivGuard right now.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="qa_question boldtext">Okay, how do I get my first shipment of ActivGuard?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSeven" class="collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                    <div class="panel-body">
                                                    	<div class="qa_answer">
                                                            <p>First, click the button below. Then, complete the secure checkout page. Feel good about this smart decision and the new possibilities of real, lasting relief that you can feel. Your order will be processed and shipped within 48 hours and sent directly to your door.</p>
 
                                                            <p>Relief starts with clicking the button below right now. We’re so excited for you to try ActivGuard.</p>
                                                            
                                                            <p>Click the button below right now.</p>
                                                            <br/>
                                                            <center><img src="https://activguardnow.com/img/multibottle.png" alt="" class="img-responsive center-block no-border text-center"></center>
                                                            
                                                            <a style="background: none !important;border: none !important;box-shadow: none !important;" href="step1.php" onClick="grayOut();"><img style="margin-top: 45px;" src="img/green_btn.png"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                             
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
           
           
            
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    <!----=========== end Second page content here ===================================--> 
    
    
    
    
    
    
    
	<footer id="footer">
		<div class="container">
			<ul class="list-inline">
            	<li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a></li>
				<li><a onClick="window.onbeforeunload = null;" href="step1.php?<?php echo $_SERVER['QUERY_STRING'];?>">Get ActivGuard Now</a></li>
				
            </ul>
            <div class="disclaimer">
				<p>The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
			</div><br />
			<p style="margin-top:30px; color:#337ab7;">Copyright &copy; ActivGuard 2017</p>
        </div>
	</footer>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.4.min.js"></script>
	<script>
		var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	function init() {
		if(isMobile.any()) {
			$('#playerid2').attr('controls','');
			setTimeout(showButton, 2712000);//2775000    
		}else 
			setInterval(function(){				  $('#overlay').show();			 }, 1000);
			setTimeout(showButton, 2712000);//2775000
		return;
	}
	</script>

<script>
$(document).ready(function(){
	//INITIALIZE
	var video = $('#playerid2');
	
	
	//remove default control when JS loaded
	video[0].removeAttribute("controls");
	/*$('.control').show().css({'bottom':-45});
	$('.loading').fadeIn(500);
	$('.caption').fadeIn(500);*/
 
	//before everything get started
	video.on('loadedmetadata', function() {
		$('.caption').animate({'top':-45},300);
			
		//set video properties
		$('.current').text(timeFormat(0));
		$('.duration').text(timeFormat(video[0].duration));
		updateVolume(0, 0.7);
			
		//start to get video buffering data 
		setTimeout(startBuffer, 150);
			
		//bind video events
		$('.videoContainer')
		.append('<div id="init"></div>')
		.hover(function() {
			/*$('.control').stop().animate({'bottom':0}, 500);
			$('.caption').stop().animate({'top':0}, 500);*/
		}, function() {
			if(!volumeDrag && !timeDrag){
				/*$('.control').stop().animate({'bottom':-45}, 500);
				$('.caption').stop().animate({'top':-45}, 500);*/
			}
		})
		.on('click', function() {
			$('#init').remove();
			$('.btnPlay').addClass('paused');
			$(this).unbind('click');
			video[0].play();
		});
	
	});
	
	
			$('#init').fadeIn(200);
			$('#init').remove();
			//$('.videoContainer').unbind('click');
			//video[0].play();
	
	//display video buffering bar
	var startBuffer = function() {
		var currentBuffer = video[0].buffered.end(0);
		var maxduration = video[0].duration;
		var perc = 100 * currentBuffer / maxduration;
		$('.bufferBar').css('width',perc+'%');
			
		if(currentBuffer < maxduration) {
			setTimeout(startBuffer, 500);
		}
	};	
	
	//display current video play time
	video.on('timeupdate', function() {
		var currentPos = video[0].currentTime;
		var maxduration = video[0].duration;
		var perc = 100 * currentPos / maxduration;
		$('.timeBar').css('width',perc+'%');	
		$('.current').text(timeFormat(currentPos));	
	});
	
	//CONTROLS EVENTS
	//video screen and play button clicked
	video.on('click', function() { playpause(); } );
	$('.btnPlay').on('click', function() { playpause(); } );
	var playpause = function() {
		if(video[0].paused || video[0].ended) {
			$('.btnPlay').addClass('paused');
			video[0].play();
		}
		else {
			$('.btnPlay').removeClass('paused');
			video[0].pause();
		}
	};
	
	//speed text clicked
	$('.btnx1').on('click', function() { fastfowrd(this, 1); });
	$('.btnx3').on('click', function() { fastfowrd(this, 3); });
	var fastfowrd = function(obj, spd) {
		$('.text').removeClass('selected');
		$(obj).addClass('selected');
		video[0].playbackRate = spd;
		video[0].play();
	};
	
	//stop button clicked
	$('.btnStop').on('click', function() {
		$('.btnPlay').removeClass('paused');
		updatebar($('.progress').offset().left);
		video[0].pause();
	});
	
	//fullscreen button clicked
	$('.btnFS').on('click', function() {
		if($.isFunction(video[0].webkitEnterFullscreen)) {
			video[0].webkitEnterFullscreen();
		}	
		else if ($.isFunction(video[0].mozRequestFullScreen)) {
			video[0].mozRequestFullScreen();
		}
		else {
			alert('Your browsers doesn\'t support fullscreen');
		}
	});
	
	//light bulb button clicked
	$('.btnLight').click(function() {
		$(this).toggleClass('lighton');
		
		//if lightoff, create an overlay
		if(!$(this).hasClass('lighton')) {
			$('body').append('<div class="overlay"></div>');
			$('.overlay').css({
				'position':'absolute',
				'width':100+'%',
				'height':$(document).height(),
				'background':'#000',
				'opacity':0.9,
				'top':0,
				'left':0,
				'z-index':999
			});
			$('.videoContainer').css({
				'z-index':1000
			});
		}
		//if lighton, remove overlay
		else {
			$('.overlay').remove();
		}
	});
	
	//sound button clicked
	$('.sound').click(function() {
		video[0].muted = !video[0].muted;
		$(this).toggleClass('muted');
		if(video[0].muted) {
			$('.volumeBar').css('width',0);
		}
		else{
			$('.volumeBar').css('width', video[0].volume*100+'%');
		}
	});
	
	//VIDEO EVENTS
	//video canplay event
	video.on('canplay', function() {
		$('.loading').fadeOut(100);
	});
	
	//video canplaythrough event
	//solve Chrome cache issue
	var completeloaded = false;
	video.on('canplaythrough', function() {
		completeloaded = true;
	});
	
	//video ended event
	video.on('ended', function() {
		$('.btnPlay').removeClass('paused');
		video[0].pause();
	});

	//video seeking event
	video.on('seeking', function() {
		//if video fully loaded, ignore loading screen
		if(!completeloaded) { 
			$('.loading').fadeIn(200);
		}	
	});
	
	//video seeked event
	video.on('seeked', function() { });
	
	//video waiting for more data event
	video.on('waiting', function() {
		$('.loading').fadeIn(200);
	});
	
	//VIDEO PROGRESS BAR
	//when video timebar clicked
	var timeDrag = false;	/* check for drag event */
	$('.progress').on('mousedown', function(e) {
		timeDrag = true;
		updatebar(e.pageX);
	});
	$(document).on('mouseup', function(e) {
		if(timeDrag) {
			timeDrag = false;
			updatebar(e.pageX);
		}
	});
	$(document).on('mousemove', function(e) {
		if(timeDrag) {
			updatebar(e.pageX);
		}
	});
	var updatebar = function(x) {
		var progress = $('.progress');
		
		//calculate drag position
		//and update video currenttime
		//as well as progress bar
		var maxduration = video[0].duration;
		var position = x - progress.offset().left;
		var percentage = 100 * position / progress.width();
		if(percentage > 100) {
			percentage = 100;
		}
		if(percentage < 0) {
			percentage = 0;
		}
		$('.timeBar').css('width',percentage+'%');	
		video[0].currentTime = maxduration * percentage / 100;
	};

	//VOLUME BAR
	//volume bar event
	var volumeDrag = false;
	$('.volume').on('mousedown', function(e) {
		volumeDrag = true;
		video[0].muted = false;
		$('.sound').removeClass('muted');
		updateVolume(e.pageX);
	});
	$(document).on('mouseup', function(e) {
		if(volumeDrag) {
			volumeDrag = false;
			updateVolume(e.pageX);
		}
	});
	$(document).on('mousemove', function(e) {
		if(volumeDrag) {
			updateVolume(e.pageX);
		}
	});
	var updateVolume = function(x, vol) {
		var volume = $('.volume');
		var percentage;
		//if only volume have specificed
		//then direct update volume
		if(vol) {
			percentage = vol * 100;
		}
		else {
			var position = x - volume.offset().left;
			percentage = 100 * position / volume.width();
		}
		
		if(percentage > 100) {
			percentage = 100;
		}
		if(percentage < 0) {
			percentage = 0;
		}
		
		//update volume bar and video volume
		$('.volumeBar').css('width',percentage+'%');	
		video[0].volume = percentage / 100;
		
		//change sound icon based on volume
		if(video[0].volume == 0){
			$('.sound').removeClass('sound2').addClass('muted');
		}
		else if(video[0].volume > 0.5){
			$('.sound').removeClass('muted').addClass('sound2');
		}
		else{
			$('.sound').removeClass('muted').removeClass('sound2');
		}
		
	};

	//Time format converter - 00:00
	var timeFormat = function(seconds){
		var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
		var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
		return m+":"+s;
	};
});

$(window).load(function() {
			//$('#init').fadeIn(200);
			//$('#init').remove();
			//$('.btnPlay').addClass('paused');*/
			//$('.videoContainer').trigger('click');
			//$('.btnPlay').trigger('click');
			//video[0].play();
			//$('.btnPlay').trigger('click');
			//$('.videoContainer').unbind('click');
			//video[0].play();
			setTimeout(function(){ 
				$('.btnPlay').trigger('click');
			}, 500); 
});
</script>

<script>
		var warning = true;
		var count_close=0;
    	window.onbeforeunload = function() {
			count_close++;
			// To remove the second pop up added if(count_close == 1) {.
			if(count_close == 1) {
				if (warning) {
					$('.vid-content').hide();
					$('.sec-content').show();
					$("html, body").animate({ scrollTop: 0 }, "slow");
					vimeoWrap = $('#vimeoWrap');
					vimeoWrap.html('');console.log(count_close);
					
					if (count_close==2){
						window.onbeforeunload = null;
						$('.thrd-content').show();
						$('.thrd-content-hide').hide();
						return "WAIT! WAIT!\n\n How would you like to get Survival Pharmacy for over 25% off?!\n\nTODAY ONLY. Stay on this page and get your discount now!";				
					}
					/*if (ytexists()) {
						$('#playerid')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*'); 
					} else {
						var video = document.getElementById("vsl");
						video.pause();
					}*/
					return "WAIT!\n\nRead my incredible story at your own pace…\n\nJust click “Stay on this page” now.";
				}
			}  
    	}
		function showButton() {
        	$('#videoButton').show();
    	}
		function ytexists() {
			for (var e = document.getElementsByTagName('iframe'), x = e.length; x--;)
				if (/player.vimeo.com\/video/.test(e[x].src)) return true;
			return false;
		}
		$(document).ready(function() {
			setTimeout(function(){ 
			$('.video_button').show();
			}, 1345000);            
        });
		/*var video = document.getElementById('playerid2');
		video.addEventListener('click',function(){
		  video.play();
		},false);*/
		
		//jQuery(function ($) {
		jQuery(document).ready(function() {
			var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
			$active.find('a').prepend('<span class="according_icon icon_minus"><img class="minus_img" src="img/minus.png" /><img src="img/plus.png" class="plus_img" /></span>');
			$('#accordion .panel-heading').not($active).find('a').prepend('<span class="according_icon icon_plus"><img class="minus_img" src="img/minus.png" /><img src="img/plus.png" class="plus_img" /></span>');
			$('#accordion').on('show.bs.collapse', function (e)
			{
				$('#accordion .panel-heading.active').removeClass('active').find('.according_icon').toggleClass('icon_plus icon_minus');
				////$(e.target).prev().addClass('active').find('.according_icon').toggleClass('icon_plus icon_minus');
				////alert("Here");
				$(e.target).prev().addClass('active').find('.according_icon').removeClass('icon_plus').addClass('icon_minus');
			});
			$('#accordion').on('hide.bs.collapse', function (e)
			{
				$(e.target).prev().removeClass('active').find('.according_icon').removeClass('icon_minus').addClass('icon_plus');
				////alert("Here1");
			});
		});
			
		//});

    </script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="js/salvattore.min.js"></script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WG3MPM3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>
<?php
@session_start();
error_reporting(0);
include('campaign_setup.php');
$validation_function='validate_checkout_form()';
if(!isset($_REQUEST['prospectId']) || empty($_REQUEST['prospectId'])){
	$validation_function='validate_one_form()';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ActivGuard</title>
	<meta name="robots" content="noindex">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <style>
		html{
			background-color: #f7f2eb;
		}
    	.container {
			max-width: 1180px;
			background-color: #fff;
		}
		
		#header {
		    padding-top: 15px;
			padding-bottom: 15px;
			/*border-bottom: 5px solid #385b1e;*/
			padding-right: 15px;
			padding-left: 15px;
			background-color:#f15d4e !important;
			border:none !important;
		}
		
		.flag_img {
			float: right;
			padding: 15px 0px;
		}
		
		.header_container {
			padding-right: 0px !important;
			padding-left: 0px !important;
		}
		
		body {
			    background-image: url(img/patern.png) !important;
				background-color: transparent;
				background-position: center top;
				background-repeat: no-repeat;
				
		}
		
		.top_menu_bar {
			padding: 10px 10px 20px;
		}
		
		.gray_box_with_border {
			background-color: #fff;
			border: 1px solid #e0dfdf;
			padding: 0px 10px;
			text-align: center;
			border-radius:50px;
		}
		.footer_section{
			margin-top: 90px !important;
		}
		.border-bottom{
			border-bottom: 1px solid #828282;
		}
		.padding_top_bottom_10{
			padding-bottom:10px;
			padding-top:10px;
		}
		.gray_box_without_border {
			background-color: #f6ebe1;
			padding: 0px;
			text-align: center;
			margin-right: 0px;
    		margin-left: 0px;
    		padding: 15px 0px;
		}
		
		.guarantee_box {
			background-color: #ffffff;
			border: 1px solid #e4dfd9;
			padding: 20px 20px 0px;
			border-radius: 5px;
			border-bottom-left-radius: 0px !important;
    		border-bottom-right-radius: 0px !important;
    		border-bottom: 3px solid #f15d4e !important;
		}
		
		.roboto_c_font {
			font-family: 'Roboto Condensed', sans-serif; 
		}
		
		.raleway_font {
			font-family: 'Raleway', sans-serif; 
		}
		
		.roboto_font {
			font-family: 'Roboto', sans-serif;
		}
		
		.opensans_font {
			font-family: 'Open Sans', sans-serif;
		}
		
		.txt_16 {
			font-size: 16px;
		}
		
		.txt_17 {
			font-size: 17px;
		}
					
		.txt_18 {
			font-size: 18px;
		}
		.txt_20 {
			font-size: 20px;
		}
		
		.txt_21 {
			font-size: 21px;
		}
		
		.txt_22 {
			font-size: 22px;
		}
		
		.txt_24 {
			font-size: 24px;
		}
		
		.txt_26 {
			font-size: 26px;
		}
		
		.txt_28 {
			font-size: 28px;
		}
				
		.txt_30 {
			font-size: 30px;
		}
		
		.txt_44 {
			font-size: 44px;
		}
		
		.txt_46 {
			font-size: 46px;
		}
		
		.font_light {
			font-weight: 300;
		}
		
		.font_regular {
			font-weight: 400;
		}
		
		.font_medium {
			font-weight: 500;
		}
				
		.font_semibold {
			font-weight: 600;
		}
		
		.font_bold {
			font-weight: 700;
		}
				
		.darkgray {
			color: #3b3636;
		}
		
		.redfont {
			color: #ef2216;
		}
		
		.redfont_2 {
			color: #ff0000;
		}
		
		.orangefont {
			color: #e44f00;
		}
		
		.orangefont_2 {
			color: #ed793c;
		}
		
		.darkgray_1 {
			color: #434343;
		}
		
		.darkgray_2 {
			color: #585858;
		}
		
		.darkgray_3 {
			color: #595959;
		}
		
		.darkgray_4 {
			color: #696969;
		}
		
		.darkgray_5 {
			color: #424141;
		}
		
		.darkgray_6 {
			color: #2f2f2f;
		}
		
		.darkgray_7 {
			color: #1e1c1c;
		}
		
		.darkgray_8 {
			color: #2a2a2a;
		}
		
		.light_gray {
			color: #838282;
		}
		
		.light_gray_2 {
			color: #f5f5f5;
		}
		
		.greenfont {
			color: #3b9001;
		}
		
		.blackfont {
			color: #000;
		}
		
		.greenfont_2 {
			color: #43a300;
		}
					
		.letterspace_7 {
			letter-spacing: 7px;
		}
		.ribbon_txt {
			position: absolute;
			margin-top: 29px;
			margin-left: 15px;
		}
		.padding_left_20 {
			padding-left: 20px;
		}
		.padding_top_10 {
			padding-top: 10px;
		}
		.padding_top_45 {
			padding-top: 45px;
		}
		
		.padding_top_55 {
			padding-top: 55px;
		}
		
		.padding_left_25 {
			padding-left: 25px;
		}
		
		.padding_top_bottom_10 {
			padding-top: 10px;
			padding-bottom: 10px;
		}
		
		.no_padding {
			padding: 0px !important;
		}
		
		.text_right {
			text-align: right;
		}
			
		.text_left {
			text-align: left;
		}
		
		.text_center {
			text-align: center;
		}
			
		.section1 {
			padding: 25px 0px;
		}
		
		.section7 {
			padding-bottom: 20px;
		}
		
		.col_sm_8 {
			width: 66.6667%;
		}
		
		.col_sm_4 {
			width: 33.3333%;
		}
		
		.shipping_free {
			padding-right: 20px;
		}
		
		.section3 {
			padding: 20px;
			
		}
		
		.section_heading {
			padding: 20px 0;
		}
		
		/*.section4 .panel .panel-heading {
			background-image: url('<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_img1.png');
			background-size: cover;
		}*/
		
		.panel {
			border: none;
		}
		
		.panel-heading {
			padding: 5px 15px !important;
		}
		.with_margin_round_corner{
			
   		 margin-top: 50px;
		 border-radius: 20px;
		 box-shadow: 0 0 10px #c3c3c3;
		-moz-box-shadow: inset 0 0 10px #c3c3c3;
		-webkit-box-shadow: 0 0 10px #c3c3c3;

		}
		/*.section4 .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_bg.png);
			background-size: contain;
			background-color: transparent;
			color: #fff;
			text-transform: uppercase;
			border: 1px solid #537c1d;
			border-bottom: none;
		}
		
		.section4 .panel-body {
		    background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/light_green_bg.png);
			border: 1px solid #537c1d;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			background-size: contain;
		}*/
		
		.panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/gray_bar_bg_1.png);
			background-size: contain;
			background-color: transparent;
			color: #000;
			text-transform: uppercase;
			background-repeat: repeat;
			border: 1px solid #c7c7c7;
		}
		
		.panel-body {
		    background-color: #fff;
			border: 1px solid #e4dfd9;
			margin-top: -2px;
			border-radius: 20px;
			border-bottom-left-radius: 0px !important;
			border-bottom-right-radius: 0px !important;
			border-bottom:3px solid #f15d4e !important;
		}
		.rush{
			margin-left: -38px;
		}
		
		.selected .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_bg.png);
			background-size: contain;
			background-color: transparent;
			color: #fff;
			text-transform: uppercase;
			border: 1px solid #537c1d;
			border-bottom: none;
		}
		
		/*.selected .panel-body {
		    background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/light_green_bg.png);
			border: 1px solid #e4dfd9;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			background-size: contain;
		}*/
		
		.linethrough {
			text-decoration:line-through;
		}
		
		/*.section5 .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/gray_bar_bg_1.png);
			background-size: contain;
			background-color: transparent;
			color: #000;
			text-transform: uppercase;
			background-repeat: repeat;
			border: 1px solid #c7c7c7;
		}
		
		.section5 .panel-body {
		    background-color: #f6f6f6;
			border: 1px solid #c7c7c7;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
		}
		
		.section6 .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/gray_bar_bg_1.png);
			background-size: contain;
			background-color: transparent;
			color: #000;
			text-transform: uppercase;
			background-repeat: repeat;
			border: 1px solid #c7c7c7;
		}
		
		.section6 .panel-body {
		    background-color: #f6f6f6;
			border: 1px solid #c7c7c7;
			border-top: none;
			margin-top: -2px;
			border-radius: 5px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
		}*/
		
		.section8 {
			margin-bottom: 25px;
		}
		
		.right_section_greenbox {
			margin-bottom: 40px;
			box-shadow: 0 0 5px #e1e1e1;
			-moz-box-shadow: 0 0 5px #e1e1e1;
			-webkit-box-shadow: 0 0 5px #e1e1e1;
			border-radius: 15px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}
		
		.right_section_greenbox_body {
			background-color: #ffffff;
			padding: 5px 20px;
			/*border: 1px solid #2d6728;*/
			border-radius: 3px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			border-top: none;
		}
		
		.right_section_header {
			text-align: center;
			/*background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/right_sidebar_heading_bg.png);
			background-size: contain;
			border-radius: 5px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;*/
		}
		
		select, input[type="text"] {
			padding: 7px;
    		width: 100%;
			border: 1px solid #e4dfd9;
			background-color: #fffcf9;
}

	
		
		.form_label {
		    padding-top: 15px;
    		padding-bottom: 10px;
		}
		
		.what_this_space {
			padding-top: 8px;
    		padding-bottom: 8px;
		}
		
		.footer_section {
			background-color: #3a1612;
			padding: 45px 45px 15px;
		}
		
		.footer_text_container {
		    width: 90%;
    		margin: 0 auto;
		}
		
		.footer_menu_container {
			text-align: center;
			padding: 30px 0px 10px;
		}
		
		.footer_menu_container .list-inline li a {
			font-size: 17px;
			color: #f15d4e;
			font-weight: 400;
			font-family: 'Raleway', sans-serif; 
		}
		
		.copywright_text {
			text-align: center;
		}
		
		.copywright_text span {
			color: #cecdcd;
			font-family: 'Raleway', sans-serif;
			font-weight: 300;
			font-size: 14px;
		}
		
		.form_submit_btn {
			max-width: 100%;
		}
		
		.hidethis {
			display: none;
		}
		
		input[name="product_selected"] {
			visibility: hidden;
		}
		.fullwidth{
			width:95%;
		}
		
		.checked {
			margin-top: -12px;
		}
		
		.notchecked {
			margin-top: -7px;
		}
		
		#header .container {
			margin: 0 auto;
    		background-color: transparent;
		}
		
		.footer_section {
			width: 100%;
    		margin: 0 auto;
		}
				
		/* Custom colored checkbox start */
		.regular-checkbox {
			display: none;
		}
		
		.regular-checkbox + label {
			-webkit-appearance: none;
			background-color: #ffffff;
			border: 1px solid #255521;
			/*padding: 9px;*/
			padding: 6px;
			border-radius: 1px;
			display: inline-block;
			position: relative;
			margin-right: 5px;
			margin-bottom: -2px;
		}
		
		.regular-checkbox:checked + label:after {
			content: ' ';
			width: 24px;
			height: 17px;
			border-radius: 1px;
			position: absolute;
			top: -5px;
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/black_checkbox.png);
			background-size: contain;
			text-shadow: 0px;
			left: 0px;
			font-size: 32px;
			background-repeat: no-repeat;
		}
		
		.regular-checkbox:checked + label {
			color: #99a1a7;
			border: 1px solid #255521;
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1), inset 0px 0px 10px rgba(0,0,0,0.1);
		}
		
		.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
		}
		input, select, textarea{
			    margin-bottom: 10px;
				border-radius: 4px;
		}
							
		/* Custom colored checkbox end */
		
		/* Popup css */
		.popupbottle {
			margin-left: -65px;
			margin-bottom: -43px;
			position: absolute;
			margin-top: 35px;
			height: 356px;
			width: 236px !important;
			max-width: 236px;
		}
		
		.glue_content {
			padding: 0px !important;
		}
		
		.popupheader {
			background-color: #fff !important;
			padding: 15px !important;
			border-radius: 5px !important;
			border-bottom-right-radius: 0px !important;
			border-bottom-left-radius: 0px !important;
		}
		
		.glue_popup {
			background: #e6f4d6 !important;
		}
		
		.glue_popup .row {
			margin-right: 0px !important;
			margin-left: 0px !important;
		}
		
		#beforeyougo {
			height: 483px !important;
    		width: 663px !important;
		}
		
		.glue_close {
			cursor: pointer;
			position: relative;
			top: -15px !important;
			left: 10px !important;
			float: right;
			font-family: Arial;
			font-size: 20px;
			background-color: #f15d4e !important;
			color: #fff !important;
			padding: 5px;
			padding-left: 12px !important;
			padding-right: 12px !important;
			text-decoration: none;
			z-index: 1;
			border-radius: 50%;
			font-weight: bold;
		}
		
		/* Popup css end */
		
		/* Select Package ul */
		.packageul {
			list-style: none;
			display: inline-block;
			margin-bottom: -5px;
		}
		
		.package_selected_text {
			display: inline-block;
		}
		
		.packageul li {
			float: left;
			background-color: rgb(56, 55, 55);
			color: #fff;
    		padding: 7px 15px;
    		margin-left: 10px;
    		border-radius: 3px;
		}
		
		.packageul li a {
			color: #fff;
			text-decoration: none;
			font-weight: 600;
    		font-family: 'Raleway', sans-serif;
		}
		
		.selected_package {
			background-color: rgb(241, 93, 78) !important;
		}
		
		/* Select Package ul end */
		
		/* Placeholder color */
		::-webkit-input-placeholder {
		   color: #aca298;
		}
		
		:-moz-placeholder { /* Firefox 18- */
		   color: #aca298;  
		}
		
		::-moz-placeholder {  /* Firefox 19+ */
		   color: #aca298;  
		}
		
		:-ms-input-placeholder {  
		   color: #aca298;  
		}		
		/* Placeholder color end */
		
		@media (min-width: 1200px) {
			.container {
				width: 1180px;
			}
		}
		
		@media (max-width: 768px) {
			.section6 .panel-body .col-sm-5 {
			    text-align: center;
			}
			
			.section6 .panel-body .col-sm-5 img {
			    padding-left: 0px !important;
			}
			
			.section4 .panel-body .col-sm-5 {
			    text-align: center;
				margin-top: 75px;
			}
			
			.section4 .panel-body .col-sm-3 {
			    text-align: center;
				position: absolute;
				top: -10px;
				right: -15px;
			}
			
			.section4 .row {
				position: relative;
			}
		}
    </style>
    
 <script src="js/jquery-3.1.1.min.js"></script>  
<script type="text/javascript" src="js/jqeury.js"></script>
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/script-mobile.js"></script>
  <link rel="stylesheet" href="css/magnific-popup.css" /> 
<script>
$(document).ready(function(){
  $("form input, select").blur(function(){
	validate_single(this.id);
  });
  modalOnClick();
});
</script>
<script type="text/javascript">
//window.onbeforeunload = grayOut;
function grayOut(){
var ldiv = document.getElementById('LoadingDiv');
ldiv.style.display='block';
}
</script>      
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89657482-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php
if($_REQUEST['pr_id'] == "5"){
	$selected_package = "package_1";
	$sel_package1 = "selected_package";
	$sel_package2 = "";
	$sel_package3 = "";
	$hide_this1 = "";
	$hide_this2 = "hidethis";
	$hide_this3 = "hidethis";
}
elseif($_REQUEST['pr_id'] == "4"){
	$selected_package = "package_2";
	$sel_package2 = "selected_package";
	$sel_package1 = "";
	$sel_package3 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "";
	$hide_this3 = "hidethis";
}else if($_REQUEST['pr_id'] == "3"){
	$selected_package = "package_3";
	$sel_package3 = "selected_package";
	$sel_package1 = "";
	$sel_package2 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "hidethis";
	$hide_this3 = "";
}else {
	$selected_package = "package_1";
	$sel_package1 = "selected_package";
	$sel_package2 = "";
	$sel_package3 = "";
	$hide_this1 = "";
	$hide_this2 = "hidethis";
	$hide_this3 = "hidethis";
}
/*}else{
	$selected_package = "package_3";
	$sel_package3 = "selected_package";
	$sel_package1 = "";
	$sel_package2 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "hidethis";
	$hide_this3 = "";
}*/
?>
 <div id="LoadingDiv" style="display:none;">One Moment Please...<br /><img src="img/progressbar.gif" class="displayed" alt="" /></div> 



       	
 <?php
	if($_REQUEST['errorMessage'])
	{
	 echo "<table style='width:1280px;' align='center'><tr><td style='background-color:#ff0000;color:#ffffff;font-size: 18px;font-family: arial,helvetica,sans-serif;font-weight:bold;height:50px;text-align: center;' align='center'>".urldecode($_REQUEST['errorMessage'])."</td></tr></table>";
	}
	?>
 
 
   <?php
     if($_REQUEST['pr_id'] == $custom_product6)
    {
	$product_name = "6 Month Supply";
        $pr_price = "294.00";
	$total = $pr_price+0.00;
    }
    elseif($_REQUEST['pr_id'] == $custom_product3)
    {
	$product_name = "3 Month Supply";
        $pr_price = "177.00";
	$total = $pr_price+0.00;
    }
    else{
	$product_name = "1 Month Supply";
        $pr_price = "69.00";
	$total = $pr_price+0.00;
    }
    ?>		    
	    
	    
	    
	    
	    
	    
	<header id="header">    	
        <div class="row container">
            <div style="padding-left: 0px;" class="col-sm-6">
                <a href="https://nutraherlean.com"><img src="img/logo.png" /></a>
            </div>
            <div class="col-sm-6">
                <img src="img/american_flag_with_text.png" class="flag_img" />
            </div>
        </div>        
    </header>
	<div class="container header_container">
        
    </div>
    <div style="background-color:#fffcf9;" class="container with_margin_round_corner">
    	<form action="includes/konn_new_order.php" method="post" onSubmit="return validate_one_form();" id="order_form">
            <div class="top_menu_bar">
                <img src="img/top_banner.png" />
            </div>
            <div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="gray_box_with_border">
                            <span class="roboto_c_font txt_28 font_regular darkgray" style="vertical-align: super;">Your order is reserved for:</span>
                            <span class="roboto_c_font txt_44 font_regular redfont letterspace_7 padding_left_20 count-up">10:00</span>
                        </div>
                        <div class="section1">
                            <div class="text_center">
                                <span class="orangefont txt_30 raleway_font font_bold">Great Job! </span>
                                <span class="txt_24 darkgray_1 font_regular opensans_font">You're taking your first step towards a healthier body. Act now so you don’t miss out on this offer!</span>
                            </div>
                        </div>
                        <div class="section2">
                            <div class="row gray_box_without_border">
                                <div class="col-sm-8">
                                    <span class="font_light txt_18 blackfont raleway_font">Current Availability:</span>
                                    <span><img src="<?php echo $_CONF['IMAGEPATH']; ?>/red_bar.png" /></span>
                                    <span class="redfont txt_18 font_semibold raleway_font">LOW</span>
                                </div>
                                <div class="col-sm-4 text_left">
                                    <span class="font_light txt_18 blackfont raleway_font">Sell out :</span>
                                    <span class="redfont txt_18 font_semibold raleway_font">Risk</span>
                                </div>
                            </div>
                        </div>
                        <div class="section3">
                            <div class="text_center">
                                <span class="greenfont txt_24 font_regular raleway_font shipping_free">Shipping is free!</span>
                                <span class="blackfont txt_22 font_medium opensans_font">Enjoy fast shipping.</span>
                            </div>
                        </div> 
                        
                        <div class="section_heading">                            	
                            <div class="txt_28 font_regular roboto_c_font darkgray package_selected_text">Your Selected Package</div>
                             <ul class="packageul">                                
                                <li class="package_2_li <?php echo $sel_package2; ?>"><a href="javascript:void(0)" onclick="package_select('package_2')">3 Bottles - $177</a></li>
                                <li class="package_1_li <?php echo $sel_package1; ?>"><a href="javascript:void(0)" onclick="package_select('package_1')">6 Bottles - $294</a></li>
                                <li class="package_3_li <?php echo $sel_package3; ?>"><a href="javascript:void(0)" onclick="package_select('package_3')">1 Bottle - $69</a></li>
                            </ul>
                        </div>
                                                
                        <div class="section6 package_1 package_section <?php echo $hide_this1; ?>">
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="img/orangecheckbox.png" class="checked hidethis" /><img src="img/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_1" class="section6" /> 6 bottles</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product2.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                    <span class="redfont txt_30 opensans_font font_bold">NutraHER</span>
                                                </div>
                                                
                                               
                                                <div>
                                                    <span class="txt_30 opensans_font font_regular blackfont">LEAN</span>
                                                </div>
                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">
                                                	<div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold">$49 / bottle</span>
                                                    </div>
                                                    <div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_4 linethrough">Was: USD $894</span>
                                                    </div>
                                                    <div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_5">Now: USD $294</span>
                                                    </div>
                                                    <div>
                                                        <span style="color:red;" class="txt_18 roboto_c_font font_regular">(Free Shipping)</span>
                                                    </div>
                                                	<?php /*?><table class="fullwidth border_top">
                                                    	<tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Price</span></td>
                                                            <td class="text_right"><span class="opensans_font font_regular blackfont txt_20">294.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_regular blackfont txt_20">Shipping & Handling:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">0.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Total:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">294.00</span></td>
                                                        </tr>
                                                        
                                                    </table><?php */?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="section7 package_2 package_section <?php echo $hide_this2; ?>">
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="<?php echo $_CONF['IMAGEPATH']; ?>/orangecheckbox.png" class="checked hidethis" /><img src="<?php echo $_CONF['IMAGEPATH']; ?>/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_2" class="section6" /> 3 bottle</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product1.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                    <span class="redfont txt_30 opensans_font font_bold">NutraHER</span>
                                                </div>
                                                
                                               
                                                <div>
                                                    <span class="txt_30 opensans_font font_regular blackfont">LEAN</span>
                                                </div>
                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">
                                                	<div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold">$59 / bottle</span>
                                                    </div>
                                                    <div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_4 linethrough">Was: USD $447</span>
                                                    </div>
                                                    <div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_5">Now: USD $177</span>
                                                    </div>
                                                    <div>
                                                        <span style="color:red;" class="txt_18 roboto_c_font font_regular">(Free Shipping)</span>
                                                    </div>
                                                	<?php /*?><table class="fullwidth border_top">
                                                    	<tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Price</span></td>
                                                            <td class="text_right"><span class="opensans_font font_regular blackfont txt_20">177.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_regular blackfont txt_20">Shipping & Handling:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">0.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Total:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">177.00</span></td>
                                                        </tr>
                                                    </table><?php */?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="section6 package_3 package_section <?php echo $hide_this3; ?>">
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="<?php echo $_CONF['IMAGEPATH']; ?>/orangecheckbox.png" class="checked hidethis" /><img src="<?php echo $_CONF['IMAGEPATH']; ?>/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_1" class="section6" /> 1 bottle</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product3.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                    <span class="redfont txt_30 opensans_font font_bold">NutraHER</span>
                                                </div>
                                                
                                                <div>
                                                    <span class="txt_30 opensans_font font_regular blackfont">LEAN</span>
                                                </div>
                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">
                                                	<?php /*?><table class="fullwidth border_top">
                                                    	<tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Price</span></td>
                                                            <td class="text_right"><span class="opensans_font font_regular blackfont txt_20">69.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_regular blackfont txt_20">Shipping & Handling:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">0.00</span></td>
                                                        </tr>
                                                        <tr class="border-bottom">
                                                            <td class="padding_top_bottom_10"><span class="opensans_font font_bold blackfont txt_20">Total:</span></td>
                                                            <td class="top_padding_10 text_right"><span class="opensans_font font_regular blackfont txt_18">69.00</span></td>
                                                        </tr>
                                                    </table><?php */?>
                                                    <div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold">$69 / bottle</span>
                                                    </div>
													<div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_4 linethrough">Was: USD $149</span>
                                                    </div>
                                                    <div>
                                                        <span class="txt_22 roboto_c_font font_regular darkgray_5">Now: USD $69</span>
                                                    </div>
                                                    <div>
                                                        <span style="color:red;" class="txt_18 roboto_c_font font_regular">(Free Shipping)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="section7 rush">
                            <div><img src="img/rush.png" /></div>
                        </div>
                        
                         <div class="section8">
                            <div class="guarantee_box">
                                <div class="row">
                                    <div class="col-sm-12 text_left" style="padding-bottom: 40px;">
                                        <span style="font-size:26px; color:#000; font-weight:700;" class="font_semibold opensans_font">INCLUDES A 60 DAY 100% MONEY BACK GUARANTEE</span>
                                    </div>
                                    <div>
                                        <div class="col-sm-3" style="margin-top: -30px;margin-bottom: 15px;">
                                            <img src="img/100money_back.png" />
                                        </div>
                                        <div class="col-sm-9" style="margin-top: -18px;margin-bottom: 15px; padding-left:0px;">
                                            <p class="darkgray_6 txt_16 font_regular opensans_font">NutraHer comes with a 60 Day, 100% Risk-Free Money Back Guarantee. That means if you change your mind about this decision at any point in the next two months – all you need to do is call us, and we’ll refund your purchase. Plus, you don’t need to return the bottle.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                         
                    </div>
                    <div class="col-sm-4">
                        <div class="right_section_greenbox">
                            <div class="right_section_header">
                                <img src="img/final_step.png" style="width: 100%;" />
                            </div>
                            <div class="right_section_greenbox_body">
                            
                                  <div class="form_fields">
                                    <div class="field_inner">
                                        <!--<div class="form_label">
                                            <span class="raleway_font font_medium txt_18 darkgray_7">First Name</span>
                                        </div>-->
                                        <div style="margin-top:20px;" class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="firstName" name="fields_fname" placeholder="First Name" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="lastName" name="fields_lname" placeholder="Last Name" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="address" name="fields_address1" placeholder="Address" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="City" name="fields_city" placeholder="City" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                             <select name="fields_state"  id="states" >
              <option value="">Select State</option>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="PR">Puerto Rico</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VI">Virgin Islands of the U.S.</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="postalCode" name="fields_zip" maxlength="5" value=""  onKeyDown="return onlyNumbers(event,'require')" placeholder="Zip Code" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="phone" name="fields_phone" maxlength="10"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Phone" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="email" name="fields_email" placeholder="Email" />
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                                
                                
                                <div class="text_center padding_top_bottom_10">
                                    <input  class="billing" type="checkbox" checked onclick = "togglebill();" id="billingsame" /><label for="billing_address_same"></label> <span class="raleway_font font_regular txt_16 darkgray_7">Billing Address same as shipping ?</span>
                                </div>
                                                            
                                <div class="form_fields">
                                
                                	<div class="billing" style="display:none;">
                                    
                                    	
					<!--div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" name="billing_fname" id="billing_fname"  placeholder="John" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" name="billing_lname" id="billing_lname"  placeholder="Smith" />
                                        </div>
                                    </div-->
					
					
					
					
                                        
                                        <div class="field_inner">
                                            
                                            <div class="raleway_font font_regular txt_16 light_gray">
                                                <input type="text" name="billing_street_address" id="billing_street_address" placeholder="Address" />
                                            </div>
                                        </div>
                                        
                                        <div class="field_inner">
                                            
                                            <div class="raleway_font font_regular txt_16 light_gray">
                                                <input type="text" name="billing_city" id="billing_city" placeholder="City" />
                                            </div>
                                        </div>
                                        
                                        <div class="field_inner">
                                            
                                            <div class="raleway_font font_regular txt_16 light_gray">
                                               <select name="billing_state" id="billing_state" >
                <option value="">Select State</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="PR">Puerto Rico</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VI">Virgin Islands of the U.S.</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </select>
                                            </div>
                                        </div>
                                        
                                        <div class="field_inner">
                                            
                                          <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" name="billing_postcode" id="billing_postcode"  maxlength="6" value=""  onKeyDown="return onlyNumbers(event,'require')" placeholder="Zip Code" />
                                        </div>
                                        </div>
                                                                                
                                    </div>
                                	<div style="padding-bottom:20px;" class="text_center">
                                    <img src="<?php echo $_CONF['IMAGEPATH']; ?>/creditcard_imgs.png" />
                                </div>
                                    <div class="field_inner">
                                        
                                          <div class="raleway_font font_regular txt_16 light_gray">
                                            <select    name="cc_type" id="cc_type" >
					    <option value="" selected>Card Type</option>                    
					    <option value = "visa" >Visa</option>
					    <option value = "master" >Master Card</option>
                                            <option value="discover">Discover</option>											
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="raleway_font font_regular txt_16 light_gray">
                                           <input type="text" name="cc_number" id="cardNumber" autocomplete="off" maxlength="16"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Card Number" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                   <select name="fields_expmonth" id="expirationMonth"   >
              <option value="" selected >Month</option>
              <option value='01' onclick=''  >(01) January</option>
              <option value='02' onclick=''  >(02) February</option>
              <option value='03' onclick=''  >(03) March</option>
              <option value='04' onclick=''  >(04) April</option>
              <option value='05' onclick=''  >(05) May</option>
              <option value='06' onclick=''  >(06) June</option>
              <option value='07' onclick=''  >(07) July</option>
              <option value='08' onclick=''  >(08) August</option>
              <option value='09' onclick=''  >(09) September</option>
              <option value='10' onclick=''  >(10) October</option>
              <option value='11' onclick=''  >(11) November</option>
              <option value='12' onclick=''  >(12) December</option>
            </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                   <select name="fields_expyear" id="expirationYear"   class="required n-year year" >
              <option value=""  selected >Year</option>
              <option value='16' onclick=''  >2016</option>
              <option value='17' onclick=''  >2017</option>
              <option value='18' onclick=''  >2018</option>
              <option value='19' onclick=''  >2019</option>
              <option value='20' onclick=''  >2020</option>
              <option value='21' onclick=''  >2021</option>
              <option value='22' onclick=''  >2022</option>
              <option value='23' onclick=''  >2023</option>
              <option value='24' onclick=''  >2024</option>
              <option value='25' onclick=''  >2025</option>
              <option value='26' onclick=''  >2026</option>
              <option value='27' onclick=''  >2027</option>
              <option value='28' onclick=''  >2028</option>
              <option value='29' onclick=''  >2029</option>
              <option value='30' onclick=''  >2030</option>
              <option value='31' onclick=''  >2031</option>
              <option value='32' onclick=''  >2032</option>
              <option value='33' onclick=''  >2033</option>
              <option value='34' onclick=''  >2034</option>
	      <option value='35' onclick=''  >2035</option>
              <option value='36' onclick=''  >2036</option>
            </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                     <input type="text"  id="securityCode" name="cc_cvv" autocomplete="off" maxlength="3"  value=""  onKeyDown="return onlyNumbers(event,'require')" placeholder="CVV Code" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="what_this_space">
                                                   <a target="_blank" href="<?php echo $_CONF['IMAGEPATH']; ?>/cvv2-location.png"><span class="font_medium raleway_font txt_16 orangefont_2">What's this?</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        <div class="form_label text_center">
                                            <img src="<?php echo $_CONF['IMAGEPATH']; ?>/secure_img_2.png" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        <div class="form_label text_center">
                                            <!--<img src="<?php echo $_CONF['IMAGEPATH']; ?>/checkoutbtn.png" />-->
                                            <input type="image" src="img/complete_checkout.png" border="0" alt="Submit" class="form_submit_btn" />
                                        </div>
                                    </div>
                                    
                                    <div class="field_inner">
                                        <div class="form_label text_center">
                                            <img src="img/visa.png" />
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
          <input type="hidden" id="custom_product" name="custom_product" value="<?php echo $_REQUEST['pr_id']; ?>"/>
                  <input type="hidden" id="custom_product_price" name="custom_product_price" value="<?php echo $pr_price;?>"/>
		  <input type="hidden" id="product_name" name="product_name" value="<?php echo $product_name;?>"/>
          <input type="hidden" name="shippingId" id="shippingId" value="<?php echo $shipping_id;?>" />
          <input type="hidden" name="campaign_id" id="campaign_id" value="<?php echo $campaign_id;?>" />
          <input type="hidden" name="billingSameAsShipping" id="billingSameAsShipping"  value="YES"/>
          <input type="hidden" id="billing_country" name="billing_country" value="US" />
          <input type="hidden" name="country" id="country" value="US" />
          <input type="hidden" id="submitted" name="submitted" value="1"/>
	  <input type="hidden" id="page" name="page" value="step2"/>
	  
	   <?php
		   foreach($_GET as $key => $value) {
			  echo "<input type='hidden' name='".safeRequestKonnective($key)."' value='".safeRequestKonnective($_GET[$key])."'>";
		   }
		  ?>
            
        </form>
        
    </div>
    
    <div class="footer_section row">
        <div class="footer_text_container text_center">
            <p class="raleway_font font_light txt_17 light_gray_2">The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
        </div>
        <div class="footer_menu_container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a></li>                    
            </ul>
        </div>
        <div class="copywright_text">
            <span>Copyright &copy; ActivGuard 2017</span>
        </div>
    </div>
    
     <!-- BEGIN Exitpop -->
    <!--div id="beforeyougo" style="display:none;" class="glue_popup">
        <div class="glue_close" onclick="jQuery.glue_close()">X</div>
        <div class="glue_content" onclick="jQuery.glue_close()">
           
            <div class="text_center popupheader">
            	<span class="txt_28 opensans_font font_semibold" style="color: #ff0000;">Wait!</span><span class="darkgray_8 txt_28 opensans_font font_regular"> We’re running low in stock and would hate for you to miss out on this risk free offer.</span>
            </div>
            <div class="row" style="border: 3px solid #000; border-top: none; border-bottom: none; background-color: #fff7eb;">
            	<div class="col-sm-8 text_center" style="padding-top: 20px;">
                	<div>
                        <span style="color: #764500;" class="txt_28 opensans_font font_regular">Click the button to try out</span><br />
                        <span class="txt_28 opensans_font font_semibold" style="color:#764500; text-decoration: underline;">NutraHER</span><span style="color: #764500;" class="txt_28 opensans_font font_regular"> right now!</span>
                    </div>
                    <div>
                        <span style="color: #191919;" class="txt_28 opensans_font font_semibold">Don't miss out on this great</span>
                    </div>
                    <div>
                    	<div style="padding: 20px;"><img src="img/risk_free_offer.png" /></div>
                        <div><span style="color: #191919;" class="txt_28 opensans_font font_regular">Includes a 180 days</span><br /><span class="txt_26 opensans_font font_semibold" style="color: #191919;">100% Money Back Guarantee!</span></div>
                    </div>                    
                </div>
                <div class="col-sm-4">
                	<img class="popupbottle" src="img/popupbottle1.png" start="position: absolute;" />
                </div>
            </div>
            <div class="row" style="margin-top: 0px;">
            	<div class="ribbon_txt"><span class="opensans_font txt_20 font_semibold" style="color: #fff;">Bottles of NutraHER Remaining : </span><span class="opensans_font txt_20 font_semibold" style="color: #ffd200;" id="random_count">222</span></div>
            	<div class="col-sm-12" style="padding-left:0px; padding-right: 0px; z-index: -1;">
                	<img src="img/ribbon_img.png" />
                </div>
            </div>
        </div>
    </div-->
    <!-- END Exitpop -->
    
	<script>
    jQuery(function() {
		// var counter = 599;
		var one        = 0;
		var ten        = 0;
		var hundered   = 6;
		var intervalId = setInterval(function(){
			time();
		}, .7);
    
    	function time(){
			one--;
		
			if(one == -1){
				ten = ten - 1;
				one = 0 + 9;
			} 
			if(ten == -1 ){
				hundered = hundered - 1;
				ten = 0 + 9;
			}
			var wholeNum = hundered+''+ten+''+one;
			if(wholeNum == 250){
				clearInterval(intervalId);
			}
		
			$('.timer').html('<span>'+hundered+'</span><span>'+ten+'</span><span>'+one+'</span>');
		
		}
		
		var min    = 0;
		var second = 00;
		var zeroPlaceholder = 0;
		
		var counterId = setInterval(function(){
			countDown();
		}, 1000);
		
		function countUp () {
			second++;
			if(second == 59){
				second = 00;
				min = min + 1;
			}
			if(second == 10){
				zeroPlaceholder = '';
			}else {
				if(second == 00){
					zeroPlaceholder = 0;
				}
		
				jQuery('.count-up').html(min+':'+zeroPlaceholder+second);
			} 
		}
		
		var min1    = 9;
		var second1 = 59;
		var zeroPlaceholder1 = 0;
		
		function countDown () {
			second1--;
			if(min1 > 0) {
				if(second1 == -1){
					second1 = 59;
					min1 = min1 - 1;
				}
				if(second1 >= 10){
					zeroPlaceholder1 = '';
				}else if(second1 < 10) {
					zeroPlaceholder1 = 0;
				}else {
					if(second1 == 00){
						zeroPlaceholder1 = 0;
					}			
				} 
				jQuery('.count-up').html(min1+':'+zeroPlaceholder1+second1);
			}else {
				jQuery('.count-up').html('0:00');
			}
		}   
    });
		
	jQuery('.product_checkbox input:radio').change(function(){
		jQuery('.product_checkbox').removeClass("selected");
		jQuery('.checked').addClass("hidethis");
		jQuery('.notchecked').removeClass("hidethis");
		var getclass = jQuery(this).attr("class");
		if(jQuery(this).is(":checked")) {
			jQuery('.'+getclass+' .product_checkbox').addClass("selected");
			jQuery('.'+getclass+' .checked').removeClass("hidethis");
			jQuery('.'+getclass+' .notchecked').addClass("hidethis");
		} else {
			jQuery('.'+getclass+' .product_checkbox').removeClass("selected");
			jQuery('.checked').addClass("hidethis");
			jQuery('.notchecked').removeClass("hidethis");
		}
	});
	
	jQuery('.product_checkbox').click(function(){
		jQuery('.product_checkbox').removeClass("selected");
		jQuery(this).addClass("selected");
		jQuery('.checked').addClass("hidethis");
		jQuery('.notchecked').removeClass("hidethis");
		jQuery('.product_checkbox input:radio').removeAttr("checked");
		
		jQuery(this).find("input:radio").attr('checked', true);
		
		var getclass = jQuery(this).find("input:radio").attr("class");
			
		if(jQuery(this).find("input:radio").is(":checked")) {
			jQuery('.'+getclass+' .product_checkbox').addClass("selected");
			jQuery('.'+getclass+' .checked').removeClass("hidethis");
			jQuery('.'+getclass+' .notchecked').addClass("hidethis");
		} else {
			jQuery('.'+getclass+' .product_checkbox').removeClass("selected");
			jQuery('.checked').addClass("hidethis");
			jQuery('.notchecked').removeClass("hidethis");
		}
		
	});
	
	jQuery('#billing_address_same').change(function(){
		if(!jQuery(this).is(":checked")) {
			jQuery(".differentshipping").removeClass("hidethis");
		}
		
		if(jQuery(this).is(":checked")) {
			jQuery(".differentshipping").addClass("hidethis");
		}
	});
    </script>
    
    <!-- BEGIN Exitpop -->
    <link rel="stylesheet" type="text/css" href="css/jquery.glue.css">
    <script src="js/jquery.glue.min.js"></script>
    <script>
    jQuery(document).ready(function(){

        jQuery.glue({
            layer: '#beforeyougo',
            maxamount: 1,
            cookie: true
        });
                    
    });
	
	function package_select(selectedpackage) {
		jQuery(".package_section").addClass("hidethis");
		jQuery("."+selectedpackage).removeClass("hidethis");
		
		jQuery(".packageul li").removeClass("selected_package");
		jQuery("."+selectedpackage+"_li").addClass("selected_package");
		if (selectedpackage == "package_1") {
			document.getElementById('custom_product').value = "5";
			document.getElementById('custom_product_price').value = "294";
			document.getElementById('product_name').value = "6 Month Supply";
		}else if (selectedpackage == "package_2") {
			document.getElementById('custom_product').value = "4";
			document.getElementById('custom_product_price').value = "177";
			document.getElementById('product_name').value = "3 Month Supply";
		}else{
			document.getElementById('custom_product').value = "3";
			document.getElementById('custom_product_price').value = "69";
			document.getElementById('product_name').value = "1 Month Supply";
		}
	}
    </script>
    <!-- END Exitpop -->
</body>
</html>

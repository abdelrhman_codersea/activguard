<?php
@session_start();
error_reporting(0);
include('campaign_setup.php');
$validation_function='validate_checkout_form()';
if(!isset($_REQUEST['prospectId']) || empty($_REQUEST['prospectId'])){
	$validation_function='validate_one_form()';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ActivGuard - Checkout</title>
	<meta name="robots" content="noindex">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="style.css" rel="stylesheet">

    <style>
		html{
			background-color: #f7f2eb;
		}
    	.container {
			max-width: 1180px;
			background-color: #fff;
		}

		#header {
		    padding-top: 15px;
			padding-bottom: 15px;
			/*border-bottom: 5px solid #385b1e;*/
			padding-right: 15px;
			padding-left: 15px;
			background-color:#fff !important;
			border:none !important;
		}

		.flag_img {
			float: right;
			padding: 15px 0px;
		}

		.header_container {
			padding-right: 0px !important;
			padding-left: 0px !important;
		}

		body {

				background-color: #fffaef;
				background-position: center top;
				background-repeat: no-repeat;

		}

		.top_menu_bar {
			padding: 20px 10px 20px;
		}

		.gray_box_with_border {
			background-color: #fff;
			border: 1px solid #e0dfdf;
			padding: 0px 10px;
			text-align: center;
			border-radius:50px;
		}
		.footer_section{
			margin-top: 90px !important;
		}
		.border-bottom{
			border-bottom: 1px solid #828282;
		}
		.padding_top_bottom_10{
			padding-bottom:10px;
			padding-top:10px;
		}
		.gray_box_without_border {
			background-color: #fff;
			padding: 0px;
			text-align: center;
			margin-right: 0px;
    		margin-left: 0px;
    		padding: 15px 0px;
			border:2px solid #e0d7c2;
		}

		.guarantee_box {
			background-color: #ffffff;
			border: 1px solid #e4dfd9;
			padding: 20px 20px 0px;
			border-radius: 5px;
			border-bottom-left-radius: 0px !important;
    		border-bottom-right-radius: 0px !important;
    		border-bottom: 4px solid #0a5693 !important;
		}

		.roboto_c_font {
			font-family: 'Roboto Condensed', sans-serif;
		}

		.raleway_font {
			font-family: 'Raleway', sans-serif;
		}

		.roboto_font {
			font-family: 'Roboto', sans-serif;
		}

		.opensans_font {
			font-family: 'Open Sans', sans-serif;
		}

		.txt_16 {
			font-size: 16px;
		}

		.txt_17 {
			font-size: 17px;
		}

		.txt_18 {
			font-size: 18px;
		}
		.txt_20 {
			font-size: 20px;
		}

		.txt_21 {
			font-size: 21px;
		}

		.txt_22 {
			font-size: 22px;
		}

		.txt_24 {
			font-size: 24px;
		}

		.txt_26 {
			font-size: 26px;
		}

		.txt_28 {
			font-size: 28px;
		}

		.txt_30 {
			font-size: 30px;
		}

		.txt_44 {
			font-size: 44px;
		}

		.txt_46 {
			font-size: 46px;
		}

		.font_light {
			font-weight: 300;
		}

		.font_regular {
			font-weight: 400;
		}

		.font_medium {
			font-weight: 500;
		}

		.font_semibold {
			font-weight: 600;
		}

		.font_bold {
			font-weight: 700;
		}

		.darkgray {
			color: #3b3636;
		}

		.redfont {
			color: #ef2216;
		}
		.redfont1{
			color: #006bc2;
		}

		.redfont_2 {
			color: #ff0000;
		}

		.orangefont1 {
			color: #006bc2;
		}
		.orangefont {
			color: #e44f00;
		}

		.orangefont_2 {
			color: #ed793c;
		}

		.darkgray_1 {
			color: #434343;
		}

		.darkgray_2 {
			color: #585858;
		}

		.darkgray_3 {
			color: #595959;
		}

		.darkgray_4 {
			color: #696969;
		}

		.darkgray_5 {
			color: #424141;
		}

		.darkgray_6 {
			color: #2f2f2f;
		}

		.darkgray_7 {
			color: #1e1c1c;
		}

		.darkgray_8 {
			color: #2a2a2a;
		}

		.light_gray {
			color: #838282;
		}

		.light_gray_2 {
			color: #f5f5f5;
		}

		.greenfont {
			color: #3b9001;
		}

		.blackfont {
			color: #000;
		}

		.greenfont_2 {
			color: #43a300;
		}

		.letterspace_7 {
			letter-spacing: 7px;
		}
		.ribbon_txt {
			position: absolute;
			margin-top: 29px;
			margin-left: 15px;
		}
		.padding_left_20 {
			padding-left: 20px;
		}
		.padding_top_10 {
			padding-top: 10px;
		}
		.padding_top_45 {
			padding-top: 45px;
		}

		.padding_top_55 {
			padding-top: 55px;
		}

		.padding_left_25 {
			padding-left: 25px;
		}

		.padding_top_bottom_10 {
			padding-top: 10px;
			padding-bottom: 10px;
		}

		.no_padding {
			padding: 0px !important;
		}

		.text_right {
			text-align: right;
		}

		.text_left {
			text-align: left;
		}

		.text_center {
			text-align: center;
		}

		.section1 {
			padding: 25px 0px;
		}

		.section7 {
			padding-bottom: 20px;
		}

		.col_sm_8 {
			width: 66.6667%;
		}

		.col_sm_4 {
			width: 33.3333%;
		}

		.shipping_free {
			padding-right: 20px;
		}

		.section3 {
			padding: 20px;

		}

		.section_heading {
			padding: 20px 0;
		}


		.panel {
			border: none;
		}

		.panel-heading {
			padding: 5px 15px !important;
		}
		.with_margin_round_corner{

   		 margin-top: 50px;
		 border-radius: 20px;
		 box-shadow: 0 0 10px #efe8d9;
		-moz-box-shadow: inset 0 0 10px #efe8d9;
		-webkit-box-shadow: 0 0 10px #efe8d9;

		}


		.panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/gray_bar_bg_1.png);
			background-size: contain;
			background-color: transparent;
			color: #000;
			text-transform: uppercase;
			background-repeat: repeat;
			border: 1px solid #c7c7c7;
		}

		.panel-body {
		    background-color: #fff;
			border: 1px solid #e4dfd9;
			margin-top: -2px;
			border-radius: 20px;
			border-bottom-left-radius: 0px !important;
			border-bottom-right-radius: 0px !important;
			border-bottom:4px solid #0a5693 !important;
		}
		.rush{
			margin-left: -32px;
		}

		.selected .panel .panel-heading {
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/green_bar_top_bg.png);
			background-size: contain;
			background-color: transparent;
			color: #fff;
			text-transform: uppercase;
			border: 1px solid #537c1d;
			border-bottom: none;
		}


		.linethrough {
			text-decoration:line-through;
		}

		.product_checkbox .panel{
			background-color:transparent !important;

		}

		.section8 {
			margin-bottom: 25px;
		}

		.right_section_greenbox {
			margin-bottom: 40px;
			box-shadow: 0 0 5px #e1e1e1;
			-moz-box-shadow: 0 0 5px #e1e1e1;
			-webkit-box-shadow: 0 0 5px #e1e1e1;
			border-radius: 15px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}

		.right_section_greenbox_body {
			background-color: #fffbf2;
			padding: 5px 20px;
			/*border: 1px solid #2d6728;*/
			border-radius: 3px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			border-top: none;
		}

		.right_section_header {
			text-align: center;
			/*background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/right_sidebar_heading_bg.png);
			background-size: contain;
			border-radius: 5px;
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;*/
		}

		select, input[type="text"] {
			padding: 7px;
    		width: 100%;
			border: 1px solid #e4dfd9;
			background-color: #fff;
}



		.form_label {
		    padding-top: 15px;
    		padding-bottom: 10px;
		}

		.what_this_space {
			padding-top: 8px;
    		padding-bottom: 8px;
		}

		.footer_section {
			background-color: #09304f;
			padding: 0px 45px 15px;
		}

		.footer_text_container {
		    width: 90%;
    		margin: 0 auto;
		}

		.footer_menu_container {
			text-align: center;
			padding: 30px 0px 10px;
		}

		.footer_menu_container .list-inline li a {
			font-size: 17px;
			color: #9ebcd3;
			font-weight: 400;
			font-family: 'Open Sans', sans-serif;
		}

		.copywright_text {
			text-align: center;
		}

		.copywright_text span {
			color: #cecdcd;
			font-family: 'Raleway', sans-serif;
			font-weight: 300;
			font-size: 14px;
		}

		.form_submit_btn {
			max-width: 100%;
		}

		.hidethis {
			display: none;
		}

		input[name="product_selected"] {
			visibility: hidden;
		}
		.fullwidth{
			width:95%;
		}

		.checked {
			margin-top: -12px;
		}

		.notchecked {
			margin-top: -7px;
		}

		#header .container {
			margin: 0 auto;
    		background-color: transparent;
		}

		.footer_section {
			width: 100%;
    		margin: 0 auto;
		}

		/* Custom colored checkbox start */
		.regular-checkbox {
			display: none;
		}

		.regular-checkbox + label {
			-webkit-appearance: none;
			background-color: #ffffff;
			border: 1px solid #255521;
			/*padding: 9px;*/
			padding: 6px;
			border-radius: 1px;
			display: inline-block;
			position: relative;
			margin-right: 5px;
			margin-bottom: -2px;
		}

		.regular-checkbox:checked + label:after {
			content: ' ';
			width: 24px;
			height: 17px;
			border-radius: 1px;
			position: absolute;
			top: -5px;
			background-image: url(<?php echo $_CONF['IMAGEPATH']; ?>/black_checkbox.png);
			background-size: contain;
			text-shadow: 0px;
			left: 0px;
			font-size: 32px;
			background-repeat: no-repeat;
		}

		.regular-checkbox:checked + label {
			color: #99a1a7;
			border: 1px solid #255521;
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1), inset 0px 0px 10px rgba(0,0,0,0.1);
		}

		.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
			box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
		}
		input, select, textarea{
			    margin-bottom: 10px;
				border-radius: 4px;
		}

		/* Custom colored checkbox end */

		/* Popup css */
		.popupbottle {
			margin-left: -65px;
			margin-bottom: -43px;
			position: absolute;
			margin-top: 35px;
			height: 356px;
			width: 236px !important;
			max-width: 236px;
		}

		.glue_content {
			padding: 0px !important;
		}

		.popupheader {
			background-color: #fff !important;
			padding: 15px !important;
			border-radius: 5px !important;
			border-bottom-right-radius: 0px !important;
			border-bottom-left-radius: 0px !important;
		}

		.glue_popup {
			background: #e6f4d6 !important;
		}

		.glue_popup .row {
			margin-right: 0px !important;
			margin-left: 0px !important;
		}

		#beforeyougo{
			height: 483px !important;
    		width: 663px !important;

		}
		#beforeyougo .risk_free {
			cursor:pointer !important;
		}

		.glue_close {
			cursor: pointer;
			position: relative;
			top: -15px !important;
			left: 10px !important;
			float: right;
			font-family: Arial;
			font-size: 20px;
			background-color: #f15d4e !important;
			color: #fff !important;
			padding: 5px;
			padding-left: 12px !important;
			padding-right: 12px !important;
			text-decoration: none;
			z-index: 1;
			border-radius: 50%;
			font-weight: bold;
		}

		/* Popup css end */

		/* Select Package ul */
		.packageul {
			list-style: none;
			display: inline-block;
			margin-bottom: -5px;
		}

		.package_selected_text {
			display: inline-block;
		}

		.packageul li {
			float: left;
			background-color: #55524a;
			color: #fff;
    		padding: 7px 15px;
    		margin-left: 10px;
    		border-radius: 3px;
		}

		.packageul li a {
			color: #fff;
			text-decoration: none;
			font-weight: 600;
    		font-family: 'Raleway', sans-serif;
		}

		.selected_package {
			background-color: #004176 !important;
		}

		/* Select Package ul end */

		/* Placeholder color */
		::-webkit-input-placeholder {
		   color: #aca298;
		}

		:-moz-placeholder { /* Firefox 18- */
		   color: #aca298;
		}

		::-moz-placeholder {  /* Firefox 19+ */
		   color: #aca298;
		}

		:-ms-input-placeholder {
		   color: #aca298;
		}
		/* Placeholder color end */

		.gurantee_img {
			width: 80%;
		}

		.hide_on_desktop {
			display: none;
		}

		.show_on_desktop {
			display: block;
		}
		.mobile_gur{
			display:none;
		}

		@media (min-width: 1200px) {
			.container {
				width: 1180px;
			}
		}

		@media (max-width: 768px) {
			.section6 .panel-body .col-sm-5 {
			    text-align: center;
			}

			.section6 .panel-body .col-sm-5 img {
			    padding-left: 0px !important;
			}

			.section4 .panel-body .col-sm-5 {
			    text-align: center;
				margin-top: 75px;
			}

			.section4 .panel-body .col-sm-3 {
			    text-align: center;
				position: absolute;
				top: -10px;
				right: -15px;
			}

			.section4 .row {
				position: relative;
			}

			.flag_img_col {
				text-align: center;
			}

			.gurantee_img {
				width: auto;
				margin-bottom: 30px;
			}

			.guarantee_box {
				text-align: center;
				padding: 20px 20px 0px 40px;
			}

			.gray_box_without_border, .gray_box_without_border .text_left {
				text-align: center !important;
			}

			.hide_on_desktop {
				display: block;
			}

			.show_on_desktop {
				display: none;
			}

			#beforeyougo {
				height: 100px !important;
				width: 100% !important;
				left: 50% !important;
    			transform: translate(-50%, 0) !important;
				margin-left: auto !important;
				margin-right: auto !important;
			}
			#beforeyougo {
				display:none;
		}

		}

		@media (max-width: 500px) {
			.packageul {
				width: 100%;
				padding-left: 0px;
				text-align:center;
			}

			.packageul li {
				width: 30.33%;
				padding: 7px 5px;
			}
			.package_selected_text {
				text-align:center !important;
				width:100%;
		}
		.guarantee_box {
				text-align: center;
				padding: 20px 20px 0px 40px;
			}
			#beforeyougo {
				display:none;
		}
		.destop_gur{
			display:none;
		}
		.mobile_gur{
			display:block;
			margin-bottom: 20px;
            margin-top: -30px;
		}
		.gurantee_img{
			margin-top:30px !important;
		}

		}
		@media (min-width: 600px) and (max-width: 1200px){
			.section_heading {
				text-align:center !important;
				margin:0 auto !important;
}
			.orangefont1{
				    font-size: 30px!important;
			}
			.mobile_bottle {
    font-size: 35px!important;
}
.what_mobile{
	font-size:13px !important;
}
.right_section_greenbox_body{
	padding: 5px 10px!important;
}
.mobile_year{
	padding-right: 10px;
    padding-left: 10px;
}
.mobile_year{
	padding-right: 10px!important;
    padding-left: 10px!important;
}
.packageul{
	text-align: center!important;
    margin: 0 auto!important;
    width: auto !important;
}
				}
    </style>

 <script src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jqeury.js"></script>
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/script-mobile.js"></script>
  <link rel="stylesheet" href="css/magnific-popup.css" />
<script>
$(document).ready(function(){
  $("form input, select").blur(function(){
	validate_single(this.id);
  });
  modalOnClick();
});
</script>
<script type="text/javascript">
//window.onbeforeunload = grayOut;
function grayOut(){
var ldiv = document.getElementById('LoadingDiv');
ldiv.style.display='block';
}
</script>

</head>

<body>
    <?php include_once("analyticstracking.php") ?>
<?php
if($_REQUEST['pr_id'] == "18"){
	$selected_package = "package_1";
	$sel_package1 = "selected_package";
	$sel_package2 = "";
	$sel_package3 = "";
	$hide_this1 = "";
	$hide_this2 = "hidethis";
	$hide_this3 = "hidethis";
}
elseif($_REQUEST['pr_id'] == "17"){
	$selected_package = "package_2";
	$sel_package2 = "selected_package";
	$sel_package1 = "";
	$sel_package3 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "";
	$hide_this3 = "hidethis";
}else if($_REQUEST['pr_id'] == "16"){
	$selected_package = "package_3";
	$sel_package3 = "selected_package";
	$sel_package1 = "";
	$sel_package2 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "hidethis";
	$hide_this3 = "";
}else {
	$selected_package = "package_1";
	$sel_package1 = "selected_package";
	$sel_package2 = "";
	$sel_package3 = "";
	$hide_this1 = "";
	$hide_this2 = "hidethis";
	$hide_this3 = "hidethis";
}
/*}else{
	$selected_package = "package_3";
	$sel_package3 = "selected_package";
	$sel_package1 = "";
	$sel_package2 = "";
	$hide_this1 = "hidethis";
	$hide_this2 = "hidethis";
	$hide_this3 = "";
}*/
?>
 <div id="LoadingDiv" style="display:none;">One Moment Please...<br /><img src="img/progressbar.gif" class="displayed" alt="" /></div>




 <?php
	if($_REQUEST['errorMessage'])
	{
	 echo "<table style='width:100%;' align='center'><tr><td style='background-color:#ff0000;color:#ffffff;font-size: 18px;font-family: arial,helvetica,sans-serif;font-weight:bold;height:50px;text-align: center;' align='center'>".urldecode($_REQUEST['errorMessage'])."</td></tr></table>";
	}
	?>


   <?php
     if($_REQUEST['pr_id'] == $custom_product6)
    {
	$product_name = "6 Month Supply";
        $pr_price = "294.00";
	$total = $pr_price+0.00;
    }
    elseif($_REQUEST['pr_id'] == $custom_product3)
    {
	$product_name = "3 Month Supply";
        $pr_price = "177.00";
	$total = $pr_price+0.00;
    }
    else{
	$product_name = "1 Month Supply";
        $pr_price = "69.00";
	$total = $pr_price+0.00;
    }
    ?>
	<header id="header">
        <div class="row container">
            <div style="padding-left: 0px;" class="col-sm-6">
                <a href="https://activguardnow.com"><img src="img/logo.png" /></a>
            </div>
            <div class="col-sm-6 flag_img_col">
                <img src="img/american_flag_with_text.png" class="flag_img" />
            </div>
        </div>
    </header>
	<div class="container header_container">

    </div>
    <div style="background-color:#f7f0e0;" class="container with_margin_round_corner">
    	<form action="kon/checkout.php?product_id=<?php echo $_REQUEST['product_id'] ?>" method="post" onSubmit="return validate_one_form();" id="order_form">
            <div class="top_menu_bar">
                <img src="img/top_banner.png" />
            </div>
            <div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="gray_box_with_border">
                            <span class="roboto_c_font txt_28 font_regular darkgray" style="vertical-align: super;">Your order is reserved for:</span>
                            <span class="roboto_c_font txt_44 font_regular redfont1 letterspace_7 padding_left_20 count-up">10:00</span>
                        </div>
                        <div class="section1">
                            <div class="text_center">
                                <span class="orangefont1 txt_30 raleway_font font_bold">Great Job! </span>
                                <span class="txt_24 darkgray_1 font_regular opensans_font">You're taking your first step towards a healthier body. Act now so you don’t miss out on this offer!</span>
                            </div>
                        </div>
                        <div class="section2">
                            <div class="row gray_box_without_border">
                                <div class="col-sm-8">
                                    <span class="font_light txt_18 blackfont raleway_font">Current Availability:</span>
                                    <span><img src="img/red_bar.png" /></span>
                                    <span class="redfont txt_18 font_semibold raleway_font">LOW</span>
                                </div>
                                <div class="col-sm-4 text_left">
                                    <span class="font_light txt_18 blackfont raleway_font">Sell out :</span>
                                    <span class="redfont txt_18 font_semibold raleway_font">Risk</span>
                                </div>
                            </div>
                        </div>
                        <div class="section3">
                            <div class="text_center">
                                <span class="orangefont1 txt_24 font_regular raleway_font shipping_free">Shipping is free!</span>
                                <span class="blackfont txt_22 font_medium opensans_font">Enjoy fast shipping.</span>
                            </div>
                        </div>

                        <div class="section_heading">
                            <div class="txt_28 font_regular roboto_c_font darkgray package_selected_text">Your Selected Package</div>
                             <ul class="packageul">
                                <li class="package_2_li <?php echo $sel_package2; ?>"><a href="javascript:void(0)" onclick="package_select('package_2')">3 Bottles - $177</a></li>
                                <li class="package_1_li  <?php echo $sel_package1; ?>"><a href="javascript:void(0)" onclick="package_select('package_1')">6 Bottles - $294</a></li>
                                <li class="package_3_li <?php echo $sel_package3; ?>"><a href="javascript:void(0)" onclick="package_select('package_3')">1 Bottle - $69</a></li>
                            </ul>
                        </div>

                        <div class="section6 package_1 package_section <?php echo $hide_this1; ?>">
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="img/orangecheckbox.png" class="checked hidethis" /><img src="img/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_1" class="section6" /> 6 bottles</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product2.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                    <span style="font-size:40px;" class="orangefont1 opensans_font font_bold">ActivGUARD</span>
                                                </div>

                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">
                                                	<div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold mobile_bottle">$49 / bottle</span>
                                                    </div>
                                                    <div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_4 linethrough">Was: USD $894</span>
                                                    </div>
                                                    <div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_5">Now: USD $294</span>
                                                    </div>
                                                    <div>
                                                        <span style="color:red; font-size:17px;" class="opensans_font font_bold">(Free Shipping)</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section7 package_2 package_section <?php echo $hide_this2; ?>">
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="/orangecheckbox.png" class="checked hidethis" /><img src="/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_2" class="section6" /> 3 bottle</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product1.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                   <span style="font-size:40px;" class="orangefont1 opensans_font font_bold">ActivGUARD</span>
                                                </div>



                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">
                                                	<div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold">$59 / bottle</span>
                                                    </div>
                                                    <div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_4 linethrough">Was: USD $447</span>
                                                    </div>
                                                    <div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_5">Now: USD $177</span>
                                                    </div>
                                                    <div>
                                                        <span style="color:red; font-size:17px;" class="opensans_font font_bold">(Free Shipping)</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section6 package_3 package_section <?php echo $hide_this3; ?>" >
                            <div class="product_checkbox">
                                <div class="panel panel-default">
                                    <div style="display:none;" class="panel-heading txt_24">
                                        <span class="font_bold opensans_font" style="border: none;"><img src="/orangecheckbox.png" class="checked hidethis" /><img src="/orangecheckbox_notselected.png" class="notchecked" /><input checked="checked" type="radio" name="product_selected" id="product_3" class="section6" /> 1 bottle</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6 text_center">
                                                <img src="img/sp_product3.png" class="" />
                                            </div>
                                            <div class="col-sm-6 text_center">
                                                <div>
                                                   <span style="font-size:40px;" class="orangefont1 opensans_font font_bold">ActivGUARD</span>
                                                </div>


                                                <div class="oreder_summary_price_table padding_top_10 fullwidth padding_bottom_55">

                                                    <div>
                                                        <span class="redfont txt_46 roboto_c_font font_bold">$69 / bottle</span>
                                                    </div>
													<div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_4 linethrough">Was: USD $149</span>
                                                    </div>
                                                    <div>
                                                        <span style="font-size:20px; font-weight:bold;" class="roboto_c_font darkgray_5">Now: USD $69</span>
                                                    </div>
                                                    <div>
                                                         <span style="color:red; font-size:17px;" class="opensans_font font_bold">(Free Shipping)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section7 rush">
                            <div><img src="img/rush.png" /></div>
                        </div>

                         <div class="section8">
                            <div class="guarantee_box">
                                <div class="row">
                                    <div class="col-sm-12 text_left destop_gur" style="padding-bottom: 40px;">
                                        <span style="font-size:26px; color:#000; font-weight:700;" class="font_semibold opensans_font">INCLUDES A 180 DAY 100% MONEY BACK GUARANTEE</span>
                                    </div>
                                    <div>
                                        <div class="col-sm-3" style="margin-top: -30px;margin-bottom: 15px;">
                                            <img src="img/gurantee_img.png" class="gurantee_img" />
                                        </div>

                                        <div class="col-sm-9" style="margin-top: -18px;margin-bottom: 15px; padding-left:0px;">
                                        <span style="font-size:26px; color:#000; font-weight:700;" class="font_semibold opensans_font mobile_gur">INCLUDES A 180 DAY 100% MONEY BACK GUARANTEE</span>
                                            <p style="text-align:left;" class="darkgray_6 txt_16 font_regular opensans_font">ActivGUARD comes with a 180 Day, 100% Risk-Free Money Back Guarantee. That means if you change your mind about this decision at any point in the next six months – all you need to do is call us, and we’ll refund your purchase. Plus, you don’t need to return the bottle.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="right_section_greenbox">
                            <div class="right_section_header">
                                <img src="img/final_step.png" style="width: 100%;" />
                            </div>
                            <div class="right_section_greenbox_body">

                                  <div class="form_fields">
                                    <div class="field_inner">
                                        <!--<div class="form_label">
                                            <span class="raleway_font font_medium txt_18 darkgray_7">First Name</span>
                                        </div>-->
                                        <div style="margin-top:20px;" class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="firstName" name="fields_fname" placeholder="First Name" value="<?php echo $_SESSION['fname']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="lastName" name="fields_lname" placeholder="Last Name" value="<?php echo $_SESSION['lname']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="address" name="fields_address1" placeholder="Address" value="<?php echo $_SESSION['address']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="City" name="fields_city" placeholder="City" value="<?php echo $_SESSION['city']; ?>"/>
                                        </div>
                                    </div>

				    <?php
		      $statesArray = array();
		      $statesArray["AL"] = "Alabama (AL)";
$statesArray["AL"] = "Alabama";
              $statesArray["AK"] = "Alaska";
              $statesArray["AZ"] = "Arizona";
              $statesArray["AR"] = "Arkansas";
              $statesArray["CA"] = "California";
              $statesArray["CO"] = "Colorado";
              $statesArray["CT"] = "Connecticut";
              $statesArray["DE"] = "Delaware";
              $statesArray["DC"] = "District of Columbia";
              $statesArray["FL"] = "Florida";
              $statesArray["GA"] = "Georgia";
              $statesArray["HI"] = "Hawaii";
              $statesArray["ID"] = "Idaho";
              $statesArray["IL"] = "Illinois";
              $statesArray["IN"] = "Indiana";
              $statesArray["IA"] = "Iowa";
              $statesArray["KS"] = "Kansas";
              $statesArray["KY"] = "Kentucky";
              $statesArray["LA"] = "Louisiana";
              $statesArray["ME"] = "Maine";
              $statesArray["MD"] = "Maryland";
              $statesArray["MA"] = "Massachusetts";
              $statesArray["MI"] = "Michigan";
              $statesArray["MN"] = "Minnesota";
              $statesArray["MS"] = "Mississippi";
              $statesArray["MO"] = "Missouri";
              $statesArray["MT"] = "Montana";
              $statesArray["NE"] = "Nebraska";
              $statesArray["NV"] = "Nevada";
              $statesArray["NH"] = "New Hampshire";
              $statesArray["NJ"] = "New Jersey";
              $statesArray["NM"] = "New Mexico";
              $statesArray["NY"] = "New York";
              $statesArray["NC"] = "North Carolina";
              $statesArray["ND"] = "North Dakota";
              $statesArray["OH"] = "Ohio";
              $statesArray["OK"] = "Oklahoma";
              $statesArray["OR"] = "Oregon";
              $statesArray["PA"] = "Pennsylvania";
              $statesArray["PR"] = "Puerto Rico";
              $statesArray["RI"] = "Rhode Island";
              $statesArray["SC"] = "South Carolina";
              $statesArray["SD"] = "South Dakota";
              $statesArray["TN"] = "Tennessee";
              $statesArray["TX"] = "Texas";
              $statesArray["UT"] = "Utah";
              $statesArray["VT"] = "Vermont";
              $statesArray["VI"] = "Virgin Islands of the U.S.";
              $statesArray["VA"] = "Virginia";
              $statesArray["WA"] = "Washington";
              $statesArray["WV"] = "West Virginia";
              $statesArray["WI"] = "Wisconsin";
              $statesArray["WY"] = "Wyoming";
?>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                             <select name="fields_state"  id="states">
              <option value="">Select State</option>
              <?php
   foreach($statesArray as $key=>$value)
	{
		if($key == $_SESSION['state'])
		{
			echo "<option value='$key' selected>$value</option>";
		}
		else{
			echo "<option value='$key'>$value</option>";
		}
	}
	?>
            </select>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="postalCode" name="fields_zip" maxlength="5" value="<?php echo $_SESSION['zip']; ?>"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Zip Code" />
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="phone" name="fields_phone" maxlength="10"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Phone" value="<?php echo $_SESSION['phone']; ?>" />
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" id="email" name="fields_email" placeholder="Email" value="<?php echo $_SESSION['email']; ?>"/>
                                        </div>
                                    </div>

                                </div>

                                 <?php
                                 if(!isset($_SESSION['billingSameAsShipping']))
                             	$_SESSION['billingSameAsShipping']='YES';
                    if($_SESSION['billingSameAsShipping'] == "YES")
				{
					$_SESSION['visibility'] = "style='display:none;'";
				}
				else{
					$_SESSION['visibility'] = "style='display:block;'";
				}
				?>
                    <?php
                    if($_SESSION['billingSameAsShipping'] == "YES")
				{
					$_SESSION['visibility'] = "style='display:none;'";
				?>


                                <div class="text_center padding_top_bottom_10">
                                    <input  class="billing" type="checkbox" checked onclick = "togglebill();" id="billingsame" />
				    <label for="billing_address_same"></label>
				    <span style=" color:#1e1c1c;" class="raleway_font font_regular txt_16">Billing Address same as shipping ?</span>
                                </div>
				   <?php
				}
				else{
					$_SESSION['visibility'] = "style='display:block;'";
				?>


				 <div class="text_center padding_top_bottom_10">
                                    <input  class="billing" type="checkbox"  onclick = "togglebill();" id="billingsame" />
				    <label for="billing_address_same"></label>
				    <span style=" color:#1e1c1c;" class="raleway_font font_regular txt_16">Billing Address same as shipping ?</span>
                                </div>

				<?php
				}
				?>

                                <div class="form_fields">

                                	<div class="billing" <?php echo $_SESSION['visibility']; ?>>



                                        <div class="field_inner">

                                            <div class="raleway_font font_regular txt_16 light_gray">
                                                <input type="text" name="billing_street_address" id="billing_street_address" value="<?php echo $_SESSION['billing_street_address']; ?>" placeholder="Address" />
                                            </div>
                                        </div>

                                        <div class="field_inner">

                                            <div class="raleway_font font_regular txt_16 light_gray">
                                                <input type="text" name="billing_city" id="billing_city" placeholder="City" value="<?php echo $_SESSION['billing_city']; ?>"/>
                                            </div>
                                        </div>

                                        <div class="field_inner">

                                            <div class="raleway_font font_regular txt_16 light_gray">
                                               <select name="billing_state" id="billing_state" >
                <option value="">Select State</option>
                <?php
   foreach($statesArray as $key=>$value)
	{
		if($key == $_SESSION['billing_state'])
		{
			echo "<option value='$key' selected>$value</option>";
		}
		else{
			echo "<option value='$key'>$value</option>";
		}
	}
	?>
              </select>
                                            </div>
                                        </div>

                                        <div class="field_inner">

                                          <div class="raleway_font font_regular txt_16 light_gray">
                                            <input type="text" name="billing_postcode" id="billing_postcode"  maxlength="6" value="<?php echo $_SESSION['billing_postcode']; ?>"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Zip Code" />
                                        </div>
                                        </div>

                                    </div>
                                	<div style="padding-bottom:20px;" class="text_center">
                                    <img src="img/creditcard_imgs.png" />
                                </div>
                                    <div class="field_inner">

                                          <div class="raleway_font font_regular txt_16 light_gray">
                                            <select    name="cc_type" id="cc_type" >
					    <option value="" selected>Card Type</option>
					    <option value = "visa" >Visa</option>
					    <option value = "master" >Master Card</option>
                                            <option value="discover">Discover</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="raleway_font font_regular txt_16 light_gray">
                                           <input type="text" name="cc_number" id="cardNumber" autocomplete="off" maxlength="16"  onKeyDown="return onlyNumbers(event,'require')" placeholder="Card Number" />
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="row">
                                            <div class="col-sm-6 mobile_month">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                   <select name="fields_expmonth" id="expirationMonth"   >
              <option value="" selected >Month</option>
              <option value='01' onclick=''  >(01) January</option>
              <option value='02' onclick=''  >(02) February</option>
              <option value='03' onclick=''  >(03) March</option>
              <option value='04' onclick=''  >(04) April</option>
              <option value='05' onclick=''  >(05) May</option>
              <option value='06' onclick=''  >(06) June</option>
              <option value='07' onclick=''  >(07) July</option>
              <option value='08' onclick=''  >(08) August</option>
              <option value='09' onclick=''  >(09) September</option>
              <option value='10' onclick=''  >(10) October</option>
              <option value='11' onclick=''  >(11) November</option>
              <option value='12' onclick=''  >(12) December</option>
            </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mobile_year">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                   <select name="fields_expyear" id="expirationYear"   class="required n-year year" >
              <option value=""  selected >Year</option>
              <option value='17' onclick=''  >2017</option>
              <option value='18' onclick=''  >2018</option>
              <option value='19' onclick=''  >2019</option>
              <option value='20' onclick=''  >2020</option>
              <option value='21' onclick=''  >2021</option>
              <option value='22' onclick=''  >2022</option>
              <option value='23' onclick=''  >2023</option>
              <option value='24' onclick=''  >2024</option>
              <option value='25' onclick=''  >2025</option>
              <option value='26' onclick=''  >2026</option>
              <option value='27' onclick=''  >2027</option>
              <option value='28' onclick=''  >2028</option>
              <option value='29' onclick=''  >2029</option>
              <option value='30' onclick=''  >2030</option>
              <option value='31' onclick=''  >2031</option>
              <option value='32' onclick=''  >2032</option>
              <option value='33' onclick=''  >2033</option>
              <option value='34' onclick=''  >2034</option>
	      <option value='35' onclick=''  >2035</option>
              <option value='36' onclick=''  >2036</option>
            </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field_inner">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="raleway_font font_regular txt_16 light_gray">
                                                     <input type="text"  id="securityCode" name="cc_cvv" autocomplete="off" maxlength="3"  value=""  onKeyDown="return onlyNumbers(event,'require')" placeholder="CVV Code" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="what_this_space">
                                                   <a target="_blank" href="img/cvv2-location.png"><span style="color:#f15d4e;" class="font_medium txt_16 opensans_font what_mobile"><i>What's this?</i></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field_inner">
                                        <div class="form_label text_center">
                                            <img src="img/secure_img_2.png" />
                                        </div>
                                    </div>

                                    <div class="field_inner">
                                        <div class="form_label text_center">

                                            <input type="image" src="img/complete_checkout.png" border="0" alt="Submit" class="form_submit_btn" />
                                        </div>
                                    </div>

                                    <div class="field_inner">
                                        <div class="form_label text_center">
                                            <img src="img/visa.png" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <input type="hidden" id="custom_product" name="custom_product" value="<?php echo $_REQUEST['pr_id']; ?>"/>
                  <input type="hidden" id="custom_product_price" name="custom_product_price" value="<?php echo $pr_price;?>"/>
		  <input type="hidden" id="product_name" name="product_name" value="<?php echo $product_name;?>"/>
          <input type="hidden" name="shippingId" id="shippingId" value="<?php echo $shipping_id;?>" />
          <input type="hidden" name="campaign_id" id="campaign_id" value="<?php echo $campaign_id;?>" />
          <input type="hidden" name="billingSameAsShipping" id="billingSameAsShipping"  value="YES"/>
          <input type="hidden" id="billing_country" name="billing_country" value="US" />
          <input type="hidden" name="country" id="country" value="US" />
          <input type="hidden" id="submitted" name="submitted" value="1"/>
	  <input type="hidden" id="page" name="page" value="step2"/>

	   <?php
		   foreach($_GET as $key => $value) {
			  echo "<input type='hidden' name='".safeRequestKonnective($key)."' value='".safeRequestKonnective($_GET[$key])."'>";
		   }
		  ?>

        </form>

    </div>

    <div class="footer_section row">
    <div class="footer_menu_container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a></li>
                <li style="display:none;"><a onClick="window.onbeforeunload = null;" href="#" target="_blank">Get ActivGUARD Now</a></li>
            </ul>
        </div>
        <div class="footer_text_container text_center">
            <p class="raleway_font font_light txt_17 light_gray_2">The statements on this website and on these product labels have not been evaluated by the food and drug administration. These products are intended as a dietary supplement only. Products are not intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body type, compliance, and other factors. All products are intended for use by adults over the age of 18. Consult a physician before taking any of our products, especially if you are pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
        </div>

        <div class="copywright_text">
            <span>Copyright &copy; ActivGuard 2017</span>
        </div>
    </div>

     <!-- BEGIN Exitpop -->
    <!--div id="beforeyougo" style="display:none;" class="glue_popup">
        <div class="glue_close" onclick="jQuery.glue_close()">X</div>
        <div class="glue_content" onclick="jQuery.glue_close()">
        	<div class="hide_on_desktop">
            	<img src="img/popup.png" />
            </div>
        	<div class="show_on_desktop">
                <div class="text_center popupheader">
                    <span class="txt_28 opensans_font font_semibold" style="color: #ff0000;">Wait!</span><span class="darkgray_8 txt_28 opensans_font font_regular"> We're running low in stock and would hate for you to miss out on this risk-free offer.</span>
                </div>
                <div class="row" style="border: 3px solid #000; border-top: none; border-bottom: none; background-color: #f3e9d2;">
                    <div class="col-sm-8 text_center" style="padding-top: 20px;">
                        <div>
                            <span style="color: #764500;" class="txt_28 opensans_font font_regular">Click the button to try out</span><br />
                            <span class="txt_28 opensans_font font_semibold" style="color:#764500; text-decoration: underline;">ActivGUARD</span><span style="color: #764500;" class="txt_28 opensans_font font_regular"> right now!</span>
                        </div>
                        <div>
                            <span style="color: #191919;" class="txt_28 opensans_font font_semibold">Don't miss out on this great</span>
                        </div>
                        <div>
                            <div class="risk_free" style="padding: 7px;"><img src="img/risk_free_offer.png" /></div>
                            <div><span style="color: #191919;" class="txt_28 opensans_font font_regular">Includes a 180 days</span><br /><span class="txt_26 opensans_font font_semibold" style="color: #191919;">100% Money Back Guarantee!</span></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <img class="popupbottle" src="img/popupbottle1.png" start="position: absolute;" />
                    </div>
                </div>
                <div class="row" style="margin-top: 0px;">
                    <div class="ribbon_txt"><span class="opensans_font txt_20 font_semibold" style="color: #fff;">Bottles of ActivGUARD Remaining : </span><span class="opensans_font txt_20 font_semibold" style="color: #ffd200;" id="random_count">25</span></div>
                    <div class="col-sm-12" style="padding-left:0px; padding-right: 0px; z-index: -1;">
                        <img src="img/ribbon_img.png" />
                    </div>
                </div>
            </div>
        </div-->
    </div><!-- END Exitpop -->

	<script>
    jQuery(function() {

    });



	/*jQuery('.product_checkbox').click(function(){
		jQuery('.product_checkbox').removeClass("selected");
		jQuery(this).addClass("selected");
		jQuery('.checked').addClass("hidethis");
		jQuery('.notchecked').removeClass("hidethis");
		jQuery('.product_checkbox input:radio').removeAttr("checked");

		jQuery(this).find("input:radio").attr('checked', true);

		var getclass = jQuery(this).find("input:radio").attr("class");

		if(jQuery(this).find("input:radio").is(":checked")) {
			jQuery('.'+getclass+' .product_checkbox').addClass("selected");
			jQuery('.'+getclass+' .checked').removeClass("hidethis");
			jQuery('.'+getclass+' .notchecked').addClass("hidethis");
		} else {
			jQuery('.'+getclass+' .product_checkbox').removeClass("selected");
			jQuery('.checked').addClass("hidethis");
			jQuery('.notchecked').removeClass("hidethis");
		}

	});*/



	jQuery(document).ready(function() {
	});
    </script>

    <!-- BEGIN Exitpop -->
    <link rel="stylesheet" type="text/css" href="https://greenreliefcbd.com/css/jquery.glue.css">
    <script src="https://greenreliefcbd.com/js/jquery.glue.min.js"></script>
    <script>
    jQuery(document).ready(function(){



    });

	function package_select(selectedpackage) {
		jQuery(".package_section").addClass("hidethis");
		jQuery("."+selectedpackage).removeClass("hidethis");

		jQuery(".packageul li").removeClass("selected_package");
		jQuery("."+selectedpackage+"_li").addClass("selected_package");
		if (selectedpackage == "package_1") {
			document.getElementById('custom_product').value = "18";
			document.getElementById('custom_product_price').value = "294";
			document.getElementById('product_name').value = "6 Month Supply";
		}else if (selectedpackage == "package_2") {
			document.getElementById('custom_product').value = "17";
			document.getElementById('custom_product_price').value = "177";
			document.getElementById('product_name').value = "3 Month Supply";
		}else{
			document.getElementById('custom_product').value = "16";
			document.getElementById('custom_product_price').value = "69";
			document.getElementById('product_name').value = "1 Month Supply";
		}
	}
    </script>
    <!-- END Exitpop -->
</body>
</html>

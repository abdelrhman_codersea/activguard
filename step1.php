<?php @session_start();
error_reporting(0);
include('./kon/KonConfig.php'); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="Expires" content="30"/>
    <meta name="robots" content="noindex,nofollow"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>ActivGuard - Package Select</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/sass/main.min.css">
</head>
<body class="beige-color">
<header>
    <div class="container">
        <div class="col-xs-12">
            <div class="col-lg-4 col-xs-12">
                <a href="./">
                    <img src="./img/logo.png" id="logo">
                </a>
            </div>
            <div class="col-lg-4 col-xs-12 pull-right" id="left-in-stock">
                <i class="fa fa-clock-o"></i><span> Bottles of ActivGUARD left in stock: </span><span class="text-red">28</span>
            </div>
        </div>
    </div>
</header>
<div id="step1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="choose-package">
                    <h1>CHOOSE YOUR DISCOUNTED PACKAGE</h1>
                    <img src="./img/divider_selectpackage_main.png" alt="Divider Select Package">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="packages col-xs-12 visible-lg visible-md">
                <div class="package-1">
                    <div class="package-header">
                        <h1><strong>3 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>3 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product1.png" alt="SP Product 3"></div>
                    <div class="package-footer">
                        <h1><strong>$59 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=17"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
                <div class="package-2">
                    <img src="./img/bestseller_tag.png" alt="Bestseller Tag" class="bestseller-tag">
                    <div class="package-header">
                        <h1><strong>6 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>6 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product2.png" alt="SP Product 6"></div>
                    <div class="package-footer">
                        <h1><strong>$49 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=18"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
                <div class="package-3">
                    <div class="package-header">
                        <h1><strong>1 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>1 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product3.png" alt="SP Product 1"></div>
                    <div class="package-footer">
                        <h1><strong>$69 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=16"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
            </div>
            <div class="packages-2 col-xs-10 col-xs-offset-1 hidden-lg hidden-md">
                <div class="package-1">
                    <div class="package-header">
                        <h1><strong>3 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>3 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product1.png" alt="SP Product 3"></div>
                    <div class="package-footer">
                        <h1><strong>$59 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=17"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
                <div class="package-2">
                    <img src="./img/bestseller_tag.png" alt="Bestseller Tag" class="bestseller-tag">
                    <div class="package-header">
                        <h1><strong>6 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>6 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product2.png" alt="SP Product 6"></div>
                    <div class="package-footer">
                        <h1><strong>$49 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=18"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
                <div class="package-3">
                    <div class="package-header">
                        <h1><strong>1 Month Supply</strong></h1>
                        <h2 class="secondary-color"><strong>1 Bottle</strong></h2>
                        <img src="./img/divider_small.png" alt="Divider Small">
                    </div>
                    <div class="product"><img src="./img/sp_product3.png" alt="SP Product 1"></div>
                    <div class="package-footer">
                        <h1><strong>$69 / bottle</strong></h1>
                        <a href="./kon/checkout.php?product_id=16"><img src="./img/addtocartbtn.png" alt="Add to Cart"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="template-1 guarantee text-left">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 text-center"><img src="./img/gurantee_img.png" alt="Guarantee Img"></div>
                        <div class="col-lg-10 col-sm-8">
                            <h2><strong><span class="secondary-color2">INCLUDES A 180 DAYS</span> 100% MONEY BACK
                                    GUARANTEE</strong></h2>
                            <p>
                                ActivGUARD comes with a 180 Day, 100% Money Back Guarantee. That means if you change
                                your mind about this decision at any point in the next six months – all you need to do
                                is email us, and we’ll refund your purchase. Plus, you don’t need to return the bottle.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="template-1 story-behind text-left">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2><strong><span class="secondary-color2">What’s the Story Behind</span>
                                    ActivGUARD?</strong></h2>
                            <p>
                                ActivGuard is founded by a team of doctors and scientists who are dedicated to
                                eradicating
                                bladder problems for all. The reality is that 99% of bladder issues, including urgency
                                and
                                incontinence, are suffered with completely unnecessarily. And a few proven, all-natural
                                nutrients that promote bladder and prostate health can be all it takes to get
                                life-changing
                                relief.
                            </p>
                            <p>
                                That’s why the family behind ActivGuard has helped change thousands of lives thanks to
                                better
                                internal health. And we invite you to be the next one at absolutely no risk at all.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="template-1 faq text-left">
                    <h2 id="faq-title">Frequently Asked Questions</h2>
                    <div class="panel-group" id="faq-container">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-1">
                                    <div class="panel-title">
                                        What is ActivGuard?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        ActivGuard is a unique, 100% natural supplement, whose ingredients have been
                                        shown to promote bladder health, shrink the male and female prostate, and
                                        eradicate both sudden and nagging feelings of urgency, and incontinence.
                                    </p>
                                    <p>
                                        It is not easy to source these ingredients in their purest and most potent
                                        forms, which is why they are so rare and difficult to find. But B Naturals
                                        spares no expense, doing everything to ensure that ActivGuard improves as many
                                        lives as possible.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-2">
                                    <div class="panel-title">
                                        Is there anything else in ActivGuard?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Yes. EVERYTHING inside ActivGuard is designed to help you feel better. There’s
                                        Saw Palmetto, Beta-Sitosterol, Pygeum Africanum, Red Raspberry Juice Extract,
                                        Graviola, and much more.
                                    </p>
                                    <p>
                                        These ingredients have been shown to support a healthy prostate, less frequent
                                        need to go to the restroom, and a greater peace of mind for people who have
                                        trouble with untimely urination.
                                    </p>
                                    <p>
                                        And we’re excited for you to be the next person who feels better thanks to
                                        ActivGuard.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-3">
                                    <div class="panel-title">
                                        How do I take ActivGuard?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Each bottle of ActivGuard contains 60 small, easy to swallow capsules. It’s
                                        recommended you take 2 capsules daily. Because ActivGuard is all natural with no
                                        side effects, you can take up to 4 capsules daily if you suffer from severe
                                        urgency or incontinence.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-4">
                                    <div class="panel-title">
                                        How fast does ActivGuard work?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        ActivGuard can begin to shrink the prostate and promote bladder health starting
                                        with the very first time you take it. This is because the pure and potent Zinc,
                                        Copper, Selenium, Saw Palmetto, Reishi Mushroom, and other ingredients inside
                                        ActivGuard will immediately do their work at reducing prostate size, cleaning
                                        the bladder, and more. That means you can feel relief from urgency and
                                        incontinence with you very first bottle of ActivGuard.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-5">
                                    <div class="panel-title">
                                        How much does ActivGuard cost?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        The relief from urgency to use the bathroom and incontinence, the improved
                                        health, and the renewed lease on life you’ll have once your bladder problems are
                                        eradicated is truly priceless.
                                    </p>
                                    <p>
                                        That’s why the original asking price of $149 is an incredible bargain –
                                        especially because of how expensive all traditional methods are… and
                                        ActivGuard’s ability to help you save so much money on all of that hassle…
                                    </p>
                                    <p>
                                        But what’s even more important than the money you can save is the relief you can
                                        feel. That’s why, even though it is expensive and time-consuming to harvest the
                                        pure ingredients inside ActivGuard, B Naturals uses its manufacturing experience
                                        to be able to keep the costs as low as possible for you, making 1 bottle just
                                        $69, and when you buy multiple bottles, it can be as low as $49 per bottle.
                                    </p>
                                    <p>
                                        This includes FREE shipping premium of your order, which is a $12.99 value.
                                    </p>
                                    <p>
                                        And when you get ActiveGuard today, you do so 100% risk-free.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#faq-container" href="#faq-6">
                                    <div class="panel-title">
                                        Okay, how do I get my first shipment of ActivGuard?
                                    </div>
                                </a>
                            </div>
                            <div id="faq-6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        First, click the button below. Then, complete the secure checkout page. Feel
                                        good about this smart decision and the new possibilities of real, lasting relief
                                        that you can feel. Your order will be processed and shipped within 48 hours and
                                        sent directly to your door.
                                    </p>
                                    <p>
                                        Relief starts with clicking the button below right now. We’re so excited for you
                                        to try ActivGuard.
                                    </p>
                                    <p>
                                        Click the button below right now.
                                    </p>
                                    <div class="text-center">
                                        <a href="./kon/checkout.php?product_id=18">
                                            <img src="./img/multibottle.png" alt="Multi Bottles">
                                            <img src="./img/ActivGuard/green_btn.png" alt="Get ActivGuard Now">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="inline-list">
                    <li><a href="./faq.html" target="_blank">FAQ</a></li>
                    <li><a href="./disclaimer.html" target="_blank">Disclaimer</a></li>
                    <li><a href="./anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                    <li><a href="./privacy.html" target="_blank">Privacy</a></li>
                    <li><a href="./terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                    <li><a href="./refunds.html" target="_blank">Refunds</a></li>
                    <li><a href="./contact-us.html" target="_blank">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-xs-10 col-xs-offset-1">
                <p class="text-light">
                    The statements on this website and on these product labels have not been evaluated by the food and
                    drug
                    administration. These products are intended as a dietary supplement only. Products are not intended
                    to
                    diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body
                    type, compliance, and other factors. All products are intended for use by adults over the age of 18.
                    Consult a physician before taking any of our products, especially if you are pregnant, nursing,
                    taking
                    medication, diabetic, or have any medical condition.
                </p>
            </div>
            <div class="col-xs-12">
                <p>Copyright © ActivGuard 2017</p>
            </div>
        </div>
    </div>
</footer>
<script src="./js/jquery.min.js"></script>
<script async src="./js/bootstrap.min.js"></script>
<script async src="./js/pages/step1.min.js"></script>
<?php if (count($_SESSION['purchased_items']) > 0) {
    foreach ($_SESSION['purchased_items'] as $v) {
        if (!in_array($v['product_id'], array(16, 17, 18)))
            continue;
        $pixel = $productArray[$v['product_id']]['pixel'];

        $returnArray = json_decode($v['return'], true);
        $orderId = $returnArray['message']['orderId'];
        if (is_array($pixel)) {
            foreach ($pixel as $pixel1) {
                ?>
                <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel1 ?>&t=<?php echo $orderId; ?>" height="1"
                        width="1" frameborder="0"></iframe>
                <?php
            }
        } else {
            ?>
            <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel ?>&t=<?php echo $orderId; ?>" height="1"
                    width="1" frameborder="0"></iframe>
            <?php
        }
    }
} ?>
</body>
</html>
<?php
@session_start();
error_reporting(0);
include('./kon/KonConfig.php');
$response = '';
$click_id = $_GET['click_id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex,nofollow"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ActivGuard - Stock Up</title>
    <meta name="robots" content="noindex">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="./style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:100,300,400,500,700,900"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <style>
        #teaser h2, strong {
            color: #588a17 !important;
            font-size: 30px !important;
        }

        .well-shade {
            background-color: #eef7ff;
            border: 1px solid #8ec0ed;
            text-align: center;
            margin-bottom: 30px;
        }

        ul.tick {
            padding-left: 60px;
            text-indent: -17px;
            list-style: none;
            list-style-position: outside;
        }

        ul.tick li:before {
            content: '✔';
            margin-left: -1em;
            margin-right: 1.100em;
        }

        li {
            font-family: 'Arimo', sans-serif;
            line-height: 40px !important;
            font-size: 18px !important;
        }

        .action-btn {
            width: 300px;
            text-align: center;
            border: none;
            padding: 12px 0;
            display: block;
            height: 60px;
            border-radius: 30px;
            background: #ffe900;
            background: -webkit-linear-gradient(#ffe900 1%, #ffa90d 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #ffe900), to(#ffa90d));
            background: linear-gradient(#ffe900 1%, #ffa90d 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#358f0d', endColorstr='#ffb700', GradientType=0);
            color: #000;
            font-family: 'Roboto Slab', serif;
            font-size: 30px;
            font-weight: 500;
            text-shadow: 0px 1px 2px #000;
            line-height: 1;
            /*-webkit-box-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);*/
        }

        .well {
            min-height: 20px;
            padding: 20px 100px;
            margin-bottom: 20px;
            /*background-color: #eef7ff;*/
            background-color: #c1e4ff;
            border: 1px solid #8ec0ed;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
        }

        #teaser {
            background-image: none !important;
        }

        .content .well-shade h2 {
            font-size: 36px;
            font-weight: 700;
            color: #6f3b03;
        }

        .box {
            padding: 30px 25px !important;
        }

        .well-shade .intermediate {
            /*color: #588a17!important;
            display: block;
            font-size: 26px!important;
            margin: 3px 0;*/
            color: #004881 !important;
            display: block;
            font-size: 26px !important;
            margin: 3px 0;
            font-family: 'Bree Serif', serif;
        }

        {
            color: #6f3b03
        ;
            display: block
        ;
            font-size: 26px
        ;
            margin: 3px 0
        ;
        }
        h3.red {
            background-color: rgb(255, 47, 44);
            padding: 10px 0px;
            color: #fff;
            text-align: center;
            font-weight: 700;
            font-size: 30px;
        }

        .well-shade .intro {
            display: block;
            font-family: 'Roboto', sans-serif;
            font-size: 20px;
            font-weight: 300;
            margin-bottom: 10px;
        }

        .raleway_font {
            font-family: 'Raleway', sans-serif;
        }

        .roboto_font {
            font-family: 'Roboto', sans-serif;
        }

        .opensans_font {
            font-family: 'Open Sans', sans-serif;
        }

        .greenfont {
            color: #1f8541;
        }

        span.red2 strong {
            color: #004881 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
        }

        .bold_text {
            font-weight: bold;
        }

        p {
            font-size: 16px;
            color: #000;
            margin: 0 50px 20px;
        }

        .box-bg {
            background-color: #004881;
            border: 1px solid #004881;
            font-weight: 400;
            padding: 15px 0;
            color: #fff !important;
        }

        .text-center {
            text-align: center;
        }

        .font_700 {
            font-weight: 700 !important;
        }

        span.red2 {
            font-size: 27px;
            display: block;
        }

        .well-shade .glyphicon-check {
            font-size: 30px;
            color: #3b9001;
        }

        .glyphicon {
            position: relative;
            top: 1px;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: 400;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        strong {
            /*color: #f33 !important;
            font-weight: 700;*/
            color: #004881 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
            font-size: 28px !important;
        }

        p a {
            color: #4472c4;
            text-decoration: underline;
        }

        .guarantee img {
            float: left;
            display: inline-block;
            margin-right: 10px;
            position: relative;
        }

        .guarantee p {
            font-size: 14px !important;
            color: #000 !important;
        }

        .no-thanks {
            color: #4472c4;
            text-decoration: underline;
            font-size: 12px;
        }

        .hd1 {
            margin: 0 0 10px;
            font-size: 19px;
            color: #ffffff;
            background: #004881;
            border-radius: 25px;
            padding: 17px 40px;
            display: inline-block;
        }

        .hd1 {
            font-size: 30px !important;
        }

        .orange_text {
            color: #f26522 !important;
            font-weight: 700;
            font-family: 'Bree Serif', serif;
        }

        .green_color {
            color: #3b9001;
        }

        @media (max-width: 560px) {
            .well {
                padding: 20px 20px !important;
            }

            p {
                margin: 0 10px 20px;
            }
        }

        @media (max-width: 768px) {
            button, html input[type=button], input[type=reset], input[type=submit] {
                max-width: 100%;
            }
        }

        body {
            background-color: #fffaef;
        }

    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<script src="/assets/js/Framework.js"></script>-->
    <script>
        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        function init() {
            if (isMobile.any())
                setTimeout(showButton, 2712000);//2775000
            else
                setInterval(function () {
                    $('#overlay').show();
                }, 1000);
            setTimeout(showButton, 2712000);//2775000
            return;
        }
    </script>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WG3MPM3');</script>
    <!-- End Google Tag Manager -->

    <script type="text/javascript">
        var mysrc = "https://tracking.softwareprojects.com/track/?a=4861&firstcookie=0&referrer=" + encodeURIComponent(document.referrer) + "&product=GRX6_UPS&sessid2=" + ReadCookie('sessid2');

        var newScript = document.createElement('script');
        newScript.type = 'text/javascript';
        newScript.async = true;
        newScript.src = mysrc;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(newScript, s);
        function ReadCookie(name) {
            name += '=';
            var parts = document.cookie.split(/;\s*/);
            for (var i = 0; i < parts.length; i++) {
                var part = parts[i];
                if (part.indexOf(name) == 0) return part.substring(name.length)
            }
            return '';
        }
    </script>


    <script type="text/javascript" src="js/jqeury.js"></script>
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript">
        //window.onbeforeunload = grayOut;
        function grayOut() {
            var ldiv = document.getElementById('LoadingDiv');
            ldiv.style.display = 'block';
        }
    </script>


    <!--<meta name="google-site-verification" content="88FZm9_l20oieVLskbZSQdEb3Unoqv8HlSc8qI94ym0" />
    <iframe src="https://grtrkng.com/p.ashx?o=3&e=2&t=TRANSACTION_ID" height="1" width="1" frameborder="0"></iframe>-->
</head>
<body>
<?php include_once("analyticstracking.php") ?>
<div style="display: none;">

</div>
<div id="LoadingDiv" style="display:none;">One Moment Please...<br/><img src="img/progressbar.gif" class="displayed"
                                                                         alt=""/></div>
<form method="POST" action="includes/upsell_process.php" name="upsell1" id="upselll">


    <?php
    if ($_REQUEST['errorMessage']) {
        echo "<table width='100%'><tr><td style='background-color:#ff0000;color:#ffffff;font-size: 18px;font-family: arial,helvetica,sans-serif;font-weight:bold;height:50px;text-align: center;'>" . urldecode($_REQUEST['errorMessage']) . "</td></tr></table>";
    }
    ?>

    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a href="https://activguardnow.com" onClick="window.onbeforeunload = null;"><img src="img/logo.png"
                                                                                                     class="img-responsive logo"></a>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div>
    </header>
    <section style="background-size:cover !important; background-repeat:no-repeat;" id="teaser">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box shade text-left">
                        <div style="text-align: center;">
                            <h3 class="hd1">Wait...Your order is not complete!</h3>
                        </div>
                        <div class="well well-shade">
                            <span class="intro">Do not hit the "Back" button as it can cause multiple charges to your card.</span>
                            <h3 class="greenfont">A Special Deal for New Customers ONLY!</h3>
                            <img src="./img/multibottle.png" alt="" class="img-responsive center-block no-border">
                            <span class="intro">Stock up on <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span> right now — for <b><u>Just $39 Per Bottle!</u></b></span>
                            <span class="intermediate">This is the absolute lowest price you’ll ever see for <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span></span>
                            <span class="intermediate">It protects you from future price increases & ensures <span
                                        style="color:#0c7e29;">100% in-stock availability…</span></span>
                            <span class="red2"><strong>Savings: $660.00 Off Retail.</strong></span>
                            <span class="intermediate">Keep reading for all the details…</span>
                        </div>
                        <div>

                            <h3 class="box-bg text-center greenfont">ATTENTION: This is the only time you will be able
                                to view this special offer.</h3>
                            <p class="roboto_font">First of all - let me just say <span
                                        style=" font-weight:bold; text-decoration:underline;">congratulations on your decision today!</span>
                            </p>
                            <p class="roboto_font">By choosing to get <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>,
                                you are finally <span style=" font-weight:bold; text-decoration:underline;">taking full control</span>
                                of your incontinence issues, your prostate health and your aging.</p>
                            <p class="roboto_font">And I have no doubt: </p>
                            <p class="roboto_font">The rapid benefits you're about to experience will be nothing short
                                of miraculous.</p>
                            <p style="font-weight:bold;" class="roboto_font"><u>You’re about to access your Order
                                    Confirmation page in just a second...</u></p>
                            <p class="roboto_font">But before you do...</p>
                            <h3 class="box-bg text-center greenfont">I have something very important to tell you -
                                something that's NOT for the general public....</h3>
                            <p class="roboto_font">As you know...</p>
                            <p class="roboto_font">Not only does each bottle of <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                contain <span style="font-weight:bold; text-decoration:underline;">NINETEEN</span>
                                scientifically-proven prostate and bladder supporting nutrients I just shared with
                                you...</p>
                            <p class="roboto_font">But every single one of these bottles contains <b>double</b> the
                                amount of all-natural prostate stabilizing ingredients than most other incontinence
                                control supplements…</p>
                            <p class="roboto_font">And each batch can take several months to make.</p>
                            <h3 class="box-bg text-center greenfont">You simply won't find better forms of these 19
                                amazing ingredients, than you will in <span
                                        style="color: #fff !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>.
                            </h3>
                            <p class="roboto_font">With <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>,
                                we have seen lab-certified results from hundreds of specific studies in which
                                individuals were able to restore their bladder health, speed up recovery from prostate
                                tumors, and even reverse rectal damage and reduce inflammation in the urethra and
                                surrounding tissues…</p>
                            <p class="roboto_font">And we have seen just how rapidly their sense of well-being restored
                                itself (with no drugs, no surgeries and no doctor visits). </p>
                            <p class="roboto_font">And because <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                has now helped 100,000+ people from all across the world…</p>
                            <p class="roboto_font">Folks are now calling the results they've seen from <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                "a miracle.”</p>
                            <h3 class="box-bg text-center greenfont">It’s clear just how powerful of a "secret weapon"
                                each bottle of <span
                                        style="color: #fff !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                is...</h3>
                            <p class="roboto_font">That's because you won’t have to: </p>
                            <ul class="tick">
                                <li>make any trips to your general practitioner…</li>
                                <li>consume any potentially hazardous prescription meds…</li>
                                <li>take any embarrassing ‘tests’…</li>
                                <li>take time out of your life to undergo surgery at the hospital…</li>
                                <li>do any extra workouts or special dieting…</li>
                                <li>or anything of the sort!</li>

                            </ul>
                            <p class="roboto_font">Plus: after 5 years of research, I’ve concluded that <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                is <span style="font-weight:bold; text-decoration:underline;">BY FAR the easiest way for you to maintain normal digestion and expulsion of toxins from your body.</span>
                            </p>
                            <p class="roboto_font">With <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                you simply take 2 small capsules a day...</p>
                            <p class="roboto_font">Then sit back, relax, and feel the powerful ingredients work from
                                within.</p>

                            <div class="well well-shade">
                                <p style="font-weight:bold;" class="roboto_font">Urge to go pee all the time - gone!</p>
                                <p style="font-weight:bold;" class="roboto_font">Swollen prostate and painful urination
                                    - improved!</p>
                                <p style="font-weight:bold;" class="roboto_font">Frequent doctor visits and endless
                                    ‘screenings’ - a thing of the past!</p>
                                <p style="font-weight:bold;" class="roboto_font">No price is too high to charge for <u>permanently
                                        eliminating incontinence, stabilizing your prostate</u> and <u>increasing
                                        longevity</u></p>

                            </div>
                            <p class="roboto_font">But you won’t have to pay an arm and a leg to get these benefits over
                                the long term.</p>
                            <p class="roboto_font">Let me explain.</p>
                            <p style="font-weight:bold; text-decoration:underline;" class="roboto_font">Normally, <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                retails for $149 per bottle...</p>
                            <p class="roboto_font">And given the delicate process that goes into formulating each
                                bottle...</p>
                            <p class="roboto_font">Along with the incredible cost of sourcing the purest forms of these
                                ingredients…</p>
                            <p class="roboto_font">$149 is a heck of a deal.</p>
                            <p class="roboto_font">But right now...</p>
                            <div class="well well-shade">
                                <h3 class="greenfont">I want to give you this money-saving opportunity to add 6 more
                                    bottles of <span
                                            style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                    to your order...</h3>
                                <span class="intro">A full 6 Months supply of this all-natural formula…</span>
                                <span class="red2">For just <strike>$149</strike> <strong>$39 per bottle.</strong></span>
                                <span class="intermediate">That’s an incredible savings of $110 per bottle.</span>
                            </div>
                            <p class="roboto_font">And it also ensures you can continue to experience the full benefits
                                of consistent bladder control & vitamin regulation with NO interruption…</p>
                            <p class="roboto_font">Day after day…</p>
                            <p class="roboto_font">Week after week…</p>
                            <p class="roboto_font">Month after month.</p>
                            <p class="roboto_font">While at the same time, being 100% protected from future price
                                increases for each bottle of <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>.
                            </p>
                            <p class="roboto_font">Now that's a big deal, and here’s why…</p>
                            <p class="roboto_font">It’s because <span
                                        style=" font-weight:bold; text-decoration:underline;">these price increases are pretty much guaranteed to happen</span>
                                - especially as more and more people just like you experience their own full health
                                transformation… and because the demand for the already scarce ingredients inside <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                is going up.</p>
                            <p class="roboto_font">Plus, your shipping & handling is still free...</p>
                            <p class="roboto_font">Which makes your 6 Month Supply an <span
                                        style=" font-weight:bold; text-decoration:underline;">even bigger discount.</span>
                            </p>
                            <div class="well well-shade">
                                <h3 class="greenfont">So to be clear, here's what you're getting with this One-Time-Only
                                    Offer:</h3>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="glyphicon glyphicon-check"></span>
                                        <h4 class="bold_text roboto_font">6 Additional Bottles of <span
                                                    style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                        </h4>
                                        <h4 class="roboto_font"><strike>$149 per bottle</strike></h4>
                                        <strong>$39 per bottle!</strong>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="glyphicon glyphicon-check"></span>
                                        <h4 class="bold_text roboto_font">Upgraded Shipping & Handling</h4>
                                        <h4 class="roboto_font"><strike>$9.99 Value</strike></h4>
                                        <strong>Free</strong>
                                    </div>
                                </div>
                            </div>
                            <p class="roboto_font">As you can see, the clear choice today is to secure your one-time,
                                deeply-discounted price on <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                            </p>
                            <p style=" font-weight:bold; text-decoration:underline;" class="roboto_font">Not only will
                                each capsule deliver the best formula of Zinc, Copper, Lycopene, Vitamins B-6 and E, and
                                more, known to man...</p>
                            <p class="roboto_font">But you'll also have the peace of mind that comes from knowing that
                                you'll be protected from any future price increases (or ingredient shortages).</p>
                            <p class="roboto_font">If you’re willing to be <span
                                        style=" font-weight:bold; text-decoration:underline;">healing your prostate, increasing your energy levels, decreasing the frequency of ‘needing to go’</span>
                                and <span style=" font-weight:bold; text-decoration:underline;">eliminating chronic pain,</span>
                                this offer is for you. </p>
                            <h3 class="box-bg text-center greenfont">Today is the day you should take massive action,
                                and here’s why:</h3>

                            <p class="roboto_font">Because of the <span
                                        style=" font-weight:bold; text-decoration:underline;">natural rarity</span> of
                                several of the luxury ingredients found inside <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                            </p>
                            <p class="roboto_font">Most notably, the herbal extracts and the mushroom powders…</p>
                            <p class="roboto_font">If one of the pharmaceutical industries were to find out about this
                                offer, they could buy out the <span
                                        style=" font-weight:bold;">entire global stock</span> of these ingredients… Set
                                their own prices… And dilute down potency!</p>
                            <p class="roboto_font">That would make it <span
                                        style=" font-weight:bold; text-decoration:underline;">impossible</span> for me
                                to get this scientifically-proven formula to folks in need.</p>
                            <h3 class="box-bg text-center greenfont">If companies can dominate such markets as ‘drinking
                                water’… Just wait until Big Pharma giants set their eyes on such life-improving ‘gifts
                                from nature’ like Graviola and Lycopene…</h3>

                            <p class="roboto_font">I simply <span style=" font-weight:bold; text-decoration:underline;">refuse</span>
                                to let that happen on my watch!</p>
                            <p class="roboto_font">Which is why I simply cannot send out this link, or share this offer
                                with anyone who hasn't already invested in <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>.
                            </p>
                            <p style=" font-weight:bold; text-decoration:underline;" class="roboto_font">So go ahead and
                                click the "YES! Add to My Order" link below...</p>
                            <p class="roboto_font">And secure the long-term benefits of <span
                                        style="color: #004881 !important;font-weight: 700;font-family: 'Bree Serif', serif;font-size: 28px !important;">ActivGuard</span>
                                now.</p>
                            <p><a target="_top" href="./kon/complete_checkout.php?product_id=49"
                                  onClick="javascript:document.getElementById('upselll').submit(),grayOut();">YES! Add 6
                                    Bottles of ActivGuard to My Order at just $39 Each! That's a HUGE Savings of $110
                                    per bottle. I’ll also have upgraded shipping and handling included for free (A $9.99
                                    Savings)...</a></p>

                            <h3 class="text-center">Total Savings: <span class="bold_text">$660.00</span></h3>

                            <img src="./img/multibottle.png" alt=""
                                 class="img-responsive center-block no-border text-center"/>


                            <div style="display:none;" class="video_button">
                                <center>
                                    <a style="background: none !important;border: none !important;box-shadow: none !important;"
                                       href="#-1"
                                       onClick="javascript:document.getElementById('upselll').submit(),grayOut();"><img
                                                style="margin-top: 10px;" src="img/green_btn.png"/></a></center>
                                <div class="cards">
                                    <img src="img/cards.png" class="img-responsive center-block cards">
                                </div>
                            </div>
                            <div style="" class="video_button">
                                <center>
                                    <a style="background: none !important;border: none !important;box-shadow: none !important;"
                                       href="./kon/complete_checkout.php?product_id=49"
                                       onClick="javascript:document.getElementById('upselll').submit(),grayOut();"><img
                                                style="margin-top: 10px;" src="img/green_btn.png"/></a></center>
                                <div class="cards">
                                    <img src="img/cards.png" class="img-responsive center-block cards">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-8 col-lg-offset-2">
                                    <div class="guarantee">
                                        <img src="./img/gurantee_img.png" alt="">
                                        <p class="roboto_font" style="padding-top: 20px;">We guarantee you’ll be
                                            completely satisfied with this product. If at any point you are not happy
                                            with the results, contact us for 100% money back within 180 days. No hassle,
                                            no questions asked.</p>
                                    </div>
                                </div>
                            </div>

                            <br/>
                            <p><a href="last-chance.php" class="no-thanks">No thanks. I understand that this is my only
                                    opportunity to get access to this special offer and I'm okay with missing out, even
                                    at this HUGE discount. Instead, if I'm blown away by the results I get from
                                    ActivGuard, like the 600,000+ happy customers from all across the world... I'll just
                                    re-order at $149 per bottle in the future, plus $9.99 shipping and handling costs. I
                                    will pass on this chance forever and have no regrets later.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer">
        <div class="container">
            <ul class="list-inline">
                <li><a onClick="window.onbeforeunload = null;" href="faq.html" target="_blank">FAQ</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="disclaimer.html" target="_blank">Disclaimer</a>
                </li>
                <li><a onClick="window.onbeforeunload = null;" href="anti-spam-policy.html" target="_blank">Anti-Spam
                        Policy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="privacy.html" target="_blank">Privacy</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="terms-and-conditions.html" target="_blank">Terms
                        and Conditions</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="refunds.html" target="_blank">Refunds</a></li>
                <li><a onClick="window.onbeforeunload = null;" href="contact-us.html" target="_blank">Contact Us</a>
                </li>

            </ul>
            <div class="disclaimer">
                <p>The statements on this website and on these product labels have not been evaluated by the food and
                    drug administration. These products are intended as a dietary supplement only. Products are not
                    intended to diagnose, treat, cure or prevent any disease. Individual results may vary based on age,
                    gender, body type, compliance, and other factors. All products are intended for use by adults over
                    the age of 18. Consult a physician before taking any of our products, especially if you are
                    pregnant, nursing, taking medication, diabetic, or have any medical condition.</p>
            </div>
            <br/>
            <p>Copyright &copy; ActivGuard 2017</p>
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-73696839-3', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WG3MPM3"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->


    <input type="hidden" name="campaign_id" value="7">
    <input type="hidden" name="product_id" value="49">
    <input type="hidden" name="product_qty" value="1">
    <input type="hidden" name="upsell" value="upsell_process">
    <input type="hidden" name="shipping_id" value="">
    <input type="hidden" name="orderId" value="<?php echo $_REQUEST['orderId']; ?>"/>
    <input type="hidden" name="sessionId" value="<?php echo $sessionId; ?>"/>


    <?php
    foreach ($_GET as $key => $value) {
        if ($key == "click_id") {
            echo "<input type='hidden' name='click_id' value='" . $click_id . "'>";
        } else {
            echo "<input type='hidden' name='" . safeRequestKonnective($key) . "' value='" . safeRequestKonnective($_GET[$key]) . "'>";
        }
    }
    ?>

</form>


<?php

if (count($_SESSION['purchased_items']) > 0) {
    foreach ($_SESSION['purchased_items'] as $v) {
        if (!in_array($v['product_id'], array(16, 17, 18)))
            continue;

        $pixel = $productArray[$v['product_id']]['pixel'];

        $returnArray = json_decode($v['return'], true);
        $orderId = $returnArray['message']['orderId'];
        if (is_array($pixel)) {
            foreach ($pixel as $pixel1) {
                ?>
                <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel1 ?>&t=<?php echo $orderId; ?>" height="1"
                        width="1" frameborder="0"></iframe>
                <?php
            }
        } else {
            ?>
            <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel ?>&t=<?php echo $orderId; ?>" height="1"
                    width="1" frameborder="0"></iframe>
            <?php
        }
    }
}
?>

</body>
</html>

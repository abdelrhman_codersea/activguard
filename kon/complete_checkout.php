<?php

include('./KonConfig.php');
//$validation_function = 'validate_checkout_form()';
//if (!isset($_REQUEST['prospectId']) || empty($_REQUEST['prospectId'])) {
//    $validation_function = 'validate_one_form()';
//}
if (!$_SESSION['billingSameAsShipping']) {
    $_SESSION['billingSameAsShipping'] = "YES";
}
if (!in_array($_REQUEST['product_id'], array(16, 17, 18))) {


    $errors = array();
//    $requiredFields = array('campaign_id' => 'Campaign Id', 'custom_product' => 'Product Id', 'cc_number' => 'Credit Card number');
//
//    foreach ($requiredFields as $k => $v)
//        if (empty($_POST[$k])) {
//            $errors[] = $requiredFields[$k] . " can not be empty.";
//        }
    //NewOrderCardOnFile($upsellCampaignId, $orderId, $upsellProductId, $upsellShippingId, $upsellPrice, $click_id, $notesUpsell);
    if (!empty($_POST['AFID'])) {
        $AFID = $_POST['AFID'];
    }
    if (!empty($_POST['SID'])) {
        $SID = $_POST['SID'];
    }
    if (!empty($_POST['AFFID'])) {
        $AFFID = $_POST['AFFID'];
    }
    if (!empty($_POST['C1'])) {
        $C1 = $_POST['C1'];
    }
    if (!empty($_POST['C2'])) {
        $C2 = $_POST['C2'];
    }
    if (!empty($_POST['C3'])) {
        $C3 = $_POST['C3'];
    }
    if (!empty($_POST['AID'])) {
        $AID = $_POST['AID'];
    }
    if (!empty($_POST['OPT'])) {
        $OPT = $_POST['OPT'];
    }
    if (!empty($_POST['click_id'])) {
        $click_id = $_POST['click_id'];
    }
    if (!empty($_POST['CID'])) {
        $CID = $_POST['CID'];
    }
    if (!empty($_POST['notes'])) {
        $notes = $_POST['notes'];
    }
    $productId = $_REQUEST['product_id'];
    $errordub = '';
    $purchasedItemsArray = array();
    foreach ($_SESSION['purchased_items'] as $v) {
        $purchasedItemsArray[] = $v['product_id'];
    }

    $refusalArray = array(
        16 => array(17, 18),
        17 => array(16, 18),
        18 => array(16, 17),
        49 => array(50, 49),
        50 => array(49, 50)

    );

    if (count(array_intersect($refusalArray[$productId], $purchasedItemsArray)) > 0) {
        $errordub = 'duplicate order';
    }


    if (empty($errordub)) {

        $shipping_price = "0.00";
        $content = NewOrderIdKonnective($_SESSION['ord_id'], $productId, $shipping_price);
        $ret = json_decode($content, true);


        if ($ret['result'] == 'SUCCESS') {
            $data2 = $ret['message'];


            $orderId = $data2['orderId'];
            $_SESSION['purchased_items'][] = array('product_id' => $productId, 'time' => time(), 'return' => $content);

            header("Location:{$productArray[$productId]['success_redirect']}");
            die();
        } else {


            if (isset($ret['message']))
                $errorMessage = $ret['message'];

//        echo $errorMessage;
            $ref = $_SERVER['HTTP_REFERER'];
            $refParts = explode('?', $ref);
            $url = $refParts[0] . '?errorMessage=' . $errorMessage;

            header("Location:$url");
        }
    } else {

        $ref = $_SERVER['HTTP_REFERER'];
        $refParts = explode('?', $ref);
        $url = $refParts[0] . '?errorMessage=' . $errordub;

        header("Location:$url");
    }
    exit();
} else {
    if (isset($_POST['submitted']) && $_POST['submitted'] == '1') {

        $errors = array();
        $requiredFields = array('campaign_id' => 'Campaign Id', 'custom_product' => 'Product Id', 'cc_number' => 'Credit Card number');

        foreach ($requiredFields as $k => $v)
            if (empty($_POST[$k])) {
                $errors[] = $requiredFields[$k] . " can not be empty.";
            }


        if (count($errors) > 0) {
            $errorMessage = implode("\n ", $errors);
        } else {

            $folder = '';
            $AFID = '';
            $AFFID = '';
            $SID = '';
            $C1 = '';
            $C2 = '';
            $C3 = '';
            $AID = '';
            $OPT = '';
            $click_id = '';
            $CID = '';
            $notes = '';

            if (!empty($_POST['AFID'])) {
                $AFID = $_POST['AFID'];
            }
            if (!empty($_POST['SID'])) {
                $SID = $_POST['SID'];
            }
            if (!empty($_POST['AFFID'])) {
                $AFFID = $_POST['AFFID'];
            }
            if (!empty($_POST['C1'])) {
                $C1 = $_POST['C1'];
            }
            if (!empty($_POST['C2'])) {
                $C2 = $_POST['C2'];
            }
            if (!empty($_POST['C3'])) {
                $C3 = $_POST['C3'];
            }
            if (!empty($_POST['AID'])) {
                $AID = $_POST['AID'];
            }
            if (!empty($_POST['OPT'])) {
                $OPT = $_POST['OPT'];
            }
            if (!empty($_POST['click_id'])) {
                $click_id = $_POST['click_id'];
            }
            if (!empty($_POST['CID'])) {
                $CID = $_POST['CID'];
            }
            if (!empty($_POST['notes'])) {
                $notes = $_POST['notes'];
            }


            $order_campain_id = $_REQUEST['campaign_id'];
            $productId = $_REQUEST['custom_product'];
            $page = $_REQUEST['page'];
            $custom_product_price_ll = $_REQUEST['custom_product_price'];
            $_SESSION['pr_price'] = $custom_product_price_ll;
            $_SESSION['product_name'] = $_REQUEST['product_name'];
            $shippingId = $_REQUEST['shippingId'];
            $cc_type = $_REQUEST['cc_type'];
            $cc_number = $_REQUEST['cc_number'];
            $_SESSION['cc_number'] = $_REQUEST['cc_number'];
            $fields_expmonth = $_REQUEST['fields_expmonth'];
            $fields_expyear = $_REQUEST['fields_expyear'];
            $cc_cvv = $_REQUEST['cc_cvv'];
            $_SESSION['cc_cvv'] = $_REQUEST['cc_cvv'];
            $_SESSION['fields_expmonth'] = $_REQUEST['fields_expmonth'];
            $_SESSION['fields_expyear'] = $_REQUEST['fields_expyear'];
            $billing_same_as_shipping = $_REQUEST['billingSameAsShipping'];

            $expirationDate = $fields_expmonth . $fields_expyear;
            $upsellCount = 0;
            $product_qty = 1;
            $amount = $_REQUEST['custom_product_price'];


            if ($billing_same_as_shipping == 'NO') {
                $billingfanme = $_REQUEST['billing_fname'];
                $billinglanme = $_REQUEST['billing_lname'];
                $billingaddress = $_REQUEST['billing_street_address'];
                $billingcity = $_REQUEST['billing_city'];
                $billingstate = $_REQUEST['billing_state'];
                $billingzip = $_REQUEST['billing_postcode'];
                $billingcountry = 'US';
            } else {
                $billingfanme = '';
                $billinglanme = '';
                $billingaddress = '';
                $billingcity = '';
                $billingstate = '';
                $billingzip = '';
                $billingcountry = '';
            }
            $_SESSION['billing_fname'] = $billingfanme;
            $_SESSION['billing_lname'] = $billinglanme;
            $_SESSION['billing_street_address'] = $billingaddress;
            $_SESSION['billing_city'] = $billingcity;
            $_SESSION['billing_state'] = $billingstate;
            $_SESSION['billing_postcode'] = $billingzip;
            $_SESSION['US'] = $billingcountry;
            $orderTempId = isset($_POST['orderId']) ? $_POST['orderId'] : '';

            $sessionId = !empty($_REQUEST['sessionId']) ? $_REQUEST['sessionId'] : '';
            $_SESSION['country'] = "US";
            $insure_campaign_id = '';
            $fields_address2 = isset($_POST['fields_address2']) ? $_POST['fields_address2'] : '';
            $insure_custom_product = '';
            $insure_shipping_id = '';
            $shipping_price = '';
            $guaranteeship = '';

            $_SESSION['fields_fname'] = $_POST['fields_fname'];
            $_SESSION['fields_lname'] = $_POST['fields_lname'];
            $_SESSION['fields_address1'] = $_POST['fields_address1'];
            $_SESSION['fields_address2'] = $fields_address2;
            $_SESSION['fields_city'] = $_POST['fields_city'];
            $_SESSION['fields_state'] = $_POST['fields_state'];
            $_SESSION['fields_zip'] = $_POST['fields_zip'];
            $_SESSION['fields_phone'] = $_POST['fields_phone'];
            $_SESSION['fields_email'] = $_POST['fields_email'];

            $content = NewOrder($order_campain_id, $_POST['fields_fname'], $_POST['fields_lname'], $_POST['fields_address1'], $fields_address2, $_POST['fields_city'], $_POST['fields_state'], $_POST['fields_zip'], "US", $_POST['fields_phone'], $_POST['fields_email'], $cc_type, $cc_number, $fields_expmonth, $fields_expyear, $cc_cvv, $productId, $shippingId, $upsellCount, $billing_same_as_shipping, $product_qty, $custom_product_price_ll, $AFID, $SID, $AFFID, $C1, $C2, $C3, $AID, $OPT, $notes, $billingaddress, $billingcity, $billingstate, $billingzip, $billingcountry, $billingfanme, $billinglanme, $sessionId, $insure_campaign_id, $insure_custom_product, $insure_shipping_id, $shipping_price, $guaranteeship);

            $ret = json_decode($content);

            if ($ret->result == 'SUCCESS') {
                $data2 = $ret->message;
                session_unset();
                $_SESSION['purchased_items'] = array();
                $_SESSION['purchased_items'][] = array('product_id' => $productId, 'time' => time(), 'return' => $content);
                $orderId = $data2->orderId;
                $_SESSION['ord_id'] = $orderId;


                header("Location:{$productArray[$productId]['success_redirect']}");
                //  die();
            } else {

                $errorMessage = "Please fix the following errors: " . $ret->message;

                //     header("Location:$url");
            }
            // exit();
        }
    } else {
        session_unset();
    }
}

?>


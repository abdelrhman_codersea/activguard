<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
include __DIR__.'/KonFunctions.php';
$campaign_path = "http://" . $_SERVER['SERVER_NAME'];
$ssl_url = "http://" . $_SERVER['SERVER_NAME'];

$folder_name = '';

// $shipping_id = 12;
$campaign_id = 7;

$productArray = array(
    16 => array(
        'id' => 16,
        'name' => 'ActiveGuard 1 Bottle',
        'img' => '../img/popupbottle1.png',
        'success_redirect' => $ssl_url.'/stock-up.php',
        'pixel'=>array(19,15),
        'price' => 69.00,),
    17 => array(
        'id' => 17,
        'name' => 'ActiveGuard 3 Bottles',
        'img' => '../img/sp_product1-1.png',
        'success_redirect' => $ssl_url.'/stock-up.php',
        'pixel'=>array(20,15),
        'price' => 177.00),
    18 => array(
        'id' => 18,
        'name' => 'ActiveGuard 6 Bottles',
        'img' => '../img/multibottle.png',
        'success_redirect' => $ssl_url.'/stock-up.php',
        'pixel'=>array(21,15),
        'price' => 294.00),
    49 => array(
        'id' => 49,
        'name' => 'ActiveGuard 6 Bottles (Stock Up)',
        'img' => '../img/multibottle.png',
        'success_redirect'=>$ssl_url.'/thank-you.php',
        'pixel'=>22,
        'price' => 234.00),
    50 => array(
        'id' => 50,
        'name' => 'ActiveGuard 3 Bottles (Last Chance)',
        'img' => '../img/sp_product1-1.png',
        'success_redirect'=>$ssl_url.'/thank-you.php',
        'pixel'=>23,
        'price' => 117.00),

);

$custom_product_price = 0.00;



$currency_text = 'USD';
?>

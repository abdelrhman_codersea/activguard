<?php


    function safeRequestKonnective($strGet) {
        $strGet = preg_replace("/[^\-a-zA-Z0-9\_]*/m", "", $strGet);
        //$strGet = preg_replace("/[^a-zA-Z0-9(\040)\(\)']*/m","",$strGet); //<--to allow space \040
        $strGet = str_ireplace("javascript", "", $strGet);
        $strGet = str_ireplace("encode", "", $strGet);
        $strGet = str_ireplace("decode", "", $strGet);
        return trim($strGet);
    }

    function NewLeadKonnective($campaign_id, $fields_fname, $fields_lname, $fields_address1, $fields_address2, $fields_city, $fields_state, $fields_zip, $country_2_digit, $fields_phone, $fields_email, $AFID, $SID, $AFFID, $C1, $C2, $C3, $AID, $OPT, $click_id, $notes = '') {
        global $limelight_api_username, $limelight_api_password, $limelight_crm_instance;
        $_SESSION['fname'] = $fields_fname;
        $_SESSION['lname'] = $fields_lname;
        $_SESSION['address'] = $fields_address1;
        $_SESSION['city'] = $fields_city;
        $_SESSION['state'] = $fields_state;
        $_SESSION['phone'] = $fields_phone;
        $_SESSION['zip'] = $fields_zip;
        $_SESSION['email'] = $fields_email;
        if ($campaign_id == "5") {
            $AFFID = "";
            $SID = "";
            $C1 = "";
            $C2 = "";
            $C3 = "";
            $click_id = "";
        }
        $fields = array('loginId' => 'lcatella52',
            'password' => 'l01Xx0A71011',
            'campaignId' => $campaign_id,
            'firstName' => trim($fields_fname),
            'lastName' => trim($fields_lname),
            'address1' => trim($fields_address1),
            'address2' => trim($fields_address2),
            'city' => trim($fields_city),
            'state' => trim($fields_state),
            'postalCode' => trim($fields_zip),
            'country' => $country_2_digit,
            'phoneNumber' => trim($fields_phone),
            'emailAddress' => trim($fields_email),
            'affId' => trim($AFFID),
            'sourceValue1' => trim($SID),
            'sourceValue2' => trim($C1),
            'sourceValue3' => trim($C2),
            'sourceValue4' => trim($C3),
            'sourceValue5' => trim($click_id),
            'ipAddress' => $_SERVER['REMOTE_ADDR']);

        $Curl_Session = curl_init();
        curl_setopt($Curl_Session, CURLOPT_URL, 'https://api.konnektive.com/leads/import/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
        return $content = curl_exec($Curl_Session);


        //return $content = curl_exec($Curl_Session);
        //$header = curl_getinfo($Curl_Session);
    }

    function NewOrderWithLeadKonnective($campaign_id, $orderTempId, $creditCardType, $creditCardNumber, $cardMonth, $cardYear, $cvv, $productId, $shippingId, $upsellCount, $billingSameAsShipping, $product_qty, $custom_product_price, $AFID, $SID, $AFFID, $C1, $C2, $C3, $AID, $OPT, $notes = '', $billingaddress = '', $billingcity = '', $billingstate = '', $billingzip = '', $billingcountry = '', $billingfanme = '', $billinglanme = '', $sessionId = '', $insure_campaign_id, $insure_custom_product, $insure_shipping_id, $shipping_price, $guaranteeship) {
        //global $limelight_api_username,$limelight_api_password,$limelight_crm_instance;

        $billing_fields = array();
        if (!empty($billingSameAsShipping) && $billingSameAsShipping == 'NO') {
            $billing_fields = array('shipFirstName' => $billingfanme,
                'shipLastName' => $billinglanme,
                'shipAddress1' => $billingaddress,
                'shipCity' => $billingcity,
                'shipState' => $billingstate,
                'shipPostalCode' => $billingzip,
                'shipCountry' => $billingcountry
            );
        }

        $fields1 = array('loginId' => 'lcatella52',
            'password' => 'l01Xx0A71011',
            'orderId' => $orderTempId,
            'paySource' => 'CREDITCARD',
            'firstName' => $_SESSION['fname'],
            'lastName' => $_SESSION['lname'],
            'address1' => $_SESSION['address'],
            'city' => $_SESSION['city'],
            'state' => $_SESSION['state'],
            'postalCode' => $_SESSION['zip'],
            'country' => $_SESSION['country'],
            'phoneNumber' => $_SESSION['phone'],
            'emailAddress' => $_SESSION['email'],
            'cardNumber' => $creditCardNumber,
            'cardMonth' => $cardMonth, //mmyy
            'cardYear' => $cardYear, //mmyy
            'cardSecurityCode' => $cvv,
            'tranType' => 'Sale',
            'product1_id' => $productId,
            'campaignId' => $campaign_id,
            'product1_shipPrice' => '4.99',
            'billShipSame' => $billingSameAsShipping,
            'affId' => 'BSD',
            'sourceValue1' => trim($SID),
            'sourceValue2' => trim($C1),
            'sourceValue3' => trim($C2),
            'sourceValue4' => trim($C3),
            'sourceValue5' => trim($click_id),
            'ipAddress' => $_SERVER['REMOTE_ADDR']);

        //echo "<pre>".print_r($fields1,true)."</pre>";

        if (!empty($billing_fields)) {
            $fields = array_merge($fields1, $billing_fields);
        } else {
            $fields = $fields1;
        }
        //echo "<pre>";
        //print_r($fields);

        $Curl_Session = curl_init();
        curl_setopt($Curl_Session, CURLOPT_URL, 'https://api.konnektive.com/order/import/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($Curl_Session);
        $ret = json_decode($content);







        return $content;
    }

    function NewOrder($campaign_id, $fields_fname, $fields_lname, $fields_address1, $fields_address2, $fields_city, $fields_state, $fields_zip, $country_2_digit, $fields_phone, $fields_email, $creditCardType, $creditCardNumber, $cardMonth, $cardYear, $cvv, $productId, $shippingId, $upsellCount, $billingSameAsShipping, $product_qty, $custom_product_price, $AFID, $SID, $AFFID, $C1, $C2, $C3, $AID, $OPT, $notes = '', $billingaddress = '', $billingcity = '', $billingstate = '', $billingzip = '', $billingcountry = '', $billingfanme = '', $billinglanme = '', $sessionId = '', $insure_campaign_id, $insure_custom_product, $insure_shipping_id, $shipping_price, $guaranteeship) {
        //global $limelight_api_username,$limelight_api_password,$limelight_crm_instance;
        /* $_SESSION['fname'] = $fields_fname;
          $_SESSION['lname'] = $fields_lname;
          $_SESSION['address'] = $fields_address1;
          $_SESSION['city'] = $fields_city;
          $_SESSION['state'] = $fields_state;
          $_SESSION['phone'] = $fields_phone;
          $_SESSION['zip'] = $fields_zip;
          $_SESSION['email'] = $fields_email; */

        $_SESSION['fname'] = $fields_fname;
        $_SESSION['lname'] = $fields_lname;
        $_SESSION['email'] = $fields_email;
        $_SESSION['phone'] = $fields_phone;
        $billing_fields = array();
        if (!empty($billingSameAsShipping) && $billingSameAsShipping == 'NO') {
            $billing_fields = array('shipFirstName' => $billingfanme,
                'shipLastName' => $billinglanme,
                'shipAddress1' => $fields_address1,
                'shipCity' => $fields_city,
                'shipState' => $fields_state,
                'shipPostalCode' => $fields_zip,
                'shipCountry' => $billingcountry
            );


            $_SESSION['address'] = $billingaddress;
            $_SESSION['city'] = $billingcity;
            $_SESSION['state'] = $billingstate;
            $_SESSION['zip'] = $billingzip;
        } else {
            $_SESSION['address'] = $fields_address1;
            $_SESSION['city'] = $fields_city;
            $_SESSION['state'] = $fields_state;
            $_SESSION['zip'] = $fields_zip;
        }

        $fields1 = array('loginId' => 'daneapi',
            'password' => 'd&8d^vgy1f%',
            'paySource' => 'CREDITCARD',
            'firstName' => trim($fields_fname),
            'lastName' => trim($fields_lname),
            'address1' => trim($_SESSION['address']),
            'address2' => trim($fields_address2),
            'city' => trim($_SESSION['city']),
            'state' => trim($_SESSION['state']),
            'postalCode' => trim($_SESSION['zip']),
            'country' => $country_2_digit,
            'phoneNumber' => trim($fields_phone),
            'emailAddress' => trim($fields_email),
            'cardNumber' => $creditCardNumber,
            'cardMonth' => $cardMonth, //mmyy
            'cardYear' => $cardYear, //mmyy
            'cardSecurityCode' => $cvv,
            'tranType' => 'Sale',
            'product1_id' => $productId,
            'campaignId' => $campaign_id,
            'product1_shipPrice' => '0.00',
            'billShipSame' => $billingSameAsShipping,
            'affId' => 'BSD',
            'sourceValue1' => trim($SID),
            'sourceValue2' => trim($C1),
            'sourceValue3' => trim($C2),
            'sourceValue4' => trim($C3),
            'sourceValue5' => trim($click_id),
            'ipAddress' => $_SERVER['REMOTE_ADDR']);

        //echo "<pre>".print_r($fields1,true)."</pre>";




        if (!empty($billing_fields)) {
            $fields = array_merge($fields1, $billing_fields);
        } else {
            $fields = $fields1;
        }

        $Curl_Session = curl_init();
        curl_setopt($Curl_Session, CURLOPT_URL, 'https://api.konnektive.com/order/import/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($Curl_Session);
        $ret = json_decode($content);







        return $content;
    }

    function NewOrderIdKonnective($orderId, $product_id, $shipping_price) {
        //global $limelight_api_username,$limelight_api_password,$limelight_crm_instance;
        $fields = array('loginId' => 'lcatella52',
            'password' => 'l01Xx0A71011',
            'method' => 'NewOrderCardOnFile',
            'orderId' => $orderId,
            'productId' => $product_id,
            'product1_shipPrice' => $shipping_price);

        //echo "<pre>".print_r($fields,true)."</pre>";die();
        $Curl_Session = curl_init();
        curl_setopt($Curl_Session, CURLOPT_URL, 'https://api.konnektive.com/upsale/import/');
        curl_setopt($Curl_Session, CURLOPT_POST, 1);
        curl_setopt($Curl_Session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($Curl_Session, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($Curl_Session);
        //echo "<pre>";
        //print_r($content);
        return $content;
        //$header = curl_getinfo($Curl_Session);
    }
?>
<?php

@session_start();
error_reporting(0);
include('./complete_checkout.php');
$validation_function = 'validate_checkout_form()';
if (!isset($_REQUEST['prospectId']) || empty($_REQUEST['prospectId'])) {
    $validation_function = 'validate_one_form()';
}
if ($_REQUEST['errorMessage']) {
    echo "<table style='width:100%;' align='center'><tr><td style='background-color:#ff0000;color:#ffffff;font-size: 18px;font-family: arial,helvetica,sans-serif;font-weight:bold;height:50px;text-align: center;' align='center'>" . urldecode($_REQUEST['errorMessage']) . "</td></tr></table>";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="Expires" content="30"/>
    <meta name="robots" content="noindex,nofollow"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-site-verification" content="GpJYgM2h4WpUeO0jiWIGxtmLnPu-7G611uSFpsvBbHk"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>ActivGuard - Checkout</title>
    <link rel="stylesheet" href="./../css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/font-awesome.min.css">
    <link rel="stylesheet" href="./../css/sass/main.min.css">
</head>
<body class="beige-color">
<header>
    <div class="container">
        <div class="col-xs-12">
            <div class="col-sm-6 text-left">
                <a href="./../">
                    <img src="./../img/logo.png" id="logo">
                </a>
            </div>
            <div class="col-sm-6" id="american-flag">
                <img src="./../img/american_flag_with_text.png" alt="American Flag">
            </div>
        </div>
    </div>
</header>
<div id="checkout">
    <div class="container">
        <div id="checkout-container" class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div id="checkout-steps">
                        <div class="col-xs-6 text-center text-light">FINISH ORDER</div>
                        <div class="col-xs-6 text-center">SUMMARY</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div id="order-counter">
                        <h2>Your order is resevered for: <span id="counter">10:00</span></h2>
                    </div>
                    <div id="great-job">
                        <div>
                            <strong class="secondary-color">Great Job!</strong>
                        </div>
                        <div>
                            You’re taking your first step towards a
                            healthier body. Act now so you don’t miss out on this offer!
                        </div>
                    </div>
                    <div id="available-sell">
                        <div id="current-availability">
                            Current Availability:
                        </div>
                        <div id="bar">
                            <div id="current-value"></div>
                        </div>
                        <div>
                            <strong class="text-red">LOW</strong>
                        </div>
                        <div id="sell-out">
                            Sell out: <strong class="text-red">Risk</strong>
                        </div>
                    </div>
                    <div class="text-center">
                        <h3><span class="secondary-color">Shipping is free! </span> Enjoy fast shipping.</h3>
                    </div>
                    <div id="buttons-package">
                        <div class="col-sm-5 col-xs-12 text-center pt-5 pl-0">
                            <h3>Your Selected Package</h3>
                        </div>
                        <div class="col-sm-7 col-xs-12 pull-right pr-0 text-center">
                            <?php $product_id = $_REQUEST['product_id']; ?>
                            <input type="hidden" name="prc_id" value="<?php echo $product_id; ?>"/>

                            <div class="btn-package-1" data-target="#preview-package-1" data-product="17">
                                <p>3 Bottles - $177</p>
                            </div>
                            <div class="btn-package-2" data-target="#preview-package-2" data-product="18">
                                <p>6 Bottles - $294</p>
                            </div>
                            <div class="btn-package-3" data-target="#preview-package-3" data-product="16">
                                <p>1 Bottles - $69</p>
                            </div>
                        </div>
                    </div>
                    <div class="template-1 product-preview" id="preview-package-1">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12">
                                <img src="./../img/sp_product1.png" alt="SP Product1">
                            </div>
                            <div class="col-sm-5 col-xs-12 pt-30">
                                <h1 class="secondary-color2"><strong>ActivGUARD</strong></h1>
                                <h1 class="text-red"><strong>$59 / bottle</strong></h1>
                                <h4><strong>
                                        <del>Was: USD $447</del>
                                    </strong></h4>
                                <h4><strong>Now: USD $117</strong></h4>
                                <h5 class="text-red"><strong>(Free Shipping)</strong></h5>
                            </div>
                        </div>
                    </div>
                    <div class="template-1 product-preview" id="preview-package-2">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12">
                                <img src="./../img/sp_product2.png" alt="SP Product2">
                            </div>
                            <div class="col-sm-5 col-xs-12 pt-30">
                                <h1 class="secondary-color2"><strong>ActivGUARD</strong></h1>
                                <h1 class="text-red"><strong>$49 / bottle</strong></h1>
                                <h4><strong>
                                        <del>Was: USD $894</del>
                                    </strong></h4>
                                <h4><strong>Now: USD $294</strong></h4>
                                <h5 class="text-red"><strong>(Free Shipping)</strong></h5>
                            </div>
                        </div>
                    </div>
                    <div class="template-1 product-preview" id="preview-package-3">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12">
                                <img src="./../img/sp_product3.png" alt="SP Product3">
                            </div>
                            <div class="col-sm-5 col-xs-12 pt-30">
                                <h1 class="secondary-color2"><strong>ActivGUARD</strong></h1>
                                <h1 class="text-red"><strong>$69 / bottle</strong></h1>
                                <h4><strong>
                                        <del>Was: USD $149</del>
                                    </strong></h4>
                                <h4><strong>Now: USD $69</strong></h4>
                                <h5 class="text-red"><strong>(Free Shipping)</strong></h5>
                            </div>
                        </div>
                    </div>
                    <div class="rush">
                        <img src="./../img/rush.png" alt="Rush image">
                    </div>
                    <div class="template-1 guarantee-2">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>
                                    <strong>NCLUDES A 180 DAYS 100% MONEY BACK GUARANTEE</strong>
                                </h3>
                            </div>
                            <div class="col-sm-2 col-xs-12 text-center">
                                <img src="./../img/gurantee_img.png" alt="Guarantee Img">
                            </div>
                            <div class="col-sm-10 col-xs-12">
                                <p>
                                    ActivGUARD comes with a 180 Day, 100% Money Back Guarantee. That means if you change
                                    your mind about this decision at any point in the next six months – all you need to
                                    do
                                    is email us, and we’ll refund your purchase. Plus, you don’t need to return the
                                    bottle.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div id="final-step">
                        <div id="title-form">
                            <h1><strong>FINAL STEP</strong></h1>
                            <h1>PAYMENT CONFIRMATION</h1>
                        </div>
                        <form data-toggle="validator" role="form" action="./checkout.php?product_id=<?php echo $_REQUEST['product_id'] ?>" method="post" id="checkout_form">

                            <div class="form-group">
                                <input type="text" id="firstName" placeholder="First name" name="fields_fname"
                                       value="<?php echo $_SESSION['fname']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="text" id="lastName" placeholder="Last name" name="fields_lname"
                                       value="<?php echo $_SESSION['lname']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="email" placeholder="Email address" id="email" name="fields_email" value="<?php echo $_SESSION['email']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="text" placeholder="Phone number" id="phone" name="fields_phone" maxlength="10" onKeyDown="return onlyNumbers(event,'require')" value="<?php echo $_SESSION['phone']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="text" id="address" placeholder="Address line 1" name="fields_address1" value="<?php echo $_SESSION['address']; ?>" required>
                            </div>

                            <!--                            <input type="text" placeholder="Address line 2" name="fields_address2" value="<?php echo $_SESSION['fields_address2']; ?>">-->

                            <div class="form-group">
                                <input type="text" placeholder="City" id="City" name="fields_city" value="<?php echo $_SESSION['city']; ?>" required>
                            </div>
                            <?php
                            $statesArray = array();
                            $statesArray["AL"] = "Alabama (AL)";
                            $statesArray["AL"] = "Alabama";
                            $statesArray["AK"] = "Alaska";
                            $statesArray["AZ"] = "Arizona";
                            $statesArray["AR"] = "Arkansas";
                            $statesArray["CA"] = "California";
                            $statesArray["CO"] = "Colorado";
                            $statesArray["CT"] = "Connecticut";
                            $statesArray["DE"] = "Delaware";
                            $statesArray["DC"] = "District of Columbia";
                            $statesArray["FL"] = "Florida";
                            $statesArray["GA"] = "Georgia";
                            $statesArray["HI"] = "Hawaii";
                            $statesArray["ID"] = "Idaho";
                            $statesArray["IL"] = "Illinois";
                            $statesArray["IN"] = "Indiana";
                            $statesArray["IA"] = "Iowa";
                            $statesArray["KS"] = "Kansas";
                            $statesArray["KY"] = "Kentucky";
                            $statesArray["LA"] = "Louisiana";
                            $statesArray["ME"] = "Maine";
                            $statesArray["MD"] = "Maryland";
                            $statesArray["MA"] = "Massachusetts";
                            $statesArray["MI"] = "Michigan";
                            $statesArray["MN"] = "Minnesota";
                            $statesArray["MS"] = "Mississippi";
                            $statesArray["MO"] = "Missouri";
                            $statesArray["MT"] = "Montana";
                            $statesArray["NE"] = "Nebraska";
                            $statesArray["NV"] = "Nevada";
                            $statesArray["NH"] = "New Hampshire";
                            $statesArray["NJ"] = "New Jersey";
                            $statesArray["NM"] = "New Mexico";
                            $statesArray["NY"] = "New York";
                            $statesArray["NC"] = "North Carolina";
                            $statesArray["ND"] = "North Dakota";
                            $statesArray["OH"] = "Ohio";
                            $statesArray["OK"] = "Oklahoma";
                            $statesArray["OR"] = "Oregon";
                            $statesArray["PA"] = "Pennsylvania";
                            $statesArray["PR"] = "Puerto Rico";
                            $statesArray["RI"] = "Rhode Island";
                            $statesArray["SC"] = "South Carolina";
                            $statesArray["SD"] = "South Dakota";
                            $statesArray["TN"] = "Tennessee";
                            $statesArray["TX"] = "Texas";
                            $statesArray["UT"] = "Utah";
                            $statesArray["VT"] = "Vermont";
                            $statesArray["VI"] = "Virgin Islands of the U.S.";
                            $statesArray["VA"] = "Virginia";
                            $statesArray["WA"] = "Washington";
                            $statesArray["WV"] = "West Virginia";
                            $statesArray["WI"] = "Wisconsin";
                            $statesArray["WY"] = "Wyoming";
                            ?>
                            <div class="form-group">
                                <select name="fields_state" id="states" required>
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($statesArray as $key => $value) {
                                        if ($key == $_SESSION['state']) {
                                            echo "<option value='$key' selected>$value</option>";
                                        } else {
                                            echo "<option value='$key'>$value</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Postal code" id="postalCode" name="fields_zip" maxlength="5" value="<?php echo $_SESSION['zip']; ?>" onKeyDown="return onlyNumbers(event,'require')" required>
                            </div>
                            <div id="Billing">
                                <input type="checkbox" checked name="">
                                <span>Billing Address same as shipping ?</span>
                                <div id="Billing-cards">
                                    <div id="billingsame" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" name="billing_street_address" id="billing_street_address" value="<?php echo $_SESSION['billing_street_address']; ?>" placeholder="Address"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="billing_city" id="billing_city" placeholder="City" value="<?php echo $_SESSION['billing_city']; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <select name="billing_state" id="billing_state">
                                                <option value="">Select State</option>
                                                <?php
                                                foreach ($statesArray as $key => $value) {
                                                    if ($key == $_SESSION['billing_state']) {
                                                        echo "<option value='$key' selected>$value</option>";
                                                    } else {
                                                        echo "<option value='$key'>$value</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="billing_postcode" id="billing_postcode" maxlength="6" value="<?php echo $_SESSION['billing_postcode']; ?>" onKeyDown="return onlyNumbers(event,'require')" placeholder="Postal Code"/>
                                        </div>
                                    </div>
                                    <img src="./../img/creditcard_imgs.png" alt="Credit Cards">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Credit Card Number" id="cardNumber" name="cc_number" autocomplete="off" maxlength="16" onKeyDown="return onlyNumbers(event,'require')" required>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <select name="fields_expmonth" id="expirationMonth">
                                        <option value="01" selected>01 (Jan)</option>
                                        <option value='02'>02 (Feb)</option>
                                        <option value='03'>03 (Mar)</option>
                                        <option value='04'>04 (Apr)</option>
                                        <option value='05'>05 (May)</option>
                                        <option value='06'>06 (Jun)</option>
                                        <option value='07'>07 (Jul)</option>
                                        <option value='08'>08 (Aug)</option>
                                        <option value='09'>09 (Sep)</option>
                                        <option value='10'>10 (Oct)</option>
                                        <option value='11'>11 (Nov)</option>
                                        <option value='12'>12 (Dec)</option>
                                    </select required>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <select name="fields_expyear" id="expirationYear" class="required n-year year">
                                            <option value="" selected>Select year</option>
                                            <option value='17'>2017</option>
                                            <option value='18'>2018</option>
                                            <option value='19'>2019</option>
                                            <option value='20'>2020</option>
                                            <option value='21'>2021</option>
                                            <option value='22'>2022</option>
                                            <option value='23'>2023</option>
                                            <option value='24'>2024</option>
                                            <option value='25'>2025</option>
                                            <option value='26'>2026</option>
                                            <option value='27'>2027</option>
                                            <option value='28'>2028</option>
                                            <option value='29'>2029</option>
                                            <option value='30'>2030</option>
                                            <option value='31'>2031</option>
                                            <option value='32'>2032</option>
                                            <option value='33'>2033</option>
                                            <option value='34'>2034</option>
                                            <option value='35'>2035</option>
                                            <option value='36'>2036</option>
                                        </select required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="CVV code" id="securityCode" name="cc_cvv" autocomplete="off" maxlength="3" value="" onKeyDown="return onlyNumbers(event,'require')" required>
                                    </div>
                                </div>
                                <div class="col-xs-6 pt-5">
                                    <em class="text-red">What's this?</em>
                                </div>
                            </div>
                            <p class="main-color text-center"><i class="fa fa-lock"></i> Secure 256-bit encrypted
                                connection</p>
                            <div class="row pb-20">
                                <div class="col-lg-12 col-lg-offset-0 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                                    <input type="image" src="./../img/complete_checkout.png" name="" id="submit-checkout">
                                </div>
                            </div>
                            <img src="./../img/visa.png" alt="Visa Cards">
                            <input type="hidden" id="custom_product" name="custom_product"
                                   value="<?php echo $_REQUEST['product_id']; ?>"/>
                            <input type="hidden" id="custom_product_price" name="custom_product_price"
                                   value="<?php echo $pr_price; ?>"/>
                            <input type="hidden" id="product_name" name="product_name"
                                   value="<?php echo $product_name; ?>"/>
                            <input type="hidden" name="shippingId" id="shippingId" value="<?php echo $shipping_id; ?>"/>
                            <input type="hidden" name="campaign_id" id="campaign_id"
                                   value="<?php echo $campaign_id; ?>"/>
                            <input type="hidden" name="billingSameAsShipping" id="billingSameAsShipping" value="YES"/>
                            <input type="hidden" id="billing_country" name="billing_country" value="US"/>
                            <input type="hidden" name="country" id="country" value="US"/>
                            <input type="hidden" id="submitted" name="submitted" value="1"/>
                            <input type="hidden" id="page" name="page" value="step2"/>

                            <?php
                            foreach ($_GET as $key => $value) {
                                echo "<input type='hidden' name='" . safeRequestKonnective($key) . "' value='" . safeRequestKonnective($_GET[$key]) . "'>";
                            }
                            ?>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="inline-list">
                    <li><a href="./../faq.html" target="_blank">FAQ</a></li>
                    <li><a href="./../disclaimer.html" target="_blank">Disclaimer</a></li>
                    <li><a href="./../anti-spam-policy.html" target="_blank">Anti-Spam Policy</a></li>
                    <li><a href="./../privacy.html" target="_blank">Privacy</a></li>
                    <li><a href="./../terms-and-conditions.html" target="_blank">Terms and Conditions</a></li>
                    <li><a href="./../refunds.html" target="_blank">Refunds</a></li>
                    <li><a href="./../contact-us.html" target="_blank">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-xs-10 col-xs-offset-1">
                <p class="text-light">
                    The statements on this website and on these product labels have not been evaluated by the food and
                    drug
                    administration. These products are intended as a dietary supplement only. Products are not intended
                    to
                    diagnose, treat, cure or prevent any disease. Individual results may vary based on age, gender, body
                    type, compliance, and other factors. All products are intended for use by adults over the age of 18.
                    Consult a physician before taking any of our products, especially if you are pregnant, nursing,
                    taking
                    medication, diabetic, or have any medical condition.
                </p>
            </div>
            <div class="col-xs-12">
                <p>Copyright © ActivGuard 2017</p>
            </div>
        </div>
    </div>
</footer>
<?php

if (count($_SESSION['purchased_items']) > 0) {
    foreach ($_SESSION['purchased_items'] as $v) {
        if (!in_array($v['product_id'], array(16, 17, 18)))
            continue;

        $pixel = $productArray[$v['product_id']]['pixel'];

        $returnArray = json_decode($v['return'], true);
        $orderId = $returnArray['message']['orderId'];
        if (is_array($pixel)) {
            foreach ($pixel as $pixel1) {
                ?>
                <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel1 ?>&t=<?php echo $orderId; ?>" height="1"
                        width="1" frameborder="0"></iframe>
                <?php
            }
        } else {
            ?>
            <iframe src="https://ahstrk.com/p.ashx?o=6&e=<?php echo $pixel ?>&t=<?php echo $orderId; ?>" height="1"
                    width="1" frameborder="0"></iframe>
            <?php
        }
    }
}
?>
<script src="./../js/jquery.min.js"></script>
<script async src="./../js/validator.min.js"></script>
<script async src="./../js/pages/checkout.min.js"></script>
</body>
</html>
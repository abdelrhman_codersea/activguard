<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Unsubscribe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style>

  * {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }

  html {
    font-size: 14px;
    line-height: 23px;
  }

  body {
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    background-color: #f2f2f2;
    color: #545D63;
    font-family: "Helvetica Neue", "Helvetica", "Arial", "Sans-Serif";
    font-weight: normal;
    margin: 0;
    padding: 52px 0 0;
    width: 100%;
  }

  h1, .h2 {
    font-weight: 900;
    line-height: 1.2em;
    margin: 0 0 12px;
    text-rendering: optimizeLegibility;
  }

  .h2 {
    font-size: 1.936rem;
    font-weight: normal;
  }

  .container {
    max-width: 600px;
    padding: 24px;
    margin: auto;
  }

  .panel {
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.08);
    padding: 24px;
  }

  .fly-btn {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
    cursor: pointer;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    background-color: #F8F8F8;
    border-radius: 3px;
    border: 1px solid #dadbdc;
    border-bottom-color: #cacccd;
    box-shadow: 0px 1px 1px -1px rgba(0, 0, 0, 0.12), 0px 1px 0px rgba(255, 255, 255, 0.72) inset;
    color: #545D63;
    display: inline-block;
    font-family: inherit;
    font-size: 0.928rem;
    font-weight: 900;
    margin: 0;
    height: 36px;
    line-height: 34px;
    padding: 0 1.24rem;
    position: relative;
    text-align: center;
    text-decoration: none;
    text-transform: capitalize;
    vertical-align: middle;
  }

  .fly-btn:hover {
    background-color: #fcfcfc;
    border-color: #d5d7d8;
    border-bottom-color: #c5c7c9;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.12), 0px 1px 0px rgba(255, 255, 255, 0.32) inset;
    color: #545D63;
    text-decoration: none;
  }
  .fly-btn:active {
    background-color: #f3f3f3;
    border-color: #d5d7d8;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.12) inset;
  }
  .fly-btn:focus {
    border-color: #2F84E7;
    outline: 0;
  }
  .fly-alert {
    background-color: #fcf8e3;
    border-radius: 3px;
    border: 1px solid #f6eaac;
    margin-bottom: 24px;
    padding: 16px;
  }

  .fly-alert.is-success {
    background-color: #dff0d8;
    border-color: #bbdfac;
    color: #3c763d;
  }
  .btn-primary {
    background-color: #009e9b;
    border-color: #008a87;
    border-bottom-color: #006b69;
    box-shadow: 0px 1px 1px -1px rgba(0, 0, 0, 0.12), 0px 1px 0px rgba(255, 255, 255, 0.32) inset;
    color: #fff;
  }
  .btn-primary:hover {
    background-color: #00aeab;
    border-color: #008a87;
    border-bottom-color: #007977;
    color: #fff;
  }
  .btn-primary:active {
    background-color: #008e8b;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.12) inset;
  }
  .btn-primary:focus {
    border-color: #2F84E7;
    box-shadow: 0px 0px 0px 1px rgba(255, 255, 255, 0.48) inset;
  }
  .btn-primary:focus:active {
    box-shadow: 0px 0px 0px 1px rgba(255, 255, 255, 0.48) inset, 0px 1px 1px rgba(0, 0, 0, 0.12) inset;
  }

  form {
    margin-bottom: 0;
  }

  </style>
  </head>
  <body>
    
    <div class="container">




					



    <div class="panel">
		<h1 class="h2">Unsubscribe</h1>
		<p>Remove yourself from all future mailings.</p>
        <!--<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css" /><link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c30432f5" type="text/css" /><script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c30432f5"></script><div class="moonray-form-p2c30432f5 ussr"><div class="moonray-form moonray-form-label-pos-stacked">-->
        
        <form action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
        <p>
        <input name="email" type="email" class="form-control" id="mr-field-element-636806414255"/>
        </p>
        <p>
        <input type="submit" name="submit-button" value="Unsubscribe" class="fly-btn btn-primary" id="mr-field-element-636806414255"/>
        </p>
        
        <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c120383f4"/></div>
         </form>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>